﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	internal abstract class DotNetWrapperBase
	{
		///// <summary>
		///// Returns the wrapped object.
		///// </summary>
		///// <typeparam name="T">The type of object.</typeparam>
		///// <returns>The wrapped object, cast to <typeparamref name="T"/>.</returns>
		//public abstract T Unwrap<T>();


		public static int GetCount<T>(SCG.ICollection<T> collection)
		{
			return collection.Count;
		}

		public static bool GetIsEmpty<T>(SCG.ICollection<T> collection)
		{
			return collection.Count == 0;
		}

		public static bool GetIsFrozen<T>(SCG.ICollection<T> collection)
		{
			return false;
		}

		public static TValue GetItem<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key)
		{
			if (key != null && !(key is TKey))
				throw new SCG.KeyNotFoundException();
			return collection[(TKey)key];
		}

		public static T GetItem<T>(SCG.IList<T> collection, int index)
		{
			return collection[index];
		}

		public static void SetItem<T>(SCG.IList<T> collection, int index, T value)
		{
			collection[index] = value;
		}

		public static void SetItem<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, TKey key, TValue value)
		{
			collection[key] = value;
		}

		public static SCG.IEnumerator<T> GetEnumerator<T>(SCG.ICollection<T> collection)
		{
			return collection.GetEnumerator();
		}

		public static SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection)
		{
			foreach (var kv in collection)
			{
				yield return Association.FromKeyValuePair(kv);
			}
		}

		public static bool IsValidMember<T>(SCG.ICollection<T> collection, object value)
		{
			return value == null
				|| value is T;
		}

		public static bool IsValidMember<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value)
		{
			return value == null
				|| value is IAssociation<TKey, TValue>;
		}

		public static bool IsValidKey<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key)
		{
			return key == null
				|| key is TKey;
		}

		public static bool IsValidValue<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value)
		{
			return value == null
				|| value is TValue;
		}

		public static bool Contains<T>(SCG.ICollection<T> collection, object value)
		{
			if (value != null && !(value is T))
				return false;
			return collection.Contains((T)value);
		}

		public static bool Contains<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value)
		{
			if (value != null && !(value is IAssociation<TKey, TValue>))
				return false;
			var kv = Association.ToKeyValuePair((IAssociation<TKey, TValue>)value);
			return collection.Contains(kv);
		}

		public static bool ContainsKey<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key)
		{
			if (key != null && !(key is TKey))
				return false;
			return collection.ContainsKey((TKey)key);
		}

		public static T GetMember<T>(SCG.ICollection<T> collection, object value)
		{
			bool isPresent;
			return GetMember(collection, value, out isPresent);
		}

		public static T GetMember<T>(SCG.ICollection<T> collection, object value, out bool isPresent)
		{
			foreach (var element in collection)
			{
				if (Object.Equals(element, value))
				{
					isPresent = true;
					return element;
				}
			}
			isPresent = false;
			return default(T);
		}

		public static IAssociation<TKey, TValue> GetMember<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value)
		{
			bool isPresent;
			return GetMember(collection, value, out isPresent);
		}

		public static IAssociation<TKey, TValue> GetMember<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value, out bool isPresent)
		{
			if (value != null && !(value is IAssociation<TKey, TValue>))
			{
				isPresent = false;
				return default(IAssociation<TKey, TValue>);
			}
			else
			{
				var kv = Association.ToKeyValuePair((IAssociation<TKey, TValue>)value);
				return GetMember((SCG.ICollection<IAssociation<TKey, TValue>>)collection, kv, out isPresent);
			}
		}

		public static TValue GetValue<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key)
		{
			bool isPresent;
			return GetValue(collection, key, out isPresent);
		}

		public static TValue GetValue<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key, out bool isPresent)
		{
			if (key != null && !(key is TKey))
			{
				isPresent = false;
				return default(TValue);
			}
			TValue value;
			isPresent = collection.TryGetValue((TKey)key, out value);
			return value;
		}

		public static int? IndexOf<T>(SCG.IList<T> collection, object value)
		{
			if (value != null && !(value is T))
				return null;
			int index = collection.IndexOf((T)value);
			return index >= 0 ? index : (int?)null;
		}

		public static bool Add<T>(SCG.ICollection<T> collection, T value)
		{
			collection.Add(value);
			return true;
		}

		public static bool Add<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, TKey key, TValue value)
		{
			collection.Add(key, value);
			return true;
		}

		public static bool Add<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, IAssociation<TKey, TValue> value)
		{
			collection.Add(Association.ToKeyValuePair(value));
			return true;
		}

		public static bool Remove<T>(SCG.ICollection<T> collection, object value)
		{
			if (value != null && !(value is T))
				return false;
			return collection.Remove((T)value);
		}

		public static bool Remove<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object value)
		{
			if (value != null && !(value is IAssociation<TKey, TValue>))
				return false;
			var kv = Association.ToKeyValuePair((IAssociation<TKey, TValue>)value);
			return ((SCG.ICollection<SCG.KeyValuePair<TKey, TValue>>)collection).Remove(kv);
		}

		public static bool RemoveKey<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection, object key)
		{
			if (key != null && !(key is TKey))
				return false;
			return collection.Remove((TKey)key);
		}

		public static void Clear<T>(SCG.ICollection<T> collection)
		{
			collection.Clear();
		}

		public static bool Insert<T>(SCG.IList<T> collection, int index, T value)
		{
			collection.Insert(index, value);
			return true;
		}

		public static bool RemoveAt<T>(SCG.IList<T> collection, int index)
		{
			collection.RemoveAt(index);
			return true;
		}

		public static ICollection<TKey> GetKeys<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection)
		{
			return Adapter.FromDotNet(collection.Keys);
		}

		public static ICollection<TValue> GetValues<TKey, TValue>(SCG.IDictionary<TKey, TValue> collection)
		{
			return Adapter.FromDotNet(collection.Values);
		}
	}
}
