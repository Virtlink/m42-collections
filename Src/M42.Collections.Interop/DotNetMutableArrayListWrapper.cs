﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="SCG.IList{T}"/>
	/// in a <see cref="IMutableArray{T}"/>.
	/// </summary>
	internal class DotNetMutableArrayListWrapper<T> : DotNetMutableCollectionWrapper<T>, IMutableArray<T>
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly SCG.IList<T> wrapped;


		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get { return this[index]; }
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get { return this[index]; }
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get { return DotNetWrapperBase.GetItem(wrapped, index); }
			set { DotNetWrapperBase.SetItem(wrapped, index, value); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DotNetMutableArrayListWrapper{T}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped list.</param>
		internal DotNetMutableArrayListWrapper(SCG.IList<T> wrapped)
			: base(wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
		}
		#endregion

		/// <inheritdoc />
		public virtual int? IndexOf(object value)
		{ return DotNetWrapperBase.IndexOf(wrapped, value); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
