﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="IMutableCollection{T}"/>
	/// in a <see cref="SCG.ICollection{T}"/>.
	/// </summary>
	internal sealed class MutableCollectionWrapper<T> : SCG.ICollection<T>
	{
		/// <summary>
		/// The wrapped collection.
		/// </summary>
		private readonly IMutableCollection<T> wrapped;

		/// <inheritdoc />
		public int Count
		{
			get { return DotNetInteropHelper.GetCount(this.wrapped); }
		}

		/// <inheritdoc />
		public bool IsReadOnly
		{
			get { return DotNetInteropHelper.GetIsReadOnly(this.wrapped); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="MutableCollectionWrapper{T}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped collection.</param>
		internal MutableCollectionWrapper(IMutableCollection<T> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
		}
		#endregion

		/// <inheritdoc />
		public void Add(T value)
		{ DotNetInteropHelper.Add(this.wrapped, value); }

		/// <inheritdoc />
		public bool Remove(T value)
		{ return DotNetInteropHelper.Remove(this.wrapped, value); }

		/// <inheritdoc />
		public void Clear()
		{ DotNetInteropHelper.Clear(this.wrapped); }



		/// <inheritdoc />
		public bool Contains(T item)
		{ return DotNetInteropHelper.Contains(this.wrapped, item); }

		/// <inheritdoc />
		public void CopyTo(T[] array, int arrayIndex)
		{ DotNetInteropHelper.CopyTo(this.wrapped, array, arrayIndex); }

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{ return DotNetInteropHelper.GetEnumerator(this.wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
