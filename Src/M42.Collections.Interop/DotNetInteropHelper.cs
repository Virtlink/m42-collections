﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Implementations for interoperability from M42 to BCL collections.
	/// </summary>
	internal static class DotNetInteropHelper
	{
		public static int GetCount<T>(ICollection<T> collection)
		{
			return collection.Count;
		}

		public static bool GetIsReadOnly<T>(ICollection<T> collection)
		{
			return collection.IsFrozen;
		}

		public static TValue GetItem<TKey, TValue>(IKeyedCollection<TKey, TValue> collection, TKey key)
		{
			return collection[key];
		}

		public static void SetItem<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> collection, TKey key, TValue value)
		{
			collection[key] = value;
		}

		public static T GetItem<T>(IList<T> collection, int index)
		{
			return collection[index];
		}

		public static void SetItem<T>(IMutableList<T> collection, int index, T value)
		{
			collection[index] = value;
		}

		public static SCG.ICollection<TKey> GetKeys<TKey, TValue>(IKeyedCollection<TKey, TValue> collection)
		{
			return Adapter.ToDotNet(collection.Keys);
		}

		public static SCG.ICollection<TValue> GetValues<TKey, TValue>(IKeyedCollection<TKey, TValue> collection)
		{
			return Adapter.ToDotNet(collection.Values);
		}

		public static bool ContainsKey<TKey, TValue>(IKeyedCollection<TKey, TValue> collection, TKey key)
		{
			return collection.ContainsKey(key);
		}

		public static bool TryGetValue<TKey, TValue>(IKeyedCollection<TKey, TValue> collection, TKey key, out TValue value)
		{
			bool present;
			value = collection.GetValue(key, out present);
			return present;
		}

		public static bool Contains<T>(ICollection<T> collection, T item)
		{
			return collection.Contains(item);
		}

		public static bool Contains<TKey, TValue>(IKeyedCollection<TKey, TValue> collection, SCG.KeyValuePair<TKey, TValue> item)
		{
			return collection.Contains(Association.FromKeyValuePair(item));
		}

		public static void CopyTo<T>(SCG.IEnumerable<T> collection, T[] array, int arrayIndex)
		{
			int i = 0;
			foreach (var item in collection)
			{
				array[arrayIndex + i] = item;
				i++;
			}
		}

		public static void CopyTo<T>(ICollection<T> collection, T[] array, int arrayIndex)
		{
			CopyTo((SCG.IEnumerable<T>)collection, array, arrayIndex);
		}

		public static void CopyTo<TKey, TValue>(IKeyedCollection<TKey, TValue> collection, SCG.KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			CopyTo(collection.Select(a => Association.ToKeyValuePair(a)), array, arrayIndex);
		}

		public static SCG.IEnumerator<SCG.KeyValuePair<TKey, TValue>> GetEnumerator<TKey, TValue>(IKeyedCollection<TKey, TValue> collection)
		{
			var kvPairs = collection.Select(a => Association.ToKeyValuePair(a));
			return GetEnumerator(kvPairs);
		}

		public static SCG.IEnumerator<T> GetEnumerator<T>(ICollection<T> collection)
		{
			return GetEnumerator(collection);
		}

		public static SCG.IEnumerator<T> GetEnumerator<T>(SCG.IEnumerable<T> collection)
		{
			return collection.GetEnumerator();
		}

		public static void Add<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> collection, TKey key, TValue value)
		{
			collection.Add(key, value);
		}

		public static void Add<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> collection, SCG.KeyValuePair<TKey, TValue> item)
		{
			Add(collection, item.Key, item.Value);
		}

		public static void Add<T>(IMutableCollection<T> collection, T item)
		{
			collection.Add(item);
		}

		public static bool Remove<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> collection, TKey item)
		{
			return collection.Remove(item);
		}

		public static bool Remove<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> collection, SCG.KeyValuePair<TKey, TValue> item)
		{
			return Remove(collection, Association.FromKeyValuePair(item));
		}

		public static bool Remove<T>(IMutableCollection<T> collection, T item)
		{
			return collection.Remove(item);
		}

		public static void Clear<T>(IMutableCollection<T> collection)
		{
			collection.Clear();
		}

		public static void Insert<T>(IMutableList<T> collection, int index, T item)
		{
			collection.Insert(index, item);
		}

		public static void RemoveAt<T>(IMutableList<T> collection, int index)
		{
			collection.RemoveAt(index);
		}

		public static int IndexOf<T>(IList<T> collection, T item)
		{
			return collection.IndexOf(item) ?? -1;
		}
	}
}
