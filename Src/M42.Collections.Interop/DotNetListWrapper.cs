﻿using System;
using SCG = System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="SCG.IList{T}"/>
	/// in a <see cref="IList{T}"/>.
	/// </summary>
	internal sealed class DotNetListWrapper<T> : DotNetWrapperBase, IList<T>
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly SCG.IList<T> wrapped;

		/// <inheritdoc />
		public override T Unwrap<T>()
		{
			return (T)wrapped;
		}


		/// <inheritdoc />
		public int Count
		{
			get { return DotNetWrapperBase.GetCount(wrapped); }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return DotNetWrapperBase.GetIsEmpty(wrapped); }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return DotNetWrapperBase.GetIsFrozen(wrapped); }
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get { return DotNetWrapperBase.GetItem(wrapped, index); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DotNetListWrapper{T}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped list.</param>
		internal DotNetListWrapper(SCG.IList<T> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
		}
		#endregion

		/// <inheritdoc />
		public int? IndexOf(object value)
		{ return DotNetWrapperBase.IndexOf(wrapped, value); }

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetMember(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public T GetMember(object value)
		{ return DotNetWrapperBase.GetMember(wrapped, value); }

		/// <inheritdoc />
		public bool Contains(object value)
		{ return DotNetWrapperBase.Contains(wrapped, value); }

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{ return DotNetWrapperBase.IsValidMember(wrapped, value); }

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{ return DotNetWrapperBase.GetEnumerator(wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
