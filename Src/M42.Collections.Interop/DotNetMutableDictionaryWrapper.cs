﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="SCG.IDictionary{TKey, TValue}"/>
	/// in a <see cref="IMutableKeyedCollection{TKey, TValue}"/>.
	/// </summary>
	internal sealed class DotNetMutableDictionaryWrapper<TKey, TValue> : DotNetWrapperBase, IMutableKeyedCollection<TKey, TValue>
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly SCG.IDictionary<TKey, TValue> wrapped;


		/// <inheritdoc />
		public int Count
		{
			get { return DotNetWrapperBase.GetCount(wrapped); }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return DotNetWrapperBase.GetIsEmpty(wrapped); }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return DotNetWrapperBase.GetIsFrozen(wrapped); }
		}

		private readonly ICollection<TKey> keys;
		/// <inheritdoc />
		public ICollection<TKey> Keys
		{
			get { return this.keys; }
		}

		private readonly ICollection<TValue> values;
		/// <inheritdoc />
		public ICollection<TValue> Values
		{
			get { return this.values; }
		}

		/// <inheritdoc />
		public TValue this[object key]
		{
			get { return DotNetWrapperBase.GetItem(wrapped, key); }
		}

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get { return DotNetWrapperBase.GetItem(wrapped, key); }
			set { DotNetWrapperBase.SetItem(wrapped, key, value); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DotNetMutableDictionaryWrapper{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped list.</param>
		internal DotNetMutableDictionaryWrapper(SCG.IDictionary<TKey, TValue> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
			this.keys = DotNetWrapperBase.GetKeys(wrapped);
			this.values = DotNetWrapperBase.GetValues(wrapped);
		}
		#endregion

		/// <inheritdoc />
		public bool Add(TKey key, TValue value)
		{ return DotNetWrapperBase.Add(wrapped, key, value); }

		/// <inheritdoc />
		public bool Add(IAssociation<TKey, TValue> value)
		{ return DotNetWrapperBase.Add(wrapped, value); }

		/// <inheritdoc />
		public bool Remove(object value)
		{ return DotNetWrapperBase.Remove(wrapped, value); }

		/// <inheritdoc />
		public bool RemoveKey(object value)
		{ return DotNetWrapperBase.RemoveKey(wrapped, value); }

		/// <inheritdoc />
		public void Clear()
		{ DotNetWrapperBase.Clear(wrapped); }



		/// <inheritdoc />
		public IAssociation<TKey, TValue> GetMember(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetMember(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public IAssociation<TKey, TValue> GetMember(object value)
		{ return DotNetWrapperBase.GetMember(wrapped, value); }

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{ return GetMember(value, out isPresent); }

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{ return GetMember(value); }

		/// <inheritdoc />
		public TValue GetValue(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetValue(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public TValue GetValue(object value)
		{ return DotNetWrapperBase.GetValue(wrapped, value); }

		/// <inheritdoc />
		public bool Contains(object value)
		{ return DotNetWrapperBase.Contains(wrapped, value); }

		/// <inheritdoc />
		public bool ContainsKey(object value)
		{ return DotNetWrapperBase.ContainsKey(wrapped, value); }

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{ return DotNetWrapperBase.IsValidMember(wrapped, value); }

		/// <inheritdoc />
		public bool IsValidKey(object key)
		{ return DotNetWrapperBase.IsValidKey(wrapped, key); }

		/// <inheritdoc />
		public bool IsValidValue(object value)
		{ return DotNetWrapperBase.IsValidValue(wrapped, value); }

		/// <inheritdoc />
		public SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator()
		{ return DotNetWrapperBase.GetEnumerator(wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
