﻿using System;
using SCG = System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="SCG.IDictionary{TKey, TValue}"/>
	/// in a <see cref="IMutableKeyedCollection{T}"/>.
	/// </summary>
	internal sealed class DotNetDictionaryWrapper<TKey, TValue> : DotNetWrapperBase, IKeyedCollection<TKey, TValue>
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly SCG.IDictionary<TKey, TValue> wrapped;

		/// <inheritdoc />
		public override T Unwrap<T>()
		{
			return (T)wrapped;
		}


		/// <inheritdoc />
		public int Count
		{
			get { return DotNetWrapperBase.GetCount(wrapped); }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return DotNetWrapperBase.GetIsEmpty(wrapped); }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return DotNetWrapperBase.GetIsFrozen(wrapped); }
		}

		private readonly ICollection<TKey> keys;
		/// <inheritdoc />
		public ICollection<TKey> Keys
		{
			get { return this.keys; }
		}

		private readonly ICollection<TValue> values;
		/// <inheritdoc />
		public ICollection<TValue> Values
		{
			get { return this.values; }
		}

		/// <inheritdoc />
		public TValue this[object key]
		{
			get { return DotNetWrapperBase.GetItem(wrapped, key); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DotNetDictionaryWrapper{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped list.</param>
		internal DotNetDictionaryWrapper(SCG.IDictionary<TKey, TValue> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
			this.keys = DotNetWrapperBase.GetKeys(wrapped);
			this.values = DotNetWrapperBase.GetValues(wrapped);
		}
		#endregion

		

		/// <inheritdoc />
		public IAssociation<TKey, TValue> GetMember(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetMember(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public IAssociation<TKey, TValue> GetMember(object value)
		{ return DotNetWrapperBase.GetMember(wrapped, value); }

		/// <inheritdoc />
		public TValue GetValue(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetValue(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public TValue GetValue(object value)
		{ return DotNetWrapperBase.GetValue(wrapped, value); }

		/// <inheritdoc />
		public bool Contains(object value)
		{ return DotNetWrapperBase.Contains(wrapped, value); }

		/// <inheritdoc />
		public bool ContainsKey(object value)
		{ return DotNetWrapperBase.ContainsKey(wrapped, value); }

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{ return DotNetWrapperBase.IsValidMember(wrapped, value); }

		/// <inheritdoc />
		public bool IsValidKey(object key)
		{ return DotNetWrapperBase.IsValidKey(wrapped, key); }

		/// <inheritdoc />
		public bool IsValidValue(object value)
		{ return DotNetWrapperBase.IsValidValue(wrapped, value); }

		/// <inheritdoc />
		public SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator()
		{ return DotNetWrapperBase.GetEnumerator(wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
