﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="IMutableKeyedCollection{TKey, TValue}"/>
	/// in a <see cref="SCG.IDictionary{TKey, TValue}"/>.
	/// </summary>
	internal sealed class MutableKeyedCollectionWrapper<TKey, TValue> : SCG.IDictionary<TKey, TValue>
	{
		/// <summary>
		/// The wrapped mutable keyed collection.
		/// </summary>
		private readonly IMutableKeyedCollection<TKey, TValue> wrapped;

		/// <inheritdoc />
		public int Count
		{
			get { return DotNetInteropHelper.GetCount(this.wrapped); }
		}

		/// <inheritdoc />
		public bool IsReadOnly
		{
			get { return DotNetInteropHelper.GetIsReadOnly(this.wrapped); }
		}

		/// <inheritdoc />
		public SCG.ICollection<TKey> Keys
		{
			get { return DotNetInteropHelper.GetKeys(this.wrapped); }
		}

		/// <inheritdoc />
		public SCG.ICollection<TValue> Values
		{
			get { return DotNetInteropHelper.GetValues(this.wrapped); }
		}

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get { return DotNetInteropHelper.GetItem(this.wrapped, key); }
			set { DotNetInteropHelper.SetItem(this.wrapped, key, value); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="MutableKeyedCollectionWrapper{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped mutable keyed collection.</param>
		internal MutableKeyedCollectionWrapper(IMutableKeyedCollection<TKey, TValue> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
		}
		#endregion

		/// <inheritdoc />
		public void Add(TKey key, TValue value)
		{ DotNetInteropHelper.Add(this.wrapped, key, value); }

		/// <inheritdoc />
		public void Add(SCG.KeyValuePair<TKey, TValue> item)
		{ DotNetInteropHelper.Add(this.wrapped, item); }

		/// <inheritdoc />
		public bool Remove(TKey key)
		{ return DotNetInteropHelper.Remove(this.wrapped, key); }

		/// <inheritdoc />
		public bool Remove(SCG.KeyValuePair<TKey, TValue> item)
		{ return DotNetInteropHelper.Remove(this.wrapped, item); }

		/// <inheritdoc />
		public void Clear()
		{ DotNetInteropHelper.Clear(this.wrapped); }



		/// <inheritdoc />
		public bool ContainsKey(TKey key)
		{ return DotNetInteropHelper.ContainsKey(this.wrapped, key); }

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value)
		{ return DotNetInteropHelper.TryGetValue(this.wrapped, key, out value); }
		
		/// <inheritdoc />
		public bool Contains(SCG.KeyValuePair<TKey, TValue> item)
		{ return DotNetInteropHelper.Contains(this.wrapped, item); }

		/// <inheritdoc />
		public void CopyTo(SCG.KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{ DotNetInteropHelper.CopyTo(this.wrapped, array, arrayIndex); }

		/// <inheritdoc />
		public SCG.IEnumerator<SCG.KeyValuePair<TKey, TValue>> GetEnumerator()
		{ return DotNetInteropHelper.GetEnumerator(this.wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
