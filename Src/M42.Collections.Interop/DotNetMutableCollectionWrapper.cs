﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interop
{
	/// <summary>
	/// Wraps a <see cref="SCG.ICollection{T}"/>
	/// in a <see cref="IMutableCollection{T}"/>.
	/// </summary>
	internal class DotNetMutableCollectionWrapper<T> : DotNetWrapperBase, IMutableCollection<T>
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly SCG.ICollection<T> wrapped;


		/// <inheritdoc />
		public virtual int Count
		{
			get { return DotNetWrapperBase.GetCount(wrapped); }
		}

		/// <inheritdoc />
		public virtual bool IsEmpty
		{
			get { return DotNetWrapperBase.GetIsEmpty(wrapped); }
		}

		/// <inheritdoc />
		public virtual bool IsFrozen
		{
			get { return DotNetWrapperBase.GetIsFrozen(wrapped); }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DotNetMutableCollectionWrapper{T}"/> class.
		/// </summary>
		/// <param name="wrapped">The wrapped collection.</param>
		internal DotNetMutableCollectionWrapper(SCG.ICollection<T> wrapped)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrapped != null);
			#endregion
			this.wrapped = wrapped;
		}
		#endregion

		/// <inheritdoc />
		public virtual bool Add(T value)
		{ return DotNetWrapperBase.Add(wrapped, value); }

		/// <inheritdoc />
		public virtual bool Remove(object value)
		{ return DotNetWrapperBase.Remove(wrapped, value); }

		/// <inheritdoc />
		public virtual void Clear()
		{ DotNetWrapperBase.Clear(wrapped); }



		/// <inheritdoc />
		public virtual T GetMember(object value, out bool isPresent)
		{ return DotNetWrapperBase.GetMember(wrapped, value, out isPresent); }

		/// <inheritdoc />
		public virtual T GetMember(object value)
		{ return DotNetWrapperBase.GetMember(wrapped, value); }

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{ return GetMember(value, out isPresent); }

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{ return GetMember(value); }

		/// <inheritdoc />
		public virtual bool Contains(object value)
		{ return DotNetWrapperBase.Contains(wrapped, value); }

		/// <inheritdoc />
		public virtual bool IsValidMember(object value)
		{ return DotNetWrapperBase.IsValidMember(wrapped, value); }

		/// <inheritdoc />
		public virtual SCG.IEnumerator<T> GetEnumerator()
		{ return DotNetWrapperBase.GetEnumerator(wrapped); }

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{ return GetEnumerator(); }

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrapped != null);
		}
		#endregion
	}
}
