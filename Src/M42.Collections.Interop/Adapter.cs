﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Interop;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// Adapter methods.
	/// </summary>
	public static class Adapter
	{
		/// <summary>
		/// Adapts an M42 <see cref="ICollection{T}"/> to a .Net <see cref="SCG.ICollection{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="collection">The M42 collection; or <see langword="null"/>.</param>
		/// <returns>The .NET collection; or <see langword="null"/>.</returns>
		public static SCG.ICollection<T> ToDotNet<T>(ICollection<T> collection)
		{
			if (collection == null)
				return null;
			return new CollectionWrapper<T>(collection);
		}

		/// <summary>
		/// Adapts an M42 <see cref="IMutableCollection{T}"/> to a .Net <see cref="SCG.ICollection{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="collection">The M42 collection; or <see langword="null"/>.</param>
		/// <returns>The .NET collection; or <see langword="null"/>.</returns>
		public static SCG.ICollection<T> ToDotNet<T>(IMutableCollection<T> collection)
		{
			if (collection == null)
				return null;
			return new MutableCollectionWrapper<T>(collection);
		}

		/// <summary>
		/// Adapts an M42 <see cref="IList{T}"/> to a .Net <see cref="SCG.IList{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="list">The M42 list; or <see langword="null"/>.</param>
		/// <returns>The .NET list; or <see langword="null"/>.</returns>
		public static SCG.IList<T> ToDotNet<T>(IList<T> list)
		{
			if (list == null)
				return null;
			return new ListWrapper<T>(list);
		}

		/// <summary>
		/// Adapts an M42 <see cref="IMutableList{T}"/> to a .Net <see cref="SCG.IList{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="list">The M42 list; or <see langword="null"/>.</param>
		/// <returns>The .NET list; or <see langword="null"/>.</returns>
		public static SCG.IList<T> ToDotNet<T>(IMutableList<T> list)
		{
			if (list == null)
				return null;
			return new MutableListWrapper<T>(list);
		}

		/// <summary>
		/// Adapts an M42 <see cref="IKeyedCollection{TKey, TValue}"/> to a .Net <see cref="SCG.IDictionary{TKey, TValue}"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of keys.</typeparam>
		/// <typeparam name="TValue">The type of values.</typeparam>
		/// <param name="keyedCollection">The M42 keyed collection; or <see langword="null"/>.</param>
		/// <returns>The .NET keyed collection; or <see langword="null"/>.</returns>
		public static SCG.IDictionary<TKey, TValue> ToDotNet<TKey, TValue>(IKeyedCollection<TKey, TValue> keyedCollection)
		{
			if (keyedCollection == null)
				return null;
			return new KeyedCollectionWrapper<TKey, TValue>(keyedCollection);
		}

		/// <summary>
		/// Adapts an M42 <see cref="IMutableKeyedCollection{TKey, TValue}"/> to a .Net <see cref="SCG.IDictionary{TKey, TValue}"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of keys.</typeparam>
		/// <typeparam name="TValue">The type of values.</typeparam>
		/// <param name="keyedCollection">The M42 keyed collection; or <see langword="null"/>.</param>
		/// <returns>The .NET keyed collection; or <see langword="null"/>.</returns>
		public static SCG.IDictionary<TKey, TValue> ToDotNet<TKey, TValue>(IMutableKeyedCollection<TKey, TValue> keyedCollection)
		{
			if (keyedCollection == null)
				return null;
			return new MutableKeyedCollectionWrapper<TKey, TValue>(keyedCollection);
		}



		/// <summary>
		/// Adapts a .NET <see cref="SCG.ICollection{T}"/> to
		/// an M42 <see cref="IMutableCollection{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="collection">The .NET collection; or <see langword="null"/>.</param>
		/// <returns>The M42 collection; or <see langword="null"/>.</returns>
		public static IMutableCollection<T> FromDotNet<T>(SCG.ICollection<T> collection)
		{
			if (collection == null)
				return null;
			return new DotNetMutableCollectionWrapper<T>(collection);
		}

		/// <summary>
		/// Adapts a .NET <see cref="SCG.IList{T}"/> to
		/// an M42 <see cref="IMutableList{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="list">The .NET list; or <see langword="null"/>.</param>
		/// <returns>The M42 list; or <see langword="null"/>.</returns>
		public static IMutableList<T> FromDotNet<T>(SCG.IList<T> list)
		{
			if (list == null)
				return null;
			return new DotNetMutableListWrapper<T>(list);
		}

		/// <summary>
		/// Adapts a .NET <see cref="SCG.IDictionary{TKey, TValue}"/> to
		/// an M42 <see cref="IMutableKeyedCollection{TKey, TValue}"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of keys.</typeparam>
		/// <typeparam name="TValue">The type of values.</typeparam>
		/// <param name="dictionary">The .NET dictionary; or <see langword="null"/>.</param>
		/// <returns>The M42 keyed collection; or <see langword="null"/>.</returns>
		public static IMutableKeyedCollection<TKey, TValue> FromDotNet<TKey, TValue>(SCG.IDictionary<TKey, TValue> dictionary)
		{
			if (dictionary == null)
				return null;
			return new DotNetMutableDictionaryWrapper<TKey, TValue>(dictionary);
		}
	}
}
