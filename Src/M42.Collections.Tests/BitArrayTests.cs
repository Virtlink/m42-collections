﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Specialized;
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests
{
	public class BitArrayTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static BitArray CreateSUT(params bool[] data)
		{
			var sut = new BitArray(data.Length);
			for (int i = 0; i < data.Length; i++)
			{
				sut[i] = data[i];
			}
			return sut;
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<bool>
		{
			protected override ICollection<bool> CreateInstance(params bool[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IListTests : IListTests<bool>
		{
			protected override IList<bool> CreateInstance(params bool[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableArrayTests : IMutableArrayTests<bool>
		{
			protected override IMutableArray<bool> CreateInstance(params bool[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGIListTests : SCGIListTests<bool>
		{
			protected override SCG.IList<bool> CreateInstance(params bool[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGICollectionTests : SCGICollectionTests<bool>
		{
			protected override SCG.ICollection<bool> CreateInstance(params bool[] data)
			{
				return CreateSUT(data);
			}
		}
	}
}
