﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Common utility methods for working with tests.
	/// </summary>
	public static class Common
	{
		/// <summary>
		/// Expects the specified object to be of the specified type.
		/// </summary>
		/// <typeparam name="TResult">The type to convert <paramref name="obj"/> to.</typeparam>
		/// <param name="obj">The object.</param>
		/// <returns><paramref name="obj"/> converted to <typeparamref name="TResult"/>.</returns>
		/// <remarks>
		/// When <paramref name="obj"/> cannot be converted to <typeparamref name="TResult"/>,
		/// this method asserts <see cref="Assert.Pass"/>, ending the test.
		/// </remarks>
		public static TResult ExpectOfType<TResult>(object obj)
		{
			if (!(obj is TResult))
				Assert.Pass();
			return (TResult)obj;
		}

		public static IEnumerable<TKey> Keys<TKey, TValue>(this IEnumerable<IAssociation<TKey, TValue>> source)
		{
			return source.Select(kv => kv.Key);
		}

		public static IEnumerable<TValue> Values<TKey, TValue>(this IEnumerable<IAssociation<TKey, TValue>> source)
		{
			return source.Select(kv => kv.Value);
		}
	}
}
