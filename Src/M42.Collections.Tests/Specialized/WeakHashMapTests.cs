﻿using M42.Collections.Specialized;
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M42.Collections.Tests.Specialized
{
	public class WeakHashMapTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static WeakHashMap<TKey, TValue> CreateSUT<TKey, TValue>(params IAssociation<TKey, TValue>[] data)
			where TValue : class
		{
			var set = new WeakHashMap<TKey, TValue>();
			foreach (var kv in data)
			{
				set.Add(kv.Key, kv.Value);
			}
			return set;
		}


		[TestFixture]
		public class InnerWeakHashMapTests : TestClass
		{
			protected WeakHashMap<string, string> CreateInstance(params IAssociation<string, string>[] data)
			{
				return CreateSUT(data);
			}

			[Test]
			public void TryGetValue_ObjectPresent_GetsValue()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var value = Data.Get<string>();
				var sut = CreateInstance(data);
				sut.Add(key, value);

				// Act
				string result;
				bool success = sut.TryGetValue(key, out result);

				// Assert
				Assert.IsTrue(success);
				Assert.AreSame(value, result);

				GC.KeepAlive(value);
			}

			[Test]
			public void TryGetValue_ObjectCollected_GetsNoValue()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var sut = CreateInstance(data);
				sut.Add(key, Data.Get<string>());
				GC.Collect();

				// Act
				string result;
				bool success = sut.TryGetValue(key, out result);

				// Assert
				Assert.IsFalse(success);
			}

			[Test]
			public void TryGetValue_KeyNotPresent_GetsNoValue()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var sut = CreateInstance(data);

				// Act
				string result;
				bool success = sut.TryGetValue(key, out result);

				// Assert
				Assert.IsFalse(success);
			}

			[Test]
			public void Add_OneItem_AddsCorrectly()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var value = Data.Get<string>();
				var sut = CreateInstance(data);

				// Act
				bool success = sut.Add(key, value);

				// Assert
				string result;
				sut.TryGetValue(key, out result);
				Assert.IsTrue(success);
				Assert.AreSame(value, result);

				GC.KeepAlive(value);
			}

			[Test]
			public void Add_ExistingKey_DoesNotAdd()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var key2 = Data.Clone(key);
				var value = Data.Get<string>();
				var sut = CreateInstance(data);
				sut.Add(key, value);

				// Act
				bool success = sut.Add(key, Data.Get<string>());

				// Assert
				Assert.IsFalse(success);

				GC.KeepAlive(value);
			}

			[Test]
			public void Put_OneItem_AddsCorrectly()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var key = Data.GetDistinct<string>(data.Keys());
				var value = Data.Get<string>();
				var sut = CreateInstance(data);

				// Act
				bool success = sut.Put(key, value);

				// Assert
				string result;
				sut.TryGetValue(key, out result);
				Assert.IsTrue(success);
				Assert.AreSame(value, result);

				GC.KeepAlive(value);
			}

			[Test]
			public void Put_ExistingKey_ReplacesKey()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var sut = CreateInstance(data);
				var key = Data.GetDistinct<string>(data.Keys());
				sut.Add(key, Data.Get<string>());
				var value = Data.Get<string>();

				// Act
				bool success = sut.Put(key, value);

				// Assert
				string result;
				sut.TryGetValue(key, out result);
				Assert.IsTrue(success);
				Assert.AreSame(value, result);

				GC.KeepAlive(value);
			}

			[Test]
			public void RemoveKey_ExistingKey_ReturnsTrue()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var sut = CreateInstance(data);
				var key = Data.GetDistinct<string>(data.Keys());
				var value = Data.Get<string>();
				sut.Add(key, value);

				// Act
				bool success = sut.RemoveKey(key);

				// Assert
				string result;
				sut.TryGetValue(key, out result);
				Assert.IsTrue(success);
				Assert.IsNull(result);

				GC.KeepAlive(value);
			}

			[Test]
			public void RemoveKey_UnknownKey_ReturnsFalse()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var sut = CreateInstance(data);
				var key = Data.GetDistinct<string>(data.Keys());

				// Act
				bool success = sut.RemoveKey(key);

				// Assert
				Assert.IsFalse(success);
			}

			[Test]
			public void Clear_RemovedAllKeys()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var sut = CreateInstance(data);
				var key1 = Data.GetDistinct<string>(data.Keys());
				var key2 = Data.GetDistinct<string>(data.Keys());
				var key3 = Data.GetDistinct<string>(data.Keys());
				sut.Put(key1, Data.Get<string>());
				sut.Put(key2, Data.Get<string>());
				sut.Put(key3, Data.Get<string>());

				// Act
				sut.Clear();

				// Assert
				string result;
				Assert.IsFalse(sut.TryGetValue(key1, out result));
				Assert.IsFalse(sut.TryGetValue(key2, out result));
				Assert.IsFalse(sut.TryGetValue(key3, out result));
			}

			[Test]
			public void GetEnumerator_IncludesOnlyActiveKeys()
			{
				// Arrange
				var data = Data.GetKeyValuePairArray<string, string>(3).ToAssociations();
				var sut = CreateInstance(data);
				var key1 = Data.GetDistinct<string>(data.Keys());
				var key2 = Data.GetDistinct<string>(data.Keys());
				sut.Put(key1, Data.Get<string>());
				sut.Put(key2, Data.Get<string>());
				sut.Put(Data.GetDistinct<string>(data.Keys()), Data.Get<string>());
				GC.Collect();

				// Act
				var result = new List<IAssociation<string, string>>();
				foreach (var pair in sut)
				{
					result.Add(pair);
				}

				// Assert
				Assert.AreEqual(2, result.Count);

				GC.KeepAlive(key1);
				GC.KeepAlive(key2);
			}
		}
	}
}
