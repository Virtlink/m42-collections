﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M42.Collections.Tests.Support
{
	public static class AssociationExt
	{
		public static IAssociation<TKey, TValue>[] ToAssociations<TKey, TValue>(this KeyValuePair<TKey, TValue>[] pairs)
		{
			Contract.Requires<ArgumentNullException>(pairs != null);
			IAssociation<TKey, TValue>[] array = new IAssociation<TKey, TValue>[pairs.Length];
			for(int i = 0; i < pairs.Length; i++)
			{
				array[i] = Association.Create(pairs[i].Key, pairs[i].Value);
			}
			return array;
		}
	}
}
