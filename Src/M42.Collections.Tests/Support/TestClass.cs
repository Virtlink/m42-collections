﻿using M42.Testing;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M42.Collections.Tests.Support
{
	/// <summary>
	/// Base class for test classes.
	/// </summary>
	public abstract class TestClass
	{
		/// <summary>
		/// Setups the test class.
		/// </summary>
		[SetUp]
		public void Setup()
		{
			this.data = CreateDataProvider();
		}

		/// <summary>
		/// Creates the test data provider.
		/// </summary>
		/// <returns>The test data provider.</returns>
		protected virtual TestDataProvider CreateDataProvider()
		{
			var provider = new TestDataProvider(0);
			provider.With<IAssociation<string, string>>(p => Association.Create(p.Get<string>(), p.Get<string>()));
			return provider;
		}

		private TestDataProvider data;
		/// <summary>
		/// Gets the test data provider.
		/// </summary>
		/// <value>A <see cref="TestDataProvider"/>.</value>
		protected TestDataProvider Data
		{
			get { return this.data; }
		}

		protected void AssertAreSameVT(object expected, object actual)
		{
			if (expected.GetType().IsValueType && actual.GetType().IsValueType)
				Assert.AreEqual(expected, actual);
			else
				Assert.AreSame(expected, actual);
		}

		protected void AssertAreNotSameVT(object expected, object actual)
		{
			if (expected.GetType().IsValueType && actual.GetType().IsValueType)
				Assert.AreNotEqual(expected, actual);
			else
				Assert.AreNotSame(expected, actual);
		}
	}

}
