﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests
{
	public class HashSetTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static HashSet<T> CreateSUT<T>(params T[] data)
		{
			var set = new HashSet<T>();
			foreach (var element in data)
			{
				set.Add(element);
			}
			return set;
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<string>
		{
			protected override ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class ISetTests : ISetTests<string>
		{
			protected override ISet<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableSetTests : IMutableSetTests<string>
		{
			protected override IMutableSet<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

#if false
		[TestFixture]
		public class SCGISetTests : SCGISetTests<string>
		{
			protected override SCG.ISet<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGMutableISetTests : SCGMutableISetTests<string>
		{
			protected override SCG.ISet<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGICollectionTests : SCGICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}
#endif
	}
}
