﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.ObjectModel;
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests.ObjectModel
{
	public class MutableCollectionBaseTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static MutableCollectionBase<T> CreateSUT<T>(params T[] data)
		{
			return new Stub<T>(new ArrayList<T>(data));
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<string>
		{
			protected override ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableCollectionTests : IMutableCollectionTests<string>
		{
			protected override IMutableCollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

#if false
		[TestFixture]
		public class SCGICollectionTests : SCGICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGMutableICollectionTests : SCGMutableICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}
#endif

		private sealed class Stub<T> : MutableCollectionBase<T>
		{
			private readonly IMutableCollection<T> innerCollection;

			/// <summary>
			/// Initializes a new instance of the <see cref="Stub"/> class.
			/// </summary>
			public Stub(IMutableCollection<T> innerCollection)
				: base(SCG.EqualityComparer<T>.Default)
			{
				this.innerCollection = innerCollection;
			}

			public override int Count
			{
				get { return this.innerCollection.Count; }
			}

			protected override void ClearElements()
			{
				this.innerCollection.Clear();
			}

			protected override bool AddElement(T value)
			{
				return this.innerCollection.Add(value);
			}

			protected override bool RemoveElement(object value)
			{
				return this.innerCollection.Remove(value);
			}

			protected override bool TryGetMemberElement(object value, out T result)
			{
				bool isPresent;
				result = this.innerCollection.GetMember(value, out isPresent);
				return isPresent;
			}

			public override SCG.IEnumerator<T> GetEnumerator()
			{
				return this.innerCollection.GetEnumerator();
			}
		}
	}
}
