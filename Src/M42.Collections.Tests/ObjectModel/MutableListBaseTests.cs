﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.ObjectModel;
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests.ObjectModel
{
	public class MutableListBaseTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static MutableListBase<T> CreateSUT<T>(params T[] data)
		{
			return new Stub<T>(new ArrayList<T>(data));
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<string>
		{
			protected override ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IListTests : IListTests<string>
		{
			protected override IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableArrayTests : IMutableArrayTests<string>
		{
			protected override IMutableArray<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableCollectionTests : IMutableCollectionTests<string>
		{
			protected override IMutableCollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableListTests : IMutableListTests<string>
		{
			protected override IMutableList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

#if false
		[TestFixture]
		public class SCGIListTests : SCGIListTests<string>
		{
			protected override SCG.IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGICollectionTests : SCGICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGMutableIListTests : SCGMutableIListTests<string>
		{
			protected override SCG.IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGMutableICollectionTests : SCGMutableICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}
#endif

		private sealed class Stub<T> : MutableListBase<T>
		{
			private readonly IMutableList<T> innerList;

			/// <summary>
			/// Initializes a new instance of the <see cref="Stub"/> class.
			/// </summary>
			public Stub(IMutableList<T> innerCollection)
				: base(SCG.EqualityComparer<T>.Default)
			{
				this.innerList = innerCollection;
			}

			public override int Count
			{
				get { return this.innerList.Count; }
			}

			protected override void ClearElements()
			{
				this.innerList.Clear();
			}

			protected override T GetElement(int index)
			{
				return this.innerList[index];
			}

			protected override void SetElement(int index, T value)
			{
				this.innerList[index] = value;
			}

			protected override bool InsertElement(int index, T value)
			{
				return this.innerList.Insert(index, value);
			}

			protected override bool RemoveElement(int index)
			{
				return this.innerList.RemoveAt(index);
			}

			protected override int? IndexOfElement(object value)
			{
				return this.innerList.IndexOf(value);
			}

			public override SCG.IEnumerator<T> GetEnumerator()
			{
				return this.innerList.GetEnumerator();
			}
		}
	}
}
