﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.ObjectModel;
using M42.Testing;
using NUnit.Framework;
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests.ObjectModel
{
	public class PossessionListTests
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static PossessionList<Owned, Owner> CreateSUT(params Owned[] data)
		{
			var owner = new Owner();
			var list = new Stub(owner);
			owner.Values = list;
			foreach(var d in data)
				list.Add(d);
			return list;
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<Owned>
		{
			protected override TestDataProvider CreateDataProvider()
			{
				var provider = base.CreateDataProvider();
				provider.With<Owned>(p => new Owned(p.GetString(10)));
				return provider;
			}

			protected override ICollection<Owned> CreateInstance(params Owned[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableCollectionTests : IMutableCollectionTests<Owned>
		{
			protected override TestDataProvider CreateDataProvider()
			{
				var provider = base.CreateDataProvider();
				provider.With<Owned>(p => new Owned(p.GetString(10)));
				return provider;
			}

			protected override IMutableCollection<Owned> CreateInstance(params Owned[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IListTests : IListTests<Owned>
		{
			protected override TestDataProvider CreateDataProvider()
			{
				var provider = base.CreateDataProvider();
				provider.With<Owned>(p => new Owned(p.GetString(10)));
				return provider;
			}

			protected override IList<Owned> CreateInstance(params Owned[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableArrayTests : IMutableArrayTests<Owned>
		{
			protected override TestDataProvider CreateDataProvider()
			{
				var provider = base.CreateDataProvider();
				provider.With<Owned>(p => new Owned(p.GetString(10)));
				return provider;
			}

			protected override IMutableArray<Owned> CreateInstance(params Owned[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableListTests : IMutableListTests<Owned>
		{
			protected override TestDataProvider CreateDataProvider()
			{
				var provider = base.CreateDataProvider();
				provider.With<Owned>(p => new Owned(p.GetString(10)));
				return provider;
			}

			protected override IMutableList<Owned> CreateInstance(params Owned[] data)
			{
				return CreateSUT(data);
			}
		}

		public sealed class Stub : PossessionList<Owned, Owner>
		{
			public Stub(Owner owner)
				: base(owner)
			{

			}

			protected override PossessionList<Owned, Owner> GetOwnerCollection(Owned item)
			{
				return item.Owner != null ? item.Owner.Values : null;
			}

			protected override void SetOwner(Owned item, Owner owner)
			{
				item.SetOwner(owner);
			}
		}

		/// <summary>
		/// An owner.
		/// </summary>
		public sealed class Owner
		{
			private Stub values;
			/// <summary>
			/// Gets the values.
			/// </summary>
			/// <value>The values.</value>
			public Stub Values
			{
				get { return this.values; }
				set { this.values = value; }
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="Owner"/> class.
			/// </summary>
			public Owner()
			{

			}
		}

		/// <summary>
		/// An owned object.
		/// </summary>
		public sealed class Owned : IEquatable<Owned>
		{
			private readonly string value;
			/// <summary>
			/// Gets the value.
			/// </summary>
			/// <value>A value; or <see langword="null" />.</value>
			public string Value
			{
				get { return this.value; }
			}

			private Owner owner;
			/// <summary>
			/// Gets the owner.
			/// </summary>
			/// <value>The owner; or <see langword="null" />.</value>
			public Owner Owner
			{
				get { return this.owner; }
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="Owned"/> class.
			/// </summary>
			public Owned(string value)
			{
				this.value = value;
			}

			#region Equality
			/// <inheritdoc />
			public bool Equals(Owned other)
			{
				if (Object.ReferenceEquals(other, null) ||      // When 'other' is null
					other.GetType() != this.GetType())          // or of a different type
					return false;                               // they are not equal.
				return this.value == other.value;
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				int hash = 17;
				unchecked
				{
					hash = hash * 29 + this.value != null ? this.value.GetHashCode() : 0;
				}
				return hash;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				return Equals(obj as Owned);
			}

			/// <summary>
			/// Returns a value that indicates whether two specified <see cref="Owned"/> objects are equal.
			/// </summary>
			/// <param name="left">The first object to compare.</param>
			/// <param name="right">The second object to compare.</param>
			/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are equal;
			/// otherwise, <see langword="false"/>.</returns>
			public static bool operator ==(Owned left, Owned right)
			{
				return Object.Equals(left, right);
			}

			/// <summary>
			/// Returns a value that indicates whether two specified <see cref="Owned"/> objects are not equal.
			/// </summary>
			/// <param name="left">The first object to compare.</param>
			/// <param name="right">The second object to compare.</param>
			/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are not equal;
			/// otherwise, <see langword="false"/>.</returns>
			public static bool operator !=(Owned left, Owned right)
			{
				return !(left == right);
			}
			#endregion

			/// <summary>
			/// Sets the owner.
			/// </summary>
			/// <param name="owner">The owner.</param>
			internal void SetOwner(Owner owner)
			{
				this.owner = owner;
			}
		}
	}
}
