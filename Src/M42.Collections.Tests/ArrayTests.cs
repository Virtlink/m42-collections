﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests
{
	[TestFixture]
	public class ArrayTests
	{
		[Test]
		public void Freezing_SafeArray_KeepsInnerArrayIntact()
		{
			// Given
			var sut = new Array<string>(3);
			sut[0] = "a";
			sut[1] = "b";
			sut[2] = "c";
			// This array is safe, a reference to the inner array has not been leaked.
			// NOTE: The following does not make the array unsafe,
			// as this is an `internal` field.
			var innerArray = sut.innerArray;

			// When
			sut.Freeze();

			// Then
			Assert.AreSame(innerArray, sut.innerArray);
		}

		[Test]
		public void Freezing_UnsafeArray_ClonesInnerArray()
		{
			// Given
			var sut = new Array<string>(3);
			sut[0] = "a";
			sut[1] = "b";
			sut[2] = "c";
			var reference = (string[])sut;
			// This array is unsafe, as a reference to the inner array has been leaked.
			// NOTE: The following does not make the array unsafe,
			// as this is an `internal` field.
			var innerArray = sut.innerArray;

			// When
			sut.Freeze();

			// Then
			Assert.AreNotSame(innerArray, sut.innerArray);
			Assert.AreEqual(innerArray, sut.innerArray);
		}

		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static Array<T> CreateSUT<T>(params T[] data)
		{
			return new Array<T>(data);
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<string>
		{
			protected override ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IListTests : IListTests<string>
		{
			protected override IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableArrayTests : IMutableArrayTests<string>
		{
			protected override IMutableArray<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

#if false
		[TestFixture]
		public class SCGIListTests : SCGIListTests<string>
		{
			protected override SCG.IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class SCGICollectionTests : SCGICollectionTests<string>
		{
			protected override SCG.ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}
#endif
	}
}
