﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IMutableArray{T}"/>.
	/// </summary>
	public abstract class IMutableSequenceTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IMutableSequence<T> CreateInstance(params T[] data);

		[Test]
		public void Add_AddsValue()
		{
			// Given
			T[] data = Data.GetArray<T>(0);
			T value = Data.Get<T>();
			var sut = CreateInstance(data);
			Assert.IsTrue(sut.IsEmpty);

			// When
			bool success = sut.Add(value);

			// Then
			Assert.IsTrue(success);
			Assert.IsFalse(sut.IsEmpty);
			Assert.AreEqual(value, sut.Peek());
		}

		[Test]
		public void Take_NonEmptySequence_TakesValue()
		{
			// Given
			T[] data = Data.GetArray<T>(1);
			var sut = CreateInstance(data);
			Assert.IsFalse(sut.IsEmpty);

			// When
			T value = sut.Take();

			// Then
			Assert.IsTrue(sut.IsEmpty);
			Assert.AreEqual(data[0], value);
		}

		[Test]
		public void Take_EmptySequence_ThrowsSequenceEmptyException()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);
			Assert.IsTrue(sut.IsEmpty);

			// When/Then
			Assert.Throws<SequenceEmptyException>(() =>
			{
				T value = sut.Take();
			});
		}

		[Test]
		public void Peek_NonEmptySequence_ReturnsValue()
		{
			// Given
			T[] data = Data.GetArray<T>(1);
			var sut = CreateInstance(data);
			Assert.IsFalse(sut.IsEmpty);

			// When
			T value = sut.Peek();

			// Then
			Assert.IsFalse(sut.IsEmpty);
			Assert.AreEqual(data[0], value);
		}

		[Test]
		public void Peek_EmptySequence_ThrowsSequenceEmptyException()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);
			Assert.IsTrue(sut.IsEmpty);

			// When/Then
			Assert.Throws<SequenceEmptyException>(() =>
			{
				T value = sut.Peek();
			});
		}
	}
}
