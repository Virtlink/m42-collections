﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System;
using System.Linq;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IMutableKeyedCollection{T}"/>.
	/// </summary>
	public abstract class IMutableKeyedCollectionTests<TKey, TValue> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IMutableKeyedCollection<TKey, TValue> CreateInstance(params IAssociation<TKey, TValue>[] data);

		[Test]
		public void Indexer_SetIndex1()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TValue value = Data.Get<TValue>();
			var sut = CreateInstance(data);

			// When
			sut[key] = value;

			// Then
			Assert.IsFalse(sut.IsFrozen);
			Assert.AreEqual(value, sut[key]);
		}

		[Test]
		public void Indexer_Frozen_SetIndex1_ThrowsException()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TValue value = Data.Get<TValue>();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut[key] = value;
			});
		}

		[Test]
		public void Add_OneItem_AddsCorrectly()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = Data.GetDistinct<TKey>(data.Keys());
			TValue value = Data.Get<TValue>();
			var sut = CreateInstance(data);

			// When
			bool success = sut.Add(key, value);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(value, sut[key]);
		}

		[Test]
		public void Add_HundredItems_AddsCorrectly()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(0).ToAssociations();
			var sut = CreateInstance(data);

			// When
			IAssociation<TKey, TValue>[] expected = new IAssociation<TKey, TValue>[100];
			for (int i = 0; i < 100; i++)
			{
				TKey key = Data.GetDistinct<TKey>(expected.Take(i).Keys());
				TValue value = Data.Get<TValue>();
				expected[i] = Association.Create(key, value);
				bool success = sut.Add(key, value);
				Assert.IsTrue(success);
			}

			// Then
			CollectionAssert.AreEquivalent(expected, sut);
		}

		[Test]
		public void Add_Frozen_ThrowsException()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = Data.Get<TKey>();
			TValue value = Data.Get<TValue>();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Add(key, value);
			});
		}

		[Test]
		public void RemoveKey_GivenSameInstance_RemovesValueAndReturnsTrue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			var sut = CreateInstance(data);

			// When
			bool result = sut.RemoveKey(key);

			// Then
			Assert.IsTrue(result);
			CollectionAssert.AreEquivalent(data.Skip(1), sut);
		}

		[Test]
		public void RemoveKey_GivenSameValue_RemovesValueAndReturnsTrue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TKey cloneKey = Data.Clone(key);
			var sut = CreateInstance(data);

			// When
			bool result = sut.RemoveKey(cloneKey);

			// Then
			Assert.IsTrue(result);
			CollectionAssert.AreEquivalent(data.Skip(1), sut);
		}

		[Test]
		public void RemoveKey_GivenNonExistingKey_ReturnsFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey nonExistingKey = Data.GetDistinct<TKey>(data.Keys());
			var sut = CreateInstance(data);

			// When
			bool result = sut.RemoveKey(nonExistingKey);

			// Then
			Assert.IsFalse(result);
			CollectionAssert.AreEquivalent(data, sut);
		}

		[Test]
		public void RemoveKey_GivenKeyOfWrongType_ReturnsFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance(data);

			// When
			bool result = sut.RemoveKey(DateTime.Now);

			// Then
			Assert.IsFalse(result);
			CollectionAssert.AreEquivalent(data, sut);
		}

		[Test]
		public void RemoveKey_Frozen_ThrowsException()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Remove(key);
			});
		}
	}
}
