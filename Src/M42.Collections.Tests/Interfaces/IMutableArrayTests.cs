﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IMutableArray{T}"/>.
	/// </summary>
	public abstract class IMutableArrayTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IMutableArray<T> CreateInstance(params T[] data);

		[Test]
		public void Indexer_SetIndex1()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = Data.GetDistinct<T>(data);
			T[] expected = (T[])data.Clone();
			expected[1] = value;
			int index = 1;
			var sut = CreateInstance((T[])data);

			// When
			sut[index] = value;

			// Then
			Assert.IsFalse(sut.IsFrozen);
			Assert.AreEqual(expected, sut);
		}

		[Test]
		public void Indexer_Frozen_SetIndex1_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = Data.Get<T>();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut[1] = value;
			});
		}
	}
}
