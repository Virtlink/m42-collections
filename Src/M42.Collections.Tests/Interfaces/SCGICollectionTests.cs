﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="SCG.ICollection{T}"/>.
	/// </summary>
	public abstract class SCGICollectionTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract SCG.ICollection<T> CreateInstance(params T[] data);

		[Test]
		public void Count_GivenThreeItems_Returns3()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			int result = sut.Count;

			// Then
			Assert.AreEqual(data.Length, result);
		}

		[Test]
		public void Contains_GivenSameInstance_ReturnsTrue()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = data[1];
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(value);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Contains_GivenSameValue_ReturnsTrue()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = Data.Clone(data[1]);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(value);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Contains_GivenNonExistingValue_ReturnsFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T nonExistingValue = Data.GetDistinct<T>(data);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(nonExistingValue);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void CopyTo_CopiesCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			T[] array = new T[3];
			sut.CopyTo(array, 0);

			// Then
			CollectionAssert.AreEquivalent(data, array);
		}
	}
}
