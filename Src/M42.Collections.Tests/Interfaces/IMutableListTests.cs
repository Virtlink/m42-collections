﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IMutableList{T}"/>.
	/// </summary>
	public abstract class IMutableListTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IMutableList<T> CreateInstance(params T[] data);

		[Test]
		public void Insert_InTheMiddle_InsertsCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T newValue = Data.Get<T>();
			int index = 1;
			var sut = CreateInstance(data);

			// When
			bool success = sut.Insert(index, newValue);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new T[] { data[0], newValue, data[1], data[2] }, sut);
		}

		[Test]
		public void Insert_AtTheStart_InsertsCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T newValue = Data.Get<T>();
			int index = 0;
			var sut = CreateInstance(data);

			// When
			bool success = sut.Insert(index, newValue);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new[] { newValue, data[0], data[1], data[2] }, sut);
		}

		[Test]
		public void Insert_AtTheEnd_InsertsCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T newValue = Data.Get<T>();
			int index = 3;
			var sut = CreateInstance(data);

			// When
			bool success = sut.Insert(index, newValue);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new[] { data[0], data[1], data[2], newValue }, sut);
		}

		[Test]
		public void Insert_ExistingItemAtSameIndex_InsertsCorrectlyOrReturnsFalse()
		{
			// Given
			T data = Data.Get<T>();
			var sut = CreateInstance(data);

			// When/Then
			Assert.DoesNotThrow(() => { bool success = sut.Insert(0, data); });
		}

		[Test]
		public void Insert_ExistingItemAtDifferentIndex_InsertsCorrectlyOrReturnsFalse()
		{
			// Given
			T data = Data.Get<T>();
			var sut = CreateInstance(data);

			// When/Then
			Assert.DoesNotThrow(() => { bool success = sut.Insert(1, data); });
		}

		[Test]
		public void Insert_Frozen_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T newValue = Data.Get<T>();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Insert(1, newValue);
			});
		}

		[Test]
		public void RemoveAt_FromTheMiddle_RemovesCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(4);
			int index = 1;
			var sut = CreateInstance(data);

			// When
			bool success = sut.RemoveAt(index);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new[] { data[0], data[2], data[3] }, sut);
		}

		[Test]
		public void RemoveAt_FromTheStart_RemovesCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(4);
			int index = 0;
			var sut = CreateInstance(data);

			// When
			bool success = sut.RemoveAt(index);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new[] { data[1], data[2], data[3] }, sut);
		}

		[Test]
		public void RemoveAt_FromTheEnd_RemovesCorrectly()
		{
			// Given
			T[] data = Data.GetArray<T>(4);
			int index = 3;
			var sut = CreateInstance(data);

			// When
			bool success = sut.RemoveAt(index);

			// Then
			Assert.IsTrue(success);
			Assert.AreEqual(new[] { data[0], data[1], data[2] }, sut);
		}

		[Test]
		public void RemoveAt_Frozen_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.RemoveAt(1);
			});
		}
	}
}
