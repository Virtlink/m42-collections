﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="SCG.ISet{T}"/>.
	/// </summary>
	public abstract class SCGMutableISetTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract SCG.ISet<T> CreateInstance(params T[] data);



		[Test]
		public void Add_EmptyCollection_AddsCorrectly()
		{
			// Given
			T value = Data.Get<T>();
			var data = Data.GetArray<T>(0);
			var sut = CreateInstance(data);

			// When
			bool success = sut.Add(value);

			// Then
			Assert.IsTrue(success);
			CollectionAssert.AreEquivalent(new[] { value }, sut);
		}

		[Test]
		public void Add_NonEmptyCollection_AddsCorrectly()
		{
			// Given
			var value = Data.Get<T>();
			Array<T> data = Data.GetArray<T>(3);
			Array<T> expected = new Array<T>(4);
			ArrayExt.Copy(data, 0, expected, 0, data.Count);
			expected[data.Count] = value;
			var sut = CreateInstance((T[])data);

			// When
			bool success = sut.Add(value);

			// Then
			Assert.IsTrue(success);
			CollectionAssert.AreEquivalent(expected, sut);
		}

		[Test]
		public void Add_HundredItems_AddsCorrectly()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);

			// When
			T[] expected = new T[100];
			for (int i = 0; i < 100; i++)
			{
				T value = Data.Get<T>();
				expected[i] = value;
				bool success = sut.Add(value);
				Assert.IsTrue(success);
			}

			// Then
			CollectionAssert.AreEquivalent(expected, sut);
		}


		[Test]
		public void ExceptWith_Same_ReturnsEmpty()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			sut.ExceptWith(same);

			// Then
			Assert.IsTrue(sut.SetEquals(new T[0]));
		}

		[Test]
		public void ExceptWith_ProperSubset_ReturnsRemaining()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			sut.ExceptWith(subset);

			// Then
			Assert.IsTrue(sut.SetEquals(elements.Skip(25)));
		}

		[Test]
		public void ExceptWith_ProperSuperset_ReturnsEmpty()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			sut.ExceptWith(superset);

			// Then
			Assert.IsTrue(sut.SetEquals(new T[0]));
		}

		[Test]
		public void ExceptWith_AllDifferent_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			sut.ExceptWith(different);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}

		[Test]
		public void ExceptWith_None_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			sut.ExceptWith(none);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}




		[Test]
		public void SymmetricExceptWith_Same_ReturnsNone()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			sut.SymmetricExceptWith(same);

			// Then
			Assert.IsTrue(sut.SetEquals(new T[0]));
		}

		[Test]
		public void SymmetricExceptWith_ProperSubset_ReturnsRemaining()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			sut.SymmetricExceptWith(subset);

			// Then
			Assert.IsTrue(sut.SetEquals(elements.Skip(25)));
		}

		[Test]
		public void SymmetricExceptWith_ProperSuperset_ReturnsRemaining()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			sut.SymmetricExceptWith(superset);

			// Then
			Assert.IsTrue(sut.SetEquals(superset.Skip(50)));
		}

		[Test]
		public void SymmetricExceptWith_AllDifferent_ReturnsUnion()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			sut.SymmetricExceptWith(different);

			// Then
			Assert.IsTrue(sut.SetEquals(elements.Concat(different)));
		}

		[Test]
		public void SymmetricExceptWith_None_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			sut.SymmetricExceptWith(none);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}





		[Test]
		public void IntersectWith_Same_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			sut.IntersectWith(same);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}

		[Test]
		public void IntersectWith_ProperSubset_ReturnsProperSubset()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			sut.IntersectWith(subset);

			// Then
			Assert.IsTrue(sut.SetEquals(subset));
		}

		[Test]
		public void IntersectWith_ProperSuperset_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			sut.IntersectWith(superset);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}

		[Test]
		public void IntersectWith_AllDifferent_ReturnsNone()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			sut.IntersectWith(different);

			// Then
			Assert.IsTrue(sut.SetEquals(new T[0]));
		}

		[Test]
		public void IntersectWith_None_ReturnsNone()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			sut.IntersectWith(none);

			// Then
			Assert.IsTrue(sut.SetEquals(new T[0]));
		}




		[Test]
		public void UnionWith_Same_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			sut.UnionWith(same);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}

		[Test]
		public void UnionWith_ProperSubset_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			sut.UnionWith(subset);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}

		[Test]
		public void UnionWith_ProperSuperset_ReturnsProperSuperset()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			sut.UnionWith(superset);

			// Then
			Assert.IsTrue(sut.SetEquals(superset));
		}

		[Test]
		public void UnionWith_AllDifferent_ReturnsSameAndAllDifferent()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			sut.UnionWith(different);

			// Then
			Assert.IsTrue(sut.SetEquals(elements.Concat(different)));
		}

		[Test]
		public void UnionWith_None_ReturnsSame()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			sut.UnionWith(none);

			// Then
			Assert.IsTrue(sut.SetEquals(elements));
		}
	}
}
