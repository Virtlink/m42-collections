﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IKeyedCollection{T}"/>.
	/// </summary>
	public abstract class IKeyedCollectionTests<TKey, TValue> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IKeyedCollection<TKey, TValue> CreateInstance(params IAssociation<TKey, TValue>[] data);

		[Test]
		public void Indexer_GivenSameInstance_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			TValue result = sut[key];

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void Indexer_GivenSameValue_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TKey cloneKey = Data.Clone(key);
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			TValue result = sut[cloneKey];

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void Indexer_GivenNonExistingKey_ThrowsKeyNotFoundException()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey nonExistingKey = Data.GetDistinct<TKey>(data.Keys());
			var sut = CreateInstance(data);

			// When/Then
			Assert.Throws<KeyNotFoundException>(() =>
			{
				TValue result = sut[nonExistingKey];
			});
		}

		[Test]
		public void Indexer_GivenKeyOfWrongType_ThrowsKeyNotFoundException()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance(data);

			// When/Then
			Assert.Throws<KeyNotFoundException>(() =>
			{
				TValue result = sut[DateTime.Now];
			});
		}

		[Test]
		public void Keys_ReturnsCollectionWithAllKeys()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance((IAssociation<TKey, TValue>[])data);

			// When
			var result = sut.Keys;

			// Then
			CollectionAssert.AreEquivalent(data.Keys(), result);
		}

		[Test]
		public void Values_ReturnsCollectionWithAllValues()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance((IAssociation<TKey, TValue>[])data);

			// When
			var result = sut.Values;

			// Then
			CollectionAssert.AreEquivalent(data.Values(), result);
		}

		[Test]
		public void GetValue2_GivenSameInstance_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			TValue result = sut.GetValue(key, out isPresent);

			// Then
			Assert.IsTrue(isPresent);
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetValue2_GivenSameValue_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TKey cloneKey = Data.Clone(key);
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			TValue result = sut.GetValue(cloneKey, out isPresent);

			// Then
			Assert.IsTrue(isPresent);
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetValue2_GivenNonExistingValue_ReturnsDefaultAndFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey nonExistingKey = Data.GetDistinct<TKey>(data.Keys());
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			TValue result = sut.GetValue(nonExistingKey, out isPresent);

			// Then
			Assert.IsFalse(isPresent);
			Assert.AreEqual(default(TKey), result);
		}

		[Test]
		public void GetValue2_GivenValueOfWrongType_ReturnsDefaultAndFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			TValue result = sut.GetValue(DateTime.Now, out isPresent);

			// Then
			Assert.IsFalse(isPresent);
			Assert.AreEqual(default(TKey), result);
		}

		[Test]
		public void GetValue1_GivenSameInstance_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			TValue result = sut.GetValue(key);

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetValue1_GivenSameValue_ReturnsAssociatedValue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TKey cloneKey = Data.Clone(key);
			TValue value = data.Values().First();
			var sut = CreateInstance(data);

			// When
			TValue result = sut.GetValue(cloneKey);

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetValue1_GivenNonExistingKey_ReturnsDefault()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey nonExistingKey = Data.GetDistinct<TKey>(data.Keys());
			var sut = CreateInstance(data);

			// When
			TValue result = sut.GetValue(nonExistingKey);

			// Then
			Assert.AreEqual(default(TKey), result);
		}

		[Test]
		public void GetValue1_GivenKeyOfWrongType_ReturnsDefault()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance(data);

			// When
			TValue result = sut.GetValue(DateTime.Now);

			// Then
			Assert.AreEqual(default(TValue), result);
		}

		[Test]
		public void ContainsKey_GivenSameInstance_ReturnsTrue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			var sut = CreateInstance(data);

			// When
			bool result = sut.ContainsKey(key);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void ContainsKey_GivenSameValue_ReturnsTrue()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey key = data.Keys().First();
			TKey cloneKey = Data.Clone(key);
			var sut = CreateInstance(data);

			// When
			bool result = sut.ContainsKey(cloneKey);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void ContainsKey_GivenNonExistingKey_ReturnsFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			TKey nonExistingKey = Data.GetDistinct<TKey>(data.Keys());
			var sut = CreateInstance(data);

			// When
			bool result = sut.ContainsKey(nonExistingKey);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void ContainsKey_GivenKeyOfWrongType_ReturnsFalse()
		{
			// Given
			var data = Data.GetKeyValuePairArray<TKey, TValue>(3).ToAssociations();
			var sut = CreateInstance(data);

			// When
			bool result = sut.ContainsKey(DateTime.Now);

			// Then
			Assert.IsFalse(result);
		}
	}
}
