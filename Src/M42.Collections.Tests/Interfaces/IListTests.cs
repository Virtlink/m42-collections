﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using M42.Testing;
using NUnit.Framework;
using System;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IList{T}"/>.
	/// </summary>
	public abstract class IListTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IList<T> CreateInstance(params T[] data);

		[Test]
		public void Indexer_GetIndex1_ReturnsItem1()
		{
			// Given
			int index = 1;
			var data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			T result = sut[index];

			// Then
			AssertAreSameVT(data[index], result);
		}

		[Test]
		public void IndexOf_GivenSameInstance_ReturnsIndexOfThatInstance()
		{
			// Given
			int index = 1;
			T fill = Data.Get<T>();
			T[] data = new T[3].Populate(fill);
			T value = Data.GetDistinct(fill);
			data[index] = value;
			var sut = CreateInstance(data);

			// When
			int? result = sut.IndexOf(value);

			// Then
			Assert.AreEqual(index, result);
		}

		[Test]
		public void IndexOf_GivenSameValue_ReturnsIndexOfEqualInstance()
		{
			// Given
			int index = 1;
			T fill = Data.Get<T>();
			T[] data = new T[3].Populate(fill);
			T value = Data.GetDistinct(fill);
			data[index] = value;
			T valueClone = Data.Clone(value);
			var sut = CreateInstance(data);

			// When
			int? result = sut.IndexOf(value);

			// Then
			Assert.AreEqual(index, result);
		}

		[Test]
		public void IndexOf_GivenNonExistingValue_ReturnsNull()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T nonExistingValue = Data.GetDistinct(data);
			var sut = CreateInstance(data);

			// When
			int? result = sut.IndexOf(nonExistingValue);

			// Then
			Assert.AreEqual((int?)null, result);
		}

		[Test]
		public void IndexOf_GivenValueOfWrongType_ReturnsNull()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			int? result = sut.IndexOf(DateTime.Now);

			// Then
			Assert.AreEqual((int?)null, result);
		}
	}
}
