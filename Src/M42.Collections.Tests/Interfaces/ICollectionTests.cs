﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using M42.Testing;
using System;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="ICollection{T}"/>.
	/// </summary>
	public abstract class ICollectionTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract ICollection<T> CreateInstance(params T[] data);

		[Test]
		public void Count_GivenThreeItems_Returns3()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			int result = sut.Count;

			// Then
			Assert.AreEqual(data.Length, result);
		}

		[Test]
		public void IsEmpty_GivenThreeItems_ReturnsFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			bool result = sut.IsEmpty;

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsEmpty_GivenZeroItems_ReturnsTrue()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);

			// When
			bool result = sut.IsEmpty;

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void GetMember2_GivenSameInstance_ReturnsThatInstance()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			T result = sut.GetMember(value, out isPresent);

			// Then
			Assert.IsTrue(isPresent);
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetMember2_GivenSameValue_ReturnsInstanceFromCollection()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			T cloneValue = Data.Clone(value);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			T result = sut.GetMember(cloneValue, out isPresent);

			// Then
			Assert.IsTrue(isPresent);
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetMember2_GivenNonExistingValue_ReturnsDefaultAndFalse()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T nonExistingValue = Data.GetDistinct<T>(data);
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			T result = sut.GetMember(nonExistingValue, out isPresent);

			// Then
			Assert.IsFalse(isPresent);
			Assert.AreEqual(default(T), result);
		}

		[Test]
		public void GetMember2_GivenValueOfWrongType_ReturnsDefaultAndFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			bool isPresent;
			T result = sut.GetMember(DateTime.Now, out isPresent);

			// Then
			Assert.IsFalse(isPresent);
			Assert.AreEqual(default(T), result);
		}

		[Test]
		public void GetMember1_GivenSameInstance_ReturnsThatInstance()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			T result = sut.GetMember(value);

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetMember1_GivenSameValue_ReturnsInstanceFromCollection()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			T cloneValue = Data.Clone(value);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			T result = sut.GetMember(cloneValue);

			// Then
			AssertAreSameVT(value, result);
		}

		[Test]
		public void GetMember1_GivenNonExistingValue_ReturnsDefault()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T nonExistingValue = Data.GetDistinct<T>(data);
			var sut = CreateInstance(data);

			// When
			T result = sut.GetMember(nonExistingValue);

			// Then
			Assert.AreEqual(default(T), result);
		}

		[Test]
		public void GetMember1_GivenValueOfWrongType_ReturnsDefault()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			T result = sut.GetMember(DateTime.Now);

			// Then
			Assert.AreEqual(default(T), result);
		}

		[Test]
		public void Contains_GivenSameInstance_ReturnsTrue()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(value);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Contains_GivenSameValue_ReturnsTrue()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T value = Data.GetDistinct<T>(data);
			T cloneValue = Data.Clone(value);
			data[1] = value;
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(cloneValue);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Contains_GivenNonExistingValue_ReturnsFalse()
		{
			// Given
			T[] data = new T[3].Populate(Data.Get<T>());
			T nonExistingValue = Data.GetDistinct<T>(data);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(nonExistingValue);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void Contains_GivenValueOfWrongType_ReturnsFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Contains(DateTime.Now);

			// Then
			Assert.IsFalse(result);
		}
	}
}
