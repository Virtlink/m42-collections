﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="IMutableCollection{T}"/>.
	/// </summary>
	public abstract class IMutableCollectionTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract IMutableCollection<T> CreateInstance(params T[] data);

		[Test]
		public void Add_EmptyCollection_AddsCorrectly()
		{
			// Given
			T value = Data.Get<T>();
			var data = Data.GetArray<T>(0);
			var sut = CreateInstance(data);

			// When
			bool success = sut.Add(value);

			// Then
			Assert.IsTrue(success);
			CollectionAssert.AreEquivalent(new[] { value }, sut);
		}

		[Test]
		public void Add_NonEmptyCollection_AddsCorrectly()
		{
			// Given
			var value = Data.Get<T>();
			Array<T> data = Data.GetArray<T>(3);
			Array<T> expected = new Array<T>(4);
			ArrayExt.Copy(data, 0, expected, 0, data.Count);
			expected[data.Count] = value;
			var sut = CreateInstance((T[])data);

			// When
			bool success = sut.Add(value);

			// Then
			Assert.IsTrue(success);
			CollectionAssert.AreEquivalent(expected, sut);
		}

		[Test]
		public void Add_HundredItems_AddsCorrectly()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);

			// When
			T[] expected = new T[100];
			for (int i = 0; i < 100; i++)
			{
				T value = Data.Get<T>();
				expected[i] = value;
				bool success = sut.Add(value);
				Assert.IsTrue(success);
			}

			// Then
			CollectionAssert.AreEquivalent(expected, sut);
		}

		[Test]
		public void Add_ExistingItem_AddsCorrectlyOrReturnsFalse()
		{
			// Given
			T data = Data.Get<T>();
			var sut = CreateInstance(data);

			// When/Then
			Assert.DoesNotThrow(() => { bool success = sut.Add(data); });
		}

		[Test]
		public void Add_Frozen_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T newValue = Data.Get<T>();
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Add(newValue);
			});
		}

		[Test]
		public void Remove_GivenSameInstance_RemovesValueAndReturnsTrue()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = data[1];
			var sut = CreateInstance(data);

			// When
			bool result = sut.Remove(value);

			// Then
			Assert.IsTrue(result);
			CollectionAssert.AreEquivalent(new T[] { data[0], data[2] }, sut);
		}

		[Test]
		public void Remove_GivenSameValue_RemovesValueAndReturnsTrue()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = Data.Clone(data[1]);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Remove(value);

			// Then
			Assert.IsTrue(result);
			CollectionAssert.AreEquivalent(new T[] { data[0], data[2] }, sut);
		}

		[Test]
		public void Remove_GivenNonExistingValue_ReturnsFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T nonExistingValue = Data.GetDistinct<T>(data);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Remove(nonExistingValue);

			// Then
			Assert.IsFalse(result);
			CollectionAssert.AreEquivalent(data, sut);
		}

		[Test]
		public void Remove_GivenValueOfWrongType_ReturnsFalse()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			bool result = sut.Remove(DateTime.Now);

			// Then
			Assert.IsFalse(result);
			CollectionAssert.AreEquivalent(data, sut);
		}

		[Test]
		public void Remove_Frozen_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			T value = data[1];
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Remove(value);
			});
		}

		[Test]
		public void Clear_NonEmptyCollection_ClearsAll()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);

			// When
			sut.Clear();

			// Then
			CollectionAssert.AreEquivalent(new T[0], sut);
		}

		[Test]
		public void Clear_EmptyCollection_ClearsAll()
		{
			// Given
			T[] data = new T[0];
			var sut = CreateInstance(data);

			// When
			sut.Clear();

			// Then
			CollectionAssert.AreEquivalent(new T[0], sut);
		}

		[Test]
		public void Clear_Frozen_ThrowsException()
		{
			// Given
			T[] data = Data.GetArray<T>(3);
			var sut = CreateInstance(data);
			var freezable = Common.ExpectOfType<IFreezable>(sut);
			freezable.Freeze();

			// When, Then
			Assert.Throws<CollectionFrozenException>(() =>
			{
				sut.Clear();
			});
		}
	}
}
