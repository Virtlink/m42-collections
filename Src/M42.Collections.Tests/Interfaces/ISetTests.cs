﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;
using System;

namespace M42.Collections.Tests
{
	/// <summary>
	/// Tests classes implementing <see cref="ISet{T}"/>.
	/// </summary>
	public abstract class ISetTests<T> : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected abstract ISet<T> CreateInstance(params T[] data);

		[Test]
		public void Overlaps_Same_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(same);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Overlaps_ProperSubset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(subset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Overlaps_ProperSuperset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(superset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void Overlaps_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void Overlaps_None_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(none);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void Overlaps_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.Overlaps(wrongType);

			// Then
			Assert.IsFalse(result);
		}



		[Test]
		public void SetEquals_Same_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(same);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void SetEquals_ProperSubset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(subset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void SetEquals_ProperSuperset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(superset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void SetEquals_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void SetEquals_None_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(none);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void SetEquals_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.SetEquals(wrongType);

			// Then
			Assert.IsFalse(result);
		}




		[Test]
		public void IsSubsetOf_Same_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(same);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsSubsetOf_ProperSubset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(subset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsSubsetOf_ProperSuperset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(superset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsSubsetOf_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsSubsetOf_None_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(none);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsSubsetOf_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSubsetOf(wrongType);

			// Then
			Assert.IsFalse(result);
		}




		[Test]
		public void IsProperSubsetOf_Same_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(same);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSubsetOf_ProperSubset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(subset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSubsetOf_ProperSuperset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(superset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsProperSubsetOf_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSubsetOf_None_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(none);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSubsetOf_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSubsetOf(wrongType);

			// Then
			Assert.IsFalse(result);
		}




		[Test]
		public void IsSupersetOf_Same_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(same);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsSupersetOf_ProperSubset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(subset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsSupersetOf_ProperSuperset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(superset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsSupersetOf_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsSupersetOf_None_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(none);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsSupersetOf_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsSupersetOf(wrongType);

			// Then
			Assert.IsFalse(result);
		}




		[Test]
		public void IsProperSupersetOf_Same_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] same = (T[])elements.Clone();
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(same);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSupersetOf_ProperSubset_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] subset = new T[25];
			System.Array.Copy(elements, 0, subset, 0, subset.Length);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(subset);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsProperSupersetOf_ProperSuperset_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] superset = new T[75];
			System.Array.Copy(elements, 0, superset, 0, elements.Length);
			System.Array.Copy(Data.GetDistinctArray<T>(25, elements), 0, superset, 50, 25);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(superset);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSupersetOf_AllDifferent_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] different = Data.GetDistinctArray<T>(50, elements);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(different);

			// Then
			Assert.IsFalse(result);
		}

		[Test]
		public void IsProperSupersetOf_None_ReturnsTrue()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			T[] none = new T[0];
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(none);

			// Then
			Assert.IsTrue(result);
		}

		[Test]
		public void IsProperSupersetOf_WrongType_ReturnsFalse()
		{
			// Given
			T[] elements = Data.GetDistinctArray<T>(50);
			DateTime[] wrongType = Data.GetArray<DateTime>(5);
			var sut = CreateInstance(elements);

			// When
			bool result = sut.IsProperSupersetOf(wrongType);

			// Then
			Assert.IsFalse(result);
		}
	}
}
