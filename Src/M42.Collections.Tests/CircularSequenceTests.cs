﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Tests.Support;
using NUnit.Framework;

namespace M42.Collections.Tests
{
	[TestFixture]
	public class CircularSequenceTests : TestClass
	{
		/// <summary>
		/// Creates an instance of the subject under test.
		/// </summary>
		/// <param name="data">The data to put in the subject.</param>
		/// <returns>The created subject.</returns>
		protected static CircularSequence<T> CreateSUT<T>(params T[] data)
		{
			var queue = new CircularSequence<T>(data.Length);
			foreach (var element in data)
			{
				queue.Add(element);
			}
			return queue;
		}

		[Test]
		public void Clear_CorrectlyClears_AfterwardsStillUsable()
		{
			// Given
			var sut = CreateSUT(Data.GetArray<string>(3));

			// When
			sut.Clear();

			// Then
			Assert.AreEqual(0, sut.Count);
			Assert.IsTrue(sut.IsEmpty);
			Assert.Throws<SequenceEmptyException>(() => sut.Peek());
			Assert.Throws<SequenceEmptyException>(() => sut.Dequeue());
			Assert.Throws<SequenceEmptyException>(() => sut.Pop());

			var data = Data.Get<string>();
			sut.Add(data);
			Assert.AreEqual(1, sut.Count);
			Assert.IsFalse(sut.IsEmpty);
			Assert.AreEqual(data, sut.Peek());
			Assert.AreEqual(data, sut.Dequeue());
		}

		[TestFixture]
		public class ICollectionTests : ICollectionTests<string>
		{
			protected override ICollection<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IListTests : IListTests<string>
		{
			protected override IList<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}

		[TestFixture]
		public class IMutableSequenceTests : IMutableSequenceTests<string>
		{
			protected override IMutableSequence<string> CreateInstance(params string[] data)
			{
				return CreateSUT(data);
			}
		}
	}
}
