﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M42.Testing
{
	/// <summary>
	/// Array extensions.
	/// </summary>
	public static class ArrayExt
	{
		/// <summary>
		/// Populates the array with the specified value.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="array">The array to populate.</param>
		/// <param name="value">The value to populate with.</param>
		/// <returns>The populated array.</returns>
		public static T[] Populate<T>(this T[] array, T value)
		{
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = value;
			}
			return array;
		}
	}
}
