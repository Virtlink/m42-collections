﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42 Testing.
// 
// M42 Testing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 Testing is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace M42.Testing
{
	/// <summary>
	/// Provides test data.
	/// </summary>
	public partial class TestDataProvider
	{
		private readonly Random random;
		/// <summary>
		/// Gets the random value provider.
		/// </summary>
		/// <value>The <see cref="Random"/> object.</value>
		protected Random Random
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<Random>() != null);
				#endregion
				return this.random;
			}
		}

		private readonly GeneratorManager manager = new GeneratorManager();
		/// <summary>
		/// Gets the generator manager.
		/// </summary>
		/// <value>The generator manager.</value>
		protected GeneratorManager Manager
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<GeneratorManager>() != null);
				#endregion
				return this.manager;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="TestDataProvider"/> class.
		/// </summary>
		public TestDataProvider()
		{
			this.random = new Random();
			this.manager = new GeneratorManager();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TestDataProvider"/> class.
		/// </summary>
		/// <param name="seed">The seed to use.</param>
		public TestDataProvider(int seed)
		{
			this.random = new Random(seed);
			this.manager = new GeneratorManager();
		}
		#endregion

		/// <summary>
		/// Gets a single random value of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value to get.</typeparam>
		/// <returns>The value.</returns>
		/// <remarks>
		/// To get an array of random values, use <see cref="GetArray"/>.
		/// To get a string, use <see cref="GetString"/>.
		/// </remarks>
		public T Get<T>()
		{
			return (T)Get(typeof(T));
		}

		/// <summary>
		/// Gets a single random value of the specified type.
		/// </summary>
		/// <param name="type">The type of value to get.</param>
		/// <returns>The value.</returns>
		/// <remarks>
		/// To get an array of random values, use <see cref="GetArray"/>.
		/// To get a string, use <see cref="GetString"/>.
		/// </remarks>
		public object Get(Type type)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(type != null);
			#endregion
			var generator = this.manager.GetGenerator(type);
			return generator(this);
		}

		/// <summary>
		/// Gets a single random value of the specified type
		/// that is different from the specified other values.
		/// </summary>
		/// <typeparam name="T">The type of value to get.</typeparam>
		/// <param name="others">The other values.</param>
		/// <returns>The value.</returns>
		/// <remarks>
		/// To get an array of random values, use <see cref="GetArray"/>.
		/// To get a string, use <see cref="GetString"/>.
		/// </remarks>
		public T GetDistinct<T>(params T[] others)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(others != null);
			#endregion
			return (T)GetDistinct((IEnumerable<T>)others);
		}

		/// <summary>
		/// Gets a single random value of the specified type
		/// that is different from the specified other values.
		/// </summary>
		/// <typeparam name="T">The type of value to get.</typeparam>
		/// <param name="others">The other values.</param>
		/// <returns>The value.</returns>
		/// <remarks>
		/// To get an array of random values, use <see cref="GetArray"/>.
		/// To get a string, use <see cref="GetString"/>.
		/// </remarks>
		public T GetDistinct<T>(IEnumerable<T> others)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(others != null);
			#endregion
			return (T)GetDistinct(typeof(T), others);
		}

		/// <summary>
		/// Gets a single random value of the specified type.
		/// </summary>
		/// <param name="type">The type of value to get.</param>
		/// <param name="others">The other values.</param>
		/// <returns>The value.</returns>
		/// <remarks>
		/// To get an array of random values, use <see cref="GetArray"/>.
		/// To get a string, use <see cref="GetString"/>.
		/// </remarks>
		public object GetDistinct(Type type, System.Collections.IEnumerable others)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(type != null);
			Contract.Requires<ArgumentNullException>(others != null);
			#endregion
			var generator = this.manager.GetGenerator(type);
			object value;
			do
			{
				value = generator(this);
			} while (others.Cast<object>().Any(v => Object.Equals(v, value)));
			return value;
		}

		#region Integers
		/// <summary>
		/// Gets a random unsigned byte.
		/// </summary>
		/// <returns>The random value.</returns>
		public SByte GetSByte()
		{
			return unchecked((SByte)this.random.Next(SByte.MinValue, SByte.MaxValue));
		}

		/// <summary>
		/// Gets a random signed byte.
		/// </summary>
		/// <returns>The random value.</returns>
		public Byte GetByte()
		{
			return unchecked((Byte)this.random.Next(Byte.MinValue, Byte.MaxValue));
		}

		/// <summary>
		/// Gets a random signed 16-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public Int16 GetInt16()
		{
			return unchecked((Int16)this.random.Next(Int16.MinValue, Int16.MaxValue));
		}

		/// <summary>
		/// Gets a random unsigned 16-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public UInt16 GetUInt16()
		{
			return unchecked((UInt16)this.random.Next(UInt16.MinValue, UInt16.MaxValue));
		}

		/// <summary>
		/// Gets a random signed 32-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public Int32 GetInt32()
		{
			byte[] buffer = new byte[4];
			this.random.NextBytes(buffer);
			return BitConverter.ToInt32(buffer, 0);
		}

		/// <summary>
		/// Gets a random unsigned 32-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public UInt32 GetUInt32()
		{
			byte[] buffer = new byte[4];
			this.random.NextBytes(buffer);
			return BitConverter.ToUInt32(buffer, 0);
		}

		/// <summary>
		/// Gets a random signed 64-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public Int64 GetInt64()
		{
			byte[] buffer = new byte[8];
			this.random.NextBytes(buffer);
			return BitConverter.ToInt64(buffer, 0);
		}

		/// <summary>
		/// Gets a random signed 64-bit integers.
		/// </summary>
		/// <param name="min">The minimum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>The random value.</returns>
		private Int64 GetInt64(Int64 min, Int64 max)
		{
			byte[] buffer = new byte[8];
			random.NextBytes(buffer);
			long value = BitConverter.ToInt64(buffer, 0);
			value = Math.Abs(value % (max - min)) + min;
			return value;
		}

		/// <summary>
		/// Gets a random unsigned 64-bit integers.
		/// </summary>
		/// <returns>The random value.</returns>
		public UInt64 GetUInt64()
		{
			byte[] buffer = new byte[8];
			this.random.NextBytes(buffer);
			return BitConverter.ToUInt64(buffer, 0);
		}
		#endregion

		#region Floating-point
		/// <summary>
		/// Gets a random 32-bit floating-point value.
		/// </summary>
		/// <returns>The random value.</returns>
		public Single GetSingle()
		{
			// From: http://stackoverflow.com/a/3365388/146622
			double mantissa = (this.random.NextDouble() * 2.0) - 1.0;
			double exponent = Math.Pow(2.0, this.random.Next(-126, 128));
			return (float)(mantissa * exponent);
		}

		/// <summary>
		/// Gets a random 64-bit floating-point value.
		/// </summary>
		/// <returns>The random value.</returns>
		public Double GetDouble()
		{
			return this.random.NextDouble();
		}
		#endregion

		#region Chars and Strings
		/// <summary>
		/// Printable ASCII characters.
		/// </summary>
		private const string PrintableAsciiChars = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

		/// <summary>
		/// Gets a random character from the printable ASCII range.
		/// </summary>
		/// <returns>The random character.</returns>
		public Char GetChar()
		{
			int index = this.random.Next(0, PrintableAsciiChars.Length);
			return PrintableAsciiChars[index];
		}
		
		/// <summary>
		/// Gets a string with random printable ASCII characters.
		/// </summary>
		/// <param name="length">The length of the string.</param>
		/// <returns>The string with random characters.</returns>
		public String GetString(int length)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			return new String(GetArray<Char>(length));
			//return GetString(length, length);
		}

#if false
		/// <summary>
		/// Gets a string with random printable ASCII characters.
		/// </summary>
		/// <param name="minLength">The minimum length of the string.</param>
		/// <param name="maxLength">The maximum length of the string.</param>
		/// <returns>The string with random characters.</returns>
		public String GetString(int minLength, int maxLength)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minLength >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength >= minLength);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength < Int32.MaxValue);
			#endregion
			return new String(GetArray<Char>(minLength, maxLength));
		}
#endif
		#endregion

		#region Miscellaneous
		/// <summary>
		/// Gets a random boolean value.
		/// </summary>
		/// <returns>The random value.</returns>
		public Boolean GetBoolean()
		{
			return this.random.Next(0, 2) != 0;
		}

		/// <summary>
		/// Gets a random object.
		/// </summary>
		/// <returns>Any object.</returns>
		public Object GetObject()
		{
			return new Object();
		}

		/// <summary>
		/// Gets a random date/time.
		/// </summary>
		/// <returns>The random value.</returns>
		public DateTime GetDateTime()
		{
			return new DateTime(GetInt64(DateTime.MinValue.Ticks, DateTime.MaxValue.Ticks));
		}
		#endregion

		#region Arrays
		/// <summary>
		/// Gets an array with random values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="length">The length of the array.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetArray<T>(int length)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			T[] array = new T[length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = Get<T>();
			}
			return array;
		}

#if false
		/// <summary>
		/// Gets an array with random values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="minLength">The minimum length of the array.</param>
		/// <param name="maxLength">The maximum length of the array.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetArray<T>(int minLength, int maxLength)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minLength >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength >= minLength);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength < Int32.MaxValue);
			#endregion
			int length = this.random.Next(minLength, maxLength + 1);
			T[] array = new T[length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = Get<T>();
			}
			return array;
		}
#endif

		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="length">The length of the array.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int length)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			return GetDistinctArray<T>(length, (IEnumerable<T>)null);
		}

#if false
		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="minLength">The minimum length of the array.</param>
		/// <param name="maxLength">The maximum length of the array.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int minLength, int maxLength)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minLength >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength >= minLength);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength < Int32.MaxValue);
			#endregion
			return GetDistinctArray(minLength, maxLength, (IEnumerable<T>)null);
		}
#endif

		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="length">The length of the array.</param>
		/// <param name="others">The other values.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int length, params T[] others)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			return GetDistinctArray(length, (IEnumerable<T>)others);
		}

		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="length">The length of the array.</param>
		/// <param name="others">The other values.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int length, IEnumerable<T> others)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			HashSet<T> set = new HashSet<T>();
			while (set.Count < length)
			{
				var value = Get<T>();
				if (others == null || !others.Contains(value))
					set.Add(value);
			}
			return set.ToArray();
		}

#if false
		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="minLength">The minimum length of the array.</param>
		/// <param name="maxLength">The maximum length of the array.</param>
		/// <param name="others">The other values.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int minLength, int maxLength, params T[] others)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minLength >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength >= minLength);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength < Int32.MaxValue);
			#endregion
			return GetDistinctArray(minLength, maxLength, (IEnumerable<T>)others);
		}

		/// <summary>
		/// Gets an array with random distinct values of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="minLength">The minimum length of the array.</param>
		/// <param name="maxLength">The maximum length of the array.</param>
		/// <param name="others">The other values; or <see langword="null"/>.</param>
		/// <returns>The array with random values.</returns>
		public T[] GetDistinctArray<T>(int minLength, int maxLength, IEnumerable<T> others)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minLength >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength >= minLength);
			Contract.Requires<ArgumentOutOfRangeException>(maxLength < Int32.MaxValue);
			#endregion
			HashSet<T> set = new HashSet<T>();
			while (set.Count < minLength)
			{
				var value = Get<T>();
				if (others == null || !others.Contains(value))
					set.Add(value);
			}
			return set.ToArray();
		}
#endif
		#endregion

		#region Key/Value Pairs
		/// <summary>
		/// Gets an array of key/value pairs.
		/// </summary>
		/// <typeparam name="TKey">The type of keys.</typeparam>
		/// <typeparam name="TValue">The type of values.</typeparam>
		/// <param name="length">The length of the array.</param>
		/// <returns>The array of key/value pairs.</returns>
		public KeyValuePair<TKey, TValue>[] GetKeyValuePairArray<TKey, TValue>(int length)
		{
			var keys = GetDistinctArray<TKey>(length);
			var values = GetArray<TValue>(length);
			KeyValuePair<TKey, TValue>[] array = new KeyValuePair<TKey, TValue>[length];
			for (int i = 0; i < length; i++)
			{
				array[i] = new KeyValuePair<TKey, TValue>(keys[i], values[i]);
			}
			return array;
		}
		#endregion

		/// <summary>
		/// Makes a shallow copy of an object.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <param name="obj">The object.</param>
		/// <returns>The shallow copy.</returns>
		public T Clone<T>(T obj)
		{
			if (Object.ReferenceEquals(obj, null))
				return default(T);
			if (obj.GetType().IsValueType)
				return obj;
			if (typeof(string).IsAssignableFrom(typeof(T)))
				return (T)(object)new String(((string)(object)obj).ToCharArray());

			var inst = obj.GetType().GetMethod("MemberwiseClone", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
			if (inst == null)
				return default(T);
			return (T)inst.Invoke(obj, null);
		}

		/// <summary>
		/// Adds a generator for the specified type of value.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="generator">The value generator.</param>
		/// <returns>The test data provider.</returns>
		public TestDataProvider With<T>(Func<TestDataProvider, T> generator)
		{
			this.manager.Register(typeof(T), p => generator(p));
			return this;
		}
	}
}
