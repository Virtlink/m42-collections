﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42 Testing.
// 
// M42 Testing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 Testing is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Globalization;

namespace M42.Testing
{
	/// <summary>
	/// A interval of T.
	/// </summary>
	/// <typeparam name="T">The type of objects in the interval.</typeparam>
	public struct Interval<T> : IFormattable, IEquatable<Interval<T>>
		where T : IComparable<T>
	{
		private readonly T minimum;
		/// <summary>
		/// Gets the minimum of the interval.
		/// </summary>
		/// <value>The inclusive start of the interval;
		/// or <see langword="null"/> to specify no lower limit.</value>
		public T Minimum
		{
			get { return this.minimum; }
		}

		private readonly IntervalEndpointKind minimumKind;
		/// <summary>
		/// Gets the kind of minimum.
		/// </summary>
		/// <value>A member of the <see cref="IntervalEndpointKind"/> enumeration.</value>
		public IntervalEndpointKind MinimumKind
		{
			get
			{
				#region Contract
				Contract.Ensures(Enum.IsDefined(typeof(IntervalEndpointKind), Contract.Result<IntervalEndpointKind>()));
				#endregion
				return this.minimumKind;
			}
		}

		private readonly T maximum;
		/// <summary>
		/// Gets the end of the interval.
		/// </summary>
		/// <value>The inclusive end of the interval;
		/// or <see langword="null"/> to specify no upper limit.</value>
		public T Maximum
		{
			get { return this.maximum; }
		}

		private readonly IntervalEndpointKind maximumKind;
		/// <summary>
		/// Gets the kind of maximum.
		/// </summary>
		/// <value>A member of the <see cref="IntervalEndpointKind"/> enumeration.</value>
		public IntervalEndpointKind MaximumKind
		{
			get
			{
				#region Contract
				Contract.Ensures(Enum.IsDefined(typeof(IntervalEndpointKind), Contract.Result<IntervalEndpointKind>()));
				#endregion
				return this.maximumKind;
			}
		}

		/// <summary>
		/// Gets whether the interval is unlimited.
		/// </summary>
		/// <value><see langword="true"/> when the interval is unlimited;
		/// otherwise, <see langword="false"/>.</value>
		public bool IsUnlimited
		{
			get { return this.minimum == null && this.maximum == null; }
		}

		/// <summary>
		/// Gets whether the interval is finite.
		/// </summary>
		/// <value><see langword="true"/> when the interval is finite;
		/// otherwise, <see langword="false"/>.</value>
		public bool IsFinite
		{
			get { return this.minimum != null && this.maximum != null; }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Interval{T}"/> class.
		/// </summary>
		/// <param name="value">The single included value in the interval.</param>
		public Interval(T value)
			: this(value, value, IntervalEndpointKind.Closed, IntervalEndpointKind.Closed)
		{
			// Nothing to do.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Interval{T}"/> class.
		/// </summary>
		/// <param name="minimum">The interval inclusive minimum; or <see langword="null"/> to specify no lower bound.</param>
		/// <param name="maximum">The interval inclusive maximum; or <see langword="null"/> to specify no upper bound.</param>
		public Interval(T minimum, T maximum)
			: this(minimum, maximum, IntervalEndpointKind.Closed, IntervalEndpointKind.Closed)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minimum == null || maximum == null || minimum.CompareTo(maximum) <= 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Interval{T}"/> class.
		/// </summary>
		/// <param name="minimum">The interval minimum; or <see langword="null"/> to specify no lower bound.</param>
		/// <param name="maximum">The interval maximum; or <see langword="null"/> to specify no upper bound.</param>
		/// <param name="minimumKind">The kind of minimum.</param>
		/// <param name="maximumKind">The kind of maximum.</param>
		public Interval(T minimum, T maximum, IntervalEndpointKind minimumKind, IntervalEndpointKind maximumKind)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minimum == null || maximum == null || minimum.CompareTo(maximum) <= 0);
			#endregion
			this.minimum = minimum;
			this.maximum = maximum;
			this.minimumKind = this.minimum != null ? minimumKind : IntervalEndpointKind.Open;
			this.maximumKind = this.maximum != null ? maximumKind : IntervalEndpointKind.Open;
		}
		#endregion

		#region Equality
		/// <inheritdoc />
		public bool Equals(Interval<T> other)
		{
			return Object.Equals(this.minimum, other.minimum)
				&& Object.Equals(this.maximum, other.maximum);
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			int hash = 17;
			unchecked
			{
				if (this.minimum != null)
					hash = hash * 29 + this.minimum.GetHashCode();
				if (this.maximum != null)
					hash = hash * 29 + this.maximum.GetHashCode();
			}
			return hash;
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (!(obj is Interval<T>))
				return false;
			return Equals((Interval<T>)obj);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified <see cref="Interval{T}"/> objects are equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator ==(Interval<T> left, Interval<T> right)
		{
			return Object.Equals(left, right);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified <see cref="Interval{T}"/> objects are not equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are not equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator !=(Interval<T> left, Interval<T> right)
		{
			return !(left == right);
		}
		#endregion

		/// <summary>
		/// Determines whether the specified value is contained in the interval.
		/// </summary>
		/// <param name="value">The value to check; or <see langword="null"/>.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/> is in the interval;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// When <paramref name="value"/> is <see langword="null"/>,
		/// this method returns <see langword="false"/>.
		/// </remarks>
		[Pure]
		public bool Contains(T value)
		{
			if (value == null)
			{
				return false;
			}
			else if (this.minimum != null && this.maximum != null)
			{
				// A finite interval
				if (this.minimumKind == IntervalEndpointKind.Open && this.maximumKind == IntervalEndpointKind.Open)
					// (a, b)
					return this.minimum.CompareTo(value) < 0
						&& value.CompareTo(this.maximum) < 0;
				else if (this.minimumKind == IntervalEndpointKind.Open && this.maximumKind == IntervalEndpointKind.Closed)
					// (a, b]
					return this.minimum.CompareTo(value) < 0
						&& value.CompareTo(this.maximum) <= 0;
				else if (this.minimumKind == IntervalEndpointKind.Closed && this.maximumKind == IntervalEndpointKind.Open)
					// [a, b)
					return this.minimum.CompareTo(value) <= 0
						&& value.CompareTo(this.maximum) < 0;
				else
					// [a, b]
					return this.minimum.CompareTo(value) <= 0
						&& value.CompareTo(this.maximum) <= 0;
			}
			else if (this.minimum != null)
			{
				if (this.minimumKind == IntervalEndpointKind.Open)
					// (a, +inf)
					return this.minimum.CompareTo(value) < 0;
				else
					// [a, +inf)
					return this.minimum.CompareTo(value) <= 0;
			}
			else if (this.maximum != null)
			{
				if (this.minimumKind == IntervalEndpointKind.Open)
					// (-inf, a)
					return value.CompareTo(this.maximum) < 0;
				else
					// (-inf, a]
					return value.CompareTo(this.maximum) <= 0;
			}
			else
			{
				// An unlimited interval: (-inf, +inf)
				return true;
			}
		}

		/// <summary>
		/// Determines whether this interval is contained inside the specified interval.
		/// </summary>
		/// <param name="interval">The other interval.</param>
		/// <returns><see langword="true"/> when this interval is contained inside <paramref name="interval"/>;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool IsSubintervalOf(Interval<T> interval)
		{
			return (interval.minimum != null ? this.Contains(interval.minimum) : this.minimum == null)
				&& (interval.maximum != null ? this.Contains(interval.maximum) : this.maximum == null);
		}

		/// <summary>
		/// Determines whether the specified interval is contained inside this interval.
		/// </summary>
		/// <param name="interval">The other interval.</param>
		/// <returns><see langword="true"/> when <paramref name="interval"/> is contained inside this interval;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool IsSuperintervalOf(Interval<T> interval)
		{
			return interval.IsSubintervalOf(this);
		}

		/// <summary>
		/// Determines whether this interval partially or fully overlaps the specified interval.
		/// </summary>
		/// <param name="interval">The other interval.</param>
		/// <returns><see langword="true"/> when this interval overlaps <paramref name="interval"/>;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool Overlaps(Interval<T> interval)
		{
			if (interval.minimum != null && interval.maximum != null)
			{
				// <a, b>
				return this.Contains(interval.minimum)
					|| this.Contains(interval.maximum);
			}
			else if (interval.minimum != null)
			{
				// <a, +inf)
				if (this.minimumKind == IntervalEndpointKind.Open)
					// (a, +inf)
					return this.maximum == null
						|| interval.minimum.CompareTo(this.maximum) < 0;
				else
					// [a, +inf)
					return this.maximum == null
						|| interval.minimum.CompareTo(this.maximum) <= 0;
			}
			else if (interval.maximum != null)
			{
				// (-inf, b>
				if (this.maximumKind == IntervalEndpointKind.Open)
					// (-inf, b)
					return this.minimum == null
						|| this.minimum.CompareTo(interval.maximum) < 0;
				else
					// (-inf, b]
					return this.minimum == null
						|| this.minimum.CompareTo(interval.maximum) <= 0;
			}
			else
			{
				// (-inf, +inf)
				return true;
			}
		}

		/// <summary>
		/// Returns the union of this interval with the specified interval.
		/// </summary>
		/// <param name="other">The other interval.</param>
		/// <returns>The union of the two intervals.</returns>
		/// <remarks>
		/// The intervals must overlap.
		/// </remarks>
		public Interval<T> UnionWith(Interval<T> other)
		{
			#region Contract
			Contract.Requires<ArgumentException>(Overlaps(other));
			#endregion
			throw new NotImplementedException();
		}

		/// <summary>
		/// Returns the intersection of this interval with the specified interval.
		/// </summary>
		/// <param name="other">The other interval.</param>
		/// <returns>The intersection of the two intervals.</returns>
		/// <remarks>
		/// The intervals must overlap.
		/// </remarks>
		public Interval<T> IntersectWith(Interval<T> other)
		{
			#region Contract
			Contract.Requires<ArgumentException>(Overlaps(other));
			#endregion
			throw new NotImplementedException();
		}

		/// <summary>
		/// Formats the value of the current instance.
		/// </summary>
		/// <returns>The value of the current instance.</returns>
		public override string ToString()
		{
			return ToString(null, null);
		}

		/// <summary>
		/// Formats the value of the current instance.
		/// </summary>
		/// <param name="provider">The provider to use to format the value; or <see langword="null"/> to use the default format provider.</param>
		/// <returns>The value of the current instance in the specified format.</returns>
		public string ToString(IFormatProvider provider)
		{
			return ToString(null, provider);
		}

		/// <summary>
		/// Formats the value of the current instance using the specified format.
		/// </summary>
		/// <param name="format">The format to use; or <see langword="null"/> to use the default value.</param>
		/// <param name="provider">The provider to use to format the value; or <see langword="null"/> to use the default format provider.</param>
		/// <returns>The value of the current instance in the specified format.</returns>
		public string ToString(string format, IFormatProvider provider)
		{
			format = String.IsNullOrEmpty(format) ? "G" : format;
			provider = provider ?? CultureInfo.CurrentCulture;

			if (format != "G")
				throw new FormatException(String.Format(CultureInfo.InvariantCulture, "The '{0}' format string is not supported.", format));

			return String.Format(provider, "[{0}; {1}]",
				this.minimum != null ? this.minimum.ToString() : "-inf",
				this.maximum != null ? this.maximum.ToString() : "+inf");
		}
	}
}
