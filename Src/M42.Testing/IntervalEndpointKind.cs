﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42 Testing.
// 
// M42 Testing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 Testing is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace M42.Testing
{
	/// <summary>
	/// Specifies the kind of end point.
	/// </summary>
	public enum IntervalEndpointKind : byte
	{
		/// <summary>
		/// An open endpoint. The value itself is not included.
		/// </summary>
		Open = 0,
		/// <summary>
		/// A closed endpoint. The value itself is included.
		/// </summary>
		Closed,
	}
}
