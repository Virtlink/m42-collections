﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42 Testing.
// 
// M42 Testing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 Testing is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;

namespace M42.Testing
{
	partial class TestDataProvider
	{
		/// <summary>
		/// Generates a random value.
		/// </summary>
		/// <param name="provider">The test data provider.</param>
		/// <returns>The random value.</returns>
		protected delegate object GeneratorDelegate(TestDataProvider provider);

		/// <summary>
		/// Manages the generator functions.
		/// </summary>
		protected sealed class GeneratorManager
		{
			/// <summary>
			/// The random value generators for primitive types.
			/// </summary>
			private static readonly Dictionary<Type, GeneratorDelegate> PrimitiveGenerators = new Dictionary<Type, GeneratorDelegate>()
			{
				{ typeof(SByte), p => p.GetSByte() },
				{ typeof(Byte), p => p.GetByte() },
				{ typeof(Int16), p => p.GetInt16() },
				{ typeof(UInt16), p => p.GetUInt16() },
				{ typeof(Int32), p => p.GetInt32() },
				{ typeof(UInt32), p => p.GetUInt32() },
				{ typeof(Int64), p => p.GetInt64() },
				{ typeof(UInt64), p => p.GetUInt64() },
				{ typeof(Single), p => p.GetSingle() },
				{ typeof(Double), p => p.GetDouble() },
				{ typeof(Char), p => p.GetChar() },
				{ typeof(Boolean), p => p.GetBoolean() },
				{ typeof(Object), p => p.GetObject() },
				{ typeof(DateTime), p => p.GetDateTime() },
				{ typeof(String), p => p.GetString(10) },
			};

			/// <summary>
			/// The random value generators.
			/// </summary>
			private Dictionary<Type, GeneratorDelegate> generators = null;

			/// <summary>
			/// Registers a value generator.
			/// </summary>
			/// <param name="type">The type for which the generator can generate values.</param>
			/// <param name="generator">The generator.</param>
			/// <remarks>
			/// When a generator is already registered for the specified type,
			/// it is overridden. The primitive types cannot be overridden.
			/// </remarks>
			public void Register(Type type, GeneratorDelegate generator)
			{
				#region Contract
				Contract.Requires<ArgumentNullException>(type != null);
				Contract.Requires<ArgumentNullException>(generator != null);
				#endregion
				this.generators = this.generators ?? new Dictionary<Type, GeneratorDelegate>();

				this.generators[type] = generator;
			}

			/// <summary>
			/// Gets the generator for the specified type.
			/// </summary>
			/// <param name="type">The type.</param>
			/// <returns>The <see cref="GeneratorDelegate"/>.</returns>
			public GeneratorDelegate GetGenerator(Type type)
			{
				#region Contract
				Contract.Requires<ArgumentNullException>(type != null);
				Contract.Ensures(Contract.Result<GeneratorDelegate>() != null);
				#endregion
				GeneratorDelegate generator;
				if (!PrimitiveGenerators.TryGetValue(type, out generator) && (this.generators == null || !this.generators.TryGetValue(type, out generator)))
					throw new NotSupportedException(String.Format(CultureInfo.InvariantCulture, "The type '{0}' is not supported.", type.Name));
				return generator;
			}
		}
	}
}
