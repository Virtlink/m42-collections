﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// An array.
	/// </summary>
	/// <typeparam name="T">The type of elements in the array.</typeparam>
	/// <remarks>
	/// Use the <see cref="Array{T}"/> class for fast working with arrays.
	/// Use the <see cref="ObjectModel.FixedSizeList{T}"/> class for a fixed-size
	/// collection whose getter and setter behavior can be overridden.
	/// </remarks>
	[DataContract]
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public sealed class Array<T> : IMutableArray<T>, IFreezable
	{
		// NOTE: This is internal to allow ArrayList.CopyTo() fast access.
		/// <summary>
		/// The underlying array.
		/// </summary>
		[DataMember]
		internal T[] innerArray;

		/// <summary>
		/// Whether the underlying array is safe (no references to it have
		/// been leaked) or not.
		/// </summary>
		private bool safe;

		#region Predefined
		private static volatile Array<T> empty;
		/// <summary>
		/// Gets an empty array.
		/// </summary>
		/// <value>An empty array.</value>
		/// <remarks>
		/// As this array cannot be changed, it is automatically frozen.
		/// </remarks>
		public static Array<T> Empty
		{
			get
			{
				if (empty == null)
				{
					empty = new Array<T>(0);
					empty.Freeze();
				}
				return empty;
			}
		}
		#endregion

		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return innerArray.Length;
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.Count == 0;
			}
		}

		private bool isFrozen = false;
		/// <inheritdoc />
		public bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.isFrozen;
			}
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IMutableList<T>
				return this.innerArray[index];
			}
			set
			{
				// CONTRACT: Inherited from IMutableList<T>
				this.innerArray[index] = value;
			}
		}

		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this.innerArray[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		#region Comparisons
		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the equality comparer used to compare elements in this list.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		/// <summary>
		/// Compares two values.
		/// </summary>
		/// <param name="expected">The expected value.</param>
		/// <param name="value">The value to compare to.</param>
		/// <returns><see langword="true"/> when the values are equal;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		private bool AreEqual(object expected, T value)
		{
			return (expected == null || expected is T)
				&& this.Comparer.Equals((T)expected, value);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Array{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		public Array(int length)
			: this(length, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Array{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="comparer">The equality comparer used to compare value
		/// in this list.</param>
		public Array(int length, SCG.IEqualityComparer<T> comparer)
			: this(checked((uint)length), comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Array{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		public Array(uint length)
			: this(length, SCG.EqualityComparer<T>.Default)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Array{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="comparer">The equality comparer used to compare value
		/// in this list.</param>
		public Array(uint length, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.innerArray = new T[length];
			this.comparer = comparer;
			this.safe = true;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Array{T}"/> class.
		/// </summary>
		/// <param name="array">The inner array.</param>
		public Array(params T[] array)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(array != null);
			#endregion
			this.innerArray = array;
			this.comparer = SCG.EqualityComparer<T>.Default;
			this.safe = false;
		}
		#endregion

		#region Conversions
		/// <summary>
		/// Converts an <see cref="Array{T}"/> to a .NET array.
		/// </summary>
		/// <param name="array">The array to convert, or <see langword="null"/>.</param>
		/// <returns>The resulting .NET array;
		/// or <see langword="null"/>.</returns>
		public static explicit operator T[](Array<T> array)
		{
			#region Contract
			Contract.Requires<ArgumentException>(array == null || !array.IsFrozen);
			#endregion
			if (array == null)
				return null;

			array.safe = false;
			return array.innerArray;
		}

		/// <summary>
		/// Converts a .NET array to an <see cref="Array{T}"/>.
		/// </summary>
		/// <param name="array">The array to convert, or <see langword="null"/>.</param>
		/// <returns>The resulting <see cref="Array{T}"/>;
		/// or <see langword="null"/>.</returns>
		public static implicit operator Array<T>(T[] array)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(array != null);
			Contract.Ensures(Contract.Result<Array<T>>() != null);
			#endregion
			if (array == null)
				return null;

			return new Array<T>(array);
		}
		#endregion

		#region SetAll() SetRange()
		/// <summary>
		/// Sets all elements to a particular value.
		/// </summary>
		/// <param name="value">The new value for each element.</param>
		public void SetAll(T value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			SetRange(0, Count, value);
		}

		/// <summary>
		/// Sets all elements to a particular value.
		/// </summary>
		/// <param name="valueProvider">A function that provides a new value
		/// for each element.</param>
		public void SetAll(Func<int, T> valueProvider)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(valueProvider != null);
			#endregion
			SetRange(0, Count, valueProvider);
		}

		/// <summary>
		/// Sets a range of elements to a particular value.
		/// </summary>
		/// <param name="start">The zero-based index of the first element
		/// to set the value of.</param>
		/// <param name="count">The number of elements to set
		/// the value of.</param>
		/// <param name="value">The new value for each element.</param>
		public void SetRange(int start, int count, T value)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(start >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start <= Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start + count <= Count);
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			int end = start + count;
			for (int i = start; i < end; i++)
			{
				this[i] = value;
			}
		}

		/// <summary>
		/// Sets a range of elements to a particular value.
		/// </summary>
		/// <param name="start">The zero-based index of the first element
		/// to set the value of.</param>
		/// <param name="count">The number of elements to set
		/// the value of.</param>
		/// <param name="valueProvider">A function that provides a new value
		/// for each element.</param>
		public void SetRange(int start, int count, Func<int, T> valueProvider)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(start >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start <= Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start + count <= Count);
			Contract.Requires<ArgumentNullException>(valueProvider != null);
			#endregion
			int end = start + count;
			for (int i = start; i < end; i++)
			{
				this[i] = valueProvider(i);
			}
		}
		#endregion

		/// <summary>
		/// Creates a clone of this array.
		/// </summary>
		/// <returns>The array's clone.</returns>
		public Array<T> Clone()
		{
			var innerArray = (T[])this.innerArray.Clone();
			var clone = new Array<T>(innerArray);
			// We throw away the reference so this array
			// is still 'safe'.
			clone.MakeSafe();
			return clone;
		}

		/// <summary>
		/// Resets all elements in this array to the default value.
		/// </summary>
		public void ResetAll()
		{
			System.Array.Clear(this.innerArray, 0, this.innerArray.Length);
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetMember(object value, out T result)
		{
			bool isPresent;
			result = ((ICollection<T>)this).GetMember(value, out isPresent);
			return isPresent;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			var index = ((IList<T>)this).IndexOf(value);
			isPresent = (index != null);
			T result = default(T);
			if (isPresent)
				result = this[(int)index];
			return result;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <summary>
		/// Returns whether the collection contains an element that is equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when the collection contains an
		/// element that is equal to <paramref name="value"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>
		/// The <see cref="Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// To specify a custom comparer, use the appropriate
		/// <see cref="System.Linq.Enumerable.Contains"/> overload.
		/// </para>
		/// </remarks>
		[Pure]
		public bool Contains(T value)
		{
			return ((ICollection<T>)this).Contains(value);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return ((IList<T>)this).IndexOf(value) != null;
		}

		/// <summary>
		/// Returns the index in the collection of an element that is
		/// considered to be equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The zero-based index of the first element that is
		/// considered to be equal to the specified value;
		/// or <see langword="null"/> when no such element
		/// could be found.</returns>
		/// <remarks>
		/// <para>
		/// Multiple calls to this method on a list that has not
		/// changed in between calls will return the same index.
		/// </para>
		/// <para>
		/// The <see cref="Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// </para>
		/// </remarks>
		[Pure]
		public int? IndexOf(T value)
		{
			return ((IList<T>)this).IndexOf((object)value);
		}

		/// <inheritdoc />
		int? IList.IndexOf(object value)
		{
			for (int i = 0; i < this.innerArray.Length; i++)
			{
				if (AreEqual(value, this.innerArray[i]))
					return i;
			}
			return null;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			return ((SCG.IEnumerable<T>)this.innerArray).GetEnumerator();
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetEnumerator();
		}

		//private IList<T> frozenList = null;
		/// <inheritdoc />
		public void Freeze()
		{
			// CONTRACT: Inherited from IFreezable
			this.isFrozen = true;
			this.innerArray = GetFrozenInnerArray();
			this.MakeSafe();

			//if (this.frozenList != null)
			//{
			//	// Assign the original inner array or a clone of it
			//	// to this array. The clone is used to prevent unauthorized modifications.
			//	this.innerArray = GetFrozenInnerArray();
			//	this.safe = true;
			//	this.frozenList = new FrozenList<T>(this.innerArray.Length, this, this.comparer);
			//}
			//return this.frozenList;
		}

		/// <summary>
		/// Returns the inner array that may be frozen.
		/// </summary>
		/// <returns>A .NET array.</returns>
		private T[] GetFrozenInnerArray()
		{
			if (this.safe)
				// The reference has not been leaked. We can safely freeze
				// the collection.
				return this.innerArray;
			else
				// The reference might have been leaked. Clone the array.
				return (T[])this.innerArray.Clone();
		}

		/// <summary>
		/// Makes the array safe, indicating that there are no references
		/// to the inner array.
		/// </summary>
		internal void MakeSafe()
		{
			this.safe = true;
		}

		#region System.Collections.Generic.IList<T>
#if false
		/// <inheritdoc />
		int SCG.ICollection<T>.Count
		{
			get { return this.Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.IsReadOnly
		{
			get { return this.IsFrozen; }
		}

		/// <inheritdoc />
		T SCG.IList<T>.this[int index]
		{
			get
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				#endregion
				return this[index];
			}
			set
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				if (!IsValidMember(value))
					throw new ArgumentException("The value is not valid.", "value");
				#endregion
				this[index] = value;
			}
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<T>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<T>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Clear()
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		[ContractVerification(false)]
		bool SCG.ICollection<T>.Contains(T item)
		{
			return this.Contains(item);
		}

		/// <inheritdoc />
		int SCG.IList<T>.IndexOf(T item)
		{
			int? index = this.IndexOf(item);
			return index ?? -1;
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}

		/// <inheritdoc />
		SCG.IEnumerator<T> SCG.IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}
#endif
		#endregion
	}
}
