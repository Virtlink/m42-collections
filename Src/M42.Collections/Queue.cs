﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using SCG = System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace M42.Collections
{
	/// <summary>
	/// A linear queue.
	/// </summary>
	/// <typeparam name="T">The type of elements in the queue.</typeparam>
	/// <remarks>
	/// Enumerating the stack returns the elements of the stack from front to back.
	/// </remarks>
	public class Queue<T> : IMutableSequence<T>, IList<T>
	{
		/// <summary>
		/// The internally used linked list.
		/// </summary>
		private readonly LinkedList<T> innerList;
		
		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.innerList.Count;
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T> and ISequence<T>
				return Count == 0;
			}
		}

		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get { return false; }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Queue{T}"/> class.
		/// </summary>
		public Queue()
		{
			this.innerList = new LinkedList<T>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Queue{T}"/> class.
		/// </summary>
		/// <param name="elements">The elements used to populate the queue.</param>
		/// <remarks>
		/// The <paramref name="elements"/> must be from queue front to back.
		/// </remarks>
		public Queue(SCG.IEnumerable<T> elements)
			: this()
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
			foreach (var element in elements)
			{
				Enqueue(element);
			}
		}
		#endregion

		/// <summary>
		/// Removes all elements from the queue.
		/// </summary>
		/// <exception cref="CollectionFrozenException">
		/// The queue is frozen.
		/// </exception>
		public void Clear()
		{
			#region Contract
			Contract.Requires<CollectionFrozenException>(!((ICollection)this).IsFrozen);
			#endregion
			this.innerList.Clear();
		}

		/// <inheritdoc />
		public T Peek()
		{
			// CONTRACT: Inherited from ISequence<T>
			var head = this.innerList.First;
			if (head == null)
				throw new SequenceEmptyException();
			return head.Value;
		}

		/// <summary>
		/// Removes and returns the next element from the queue.
		/// </summary>
		/// <returns>The next element in the queue.</returns>
		public T Dequeue()
		{
			var head = this.innerList.First;
			if (head == null)
				throw new SequenceEmptyException();
			var value = head.Value;
			head.Remove();
			return value;
		}

		/// <summary>
		/// Adds a value to the queue.
		/// </summary>
		/// <param name="value">The value to add to the queue.</param>
		public void Enqueue(T value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			this.innerList.Add(value);
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			return ((ICollection<T>)this.innerList).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return ((ICollection<T>)this.innerList).GetMember(value);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection)this.innerList).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection)this.innerList).GetMember(value);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			return this.innerList.Contains(value);
		}

		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this.innerList[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this.innerList[index];
			}
		}

		/// <inheritdoc />
		int? IList.IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			return this.innerList.IndexOf(value);
		}

		/// <inheritdoc />
		/// <remarks>
		/// Override this member to restrict the values that are allowed as
		/// members of this sequence.
		/// </remarks>
		public virtual bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from IEnumerable<T>
			return this.innerList.GetEnumerator();
		}

		#region Details
		/// <inheritdoc />
		T IMutableSequence<T>.Take()
		{
			// CONTRACT: Inherited from ISequence<T>
			return Dequeue();
		}

		/// <inheritdoc />
		bool IMutableSequence<T>.Add(T element)
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			Enqueue(element);
			return true;
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from IEnumerable
			return GetEnumerator();
		}
		#endregion
	}
}
