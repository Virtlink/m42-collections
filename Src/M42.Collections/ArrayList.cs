﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.ObjectModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;

namespace M42.Collections
{
	/// <summary>
	/// A mutable list of elements of type <typeparamref name="T"/>.
	/// </summary>
	/// <typeparam name="T">The type of the elements in the list.</typeparam>
	/// <remarks>
	/// <para>This list uses an array internally.</para>
	/// <para>An <see cref="ArrayList{T}"/> has O(1) time complexity for
	/// getting or setting the value of an element. It has O(n) time complexity
	/// for inserting and removing elements, where n is the number of elements
	/// in the collection.</para>
	/// </remarks>
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public class ArrayList<T> : MutableListBase<T>, IFreezable
	{
		/// <summary>
		/// The modification version.
		/// </summary>
		private int version;

		/// <summary>
		/// The underlying array.
		/// </summary>
		private Array<T> innerArray;

		private int count = 0;
		/// <inheritdoc />
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public sealed override int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.count;
			}
		}

		private bool isFrozen = false;
		/// <inheritdoc />
		public override bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return isFrozen;
			}
		}

		#region Capacity
		/// <summary>
		/// The default initial capacity.
		/// </summary>
		private const int DefaultInitialCapacity = 4;

		/// <summary>
		/// The inclusive maximum capacity.
		/// </summary>
		private const int MaxCapacity = Int32.MaxValue;

		/// <summary>
		/// Gets or sets the capacity of the underlying array.
		/// </summary>
		/// <value>The capacity of the underlying array.</value>
		protected int Capacity
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<int>() >= 0);
				Contract.Ensures(Contract.Result<int>() >= Count);
				#endregion
				return this.innerArray.Count;
			}
			set
			{
				#region Contract
				Contract.Requires<ArgumentOutOfRangeException>(value >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(value >= Count);
				#endregion
				SetCapacity(value);
			}
		}

		/// <summary>
		/// Ensures that the internal array has a capacity equal to or
		/// greater than the specified capacity.
		/// </summary>
		/// <param name="minCapacity">The minimum capacity of the internal
		/// array.</param>
		protected void EnsureCapacity(int minCapacity)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minCapacity >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(minCapacity <= MaxCapacity);
			#endregion
			if (this.innerArray.Count >= minCapacity)
				return;
			int capacity = Math.Max(Math.Max(this.innerArray.Count, DefaultInitialCapacity) * 2, minCapacity);
			SetCapacity(capacity);
		}

		/// <summary>
		/// Sets the exact capacity of the underlying array.
		/// </summary>
		/// <param name="capacity">The new capacity of the underlying
		/// array.</param>
		private void SetCapacity(int capacity)
		{
			#region Contract
			Contract.Requires(capacity >= 0);
			Contract.Requires(capacity <= MaxCapacity);
			#endregion
			if (capacity > 0)
			{
				Array<T> newInnerArray = new Array<T>(capacity);
				if (this.count > 0)
					ArrayExt.Copy(this.innerArray, newInnerArray, this.count);
				this.innerArray = newInnerArray;
			}
			else
				this.innerArray = Array<T>.Empty;
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="ArrayList{T}"/> class.
		/// </summary>
		public ArrayList()
			: this(DefaultInitialCapacity, EqualityComparer<T>.Default)
		{
		}

		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="ArrayList{T}"/> class.
		/// </summary>
		/// <param name="capacity">The initial capacity of the list.</param>
		public ArrayList(int capacity)
			: this(capacity, EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(capacity > 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="ArrayList{T}"/> class.
		/// </summary>
		/// <param name="capacity">The initial capacity of the list.</param>
		/// <param name="comparer">The equality comparer to use to compare the
		/// elements in this list.</param>
		public ArrayList(int capacity, IEqualityComparer<T> comparer)
			: base(comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(capacity > 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.innerArray = new Array<T>(capacity);
			this.count = 0;
		}

		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="ArrayList{T}"/> class.
		/// </summary>
		/// <param name="comparer">The equality comparer to use to compare the
		/// elements in this list.</param>
		public ArrayList(IEqualityComparer<T> comparer)
			: this(DefaultInitialCapacity, comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="ArrayList{T}"/> class.
		/// </summary>
		/// <param name="collection">A collection of elements.</param>
		public ArrayList(IEnumerable<T> collection)
			: base(EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(collection != null);
			#endregion
			this.innerArray = collection.ToM42Array();
			this.count = this.innerArray.Count;
		}
		#endregion

		/// <inheritdoc />
		protected override void ClearElements()
		{
			this.version++;
			if (this.count > 0)
				this.innerArray.ResetAll();
			this.count = 0;
		}

		/// <inheritdoc />
		protected override T GetElement(int index)
		{
			return this.innerArray[index];
		}

		/// <inheritdoc />
		protected override void SetElement(int index, T value)
		{
			this.version++;
			this.innerArray[index] = value;
		}

		/// <inheritdoc />
		protected override bool InsertElement(int index, T value)
		{
			this.version++;
			EnsureCapacity(this.count + 1);
			ArrayExt.Copy(this.innerArray, index, this.innerArray, index + 1, this.count - index);
			this.innerArray[index] = value;
			this.count++;
			return true;
		}

		/// <inheritdoc />
		protected override bool RemoveElement(int index)
		{
			this.version++;
			this.count--;
			ArrayExt.Copy(this.innerArray, index + 1, this.innerArray, index, this.count - index);
			this.innerArray[this.count] = default(T);
			return true;
		}

		/// <inheritdoc />
		protected override int? IndexOfElement(object value)
		{
			for (int i = 0; i < this.count; i++)
			{
				if (AreEqual(value, this.innerArray[i]))
					return i;
			}
			return null;
		}

		/// <inheritdoc />
		public void Freeze()
		{
			// CONTRACT: Inherited from IFreezable
			this.isFrozen = true;
		}

		/// <inheritdoc />
		public sealed override IEnumerator<T> GetEnumerator()
		{
			int version = this.version;
			for (int i = 0; i < this.count; i++)
			{
				if (this.version != version)
					throw new InvalidOperationException("The collection was modified.");
				yield return this.innerArray[i];
			}
		}

		#region CopyTo()
		/// <summary>
		/// Copies all elements from the list to the specified array.
		/// </summary>
		/// <param name="array">The array to copy the elements to.</param>
		public void CopyTo(Array<T> array)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(array != null);
			Contract.Requires<ArgumentException>(Count <= array.Count);
			#endregion
			CopyTo(array, 0);
		}

		/// <summary>
		/// Copies all elements from the list to the specified array.
		/// </summary>
		/// <param name="array">The array to copy the elements to.</param>
		/// <param name="arrayIndex">The zero-based index in
		/// <paramref name="array"/> where the first copied element
		/// is stored.</param>
		public void CopyTo(Array<T> array, int arrayIndex)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(array != null);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex <= array.Count);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex + Count <= array.Count);
			#endregion
			CopyTo(0, array, arrayIndex, this.count);
		}

		/// <summary>
		/// Copies a range of elements from the list to the specified array.
		/// </summary>
		/// <param name="startIndex">The zero-based index of the first element
		/// to copy.</param>
		/// <param name="array">The array to copy the elements to.</param>
		/// <param name="arrayIndex">The zero-based index in
		/// <paramref name="array"/> where the first copied element
		/// is stored.</param>
		/// <param name="count">The number of elements to copy.</param>
		public void CopyTo(int startIndex, Array<T> array, int arrayIndex, int count)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(startIndex >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(startIndex <= Count);
			Contract.Requires<ArgumentNullException>(array != null);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex <= array.Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(startIndex + count <= Count);
			Contract.Requires<ArgumentOutOfRangeException>(arrayIndex + count <= array.Count);
			#endregion
			ArrayExt.Copy(this.innerArray, startIndex, array, arrayIndex, count);
		}
		#endregion
	}
}
