﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.IO;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// An array of elements read from a binary reader.
	/// </summary>
	/// <typeparam name="T">The type of elements in the list.</typeparam>
	public abstract class BinaryReaderArray<T> : IList<T>
	{
		/// <summary>
		/// The size of an element, in bytes.
		/// </summary>
		private readonly int elementSize;

		/// <summary>
		/// The offset of the first element, relative to the start of the stream.
		/// </summary>
		private readonly long offset;

		private readonly BinaryReader reader;
		/// <summary>
		/// Gets the binary reader.
		/// </summary>
		/// <value>The <see cref="BinaryReader"/>.</value>
		public BinaryReader Reader
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<BinaryReader>() != null);
				#endregion
				return this.reader;
			}
		}

		/// <inheritdoc />
		public abstract int Count { get; }

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection
				return Count == 0;
			}
		}

		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection
				return false;
			}
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return ReadElementAtIndex(index);
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="BinaryReaderArray{T}"/> class.
		/// </summary>
		/// <param name="reader">The binary reader.</param>
		/// <param name="offset">The offset of the first element, relative to the start of the stream.</param>
		/// <param name="elementSize">The size of an element, in bytes.</param>
		public BinaryReaderArray(BinaryReader reader, long offset, int elementSize)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(reader != null);
			Contract.Requires<ArgumentOutOfRangeException>(offset >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(elementSize >= 0);
			#endregion
			this.reader = reader;
			this.offset = offset;
			this.elementSize = elementSize;
		}
		#endregion

		/// <summary>
		/// Reads the element with the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element.</param>
		/// <returns>The element that was read.</returns>
		protected T ReadElementAtIndex(int index)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(index < Count);
			#endregion
			reader.BaseStream.Seek(offset + index * elementSize, SeekOrigin.Begin);
			var element = ReadElement();
			return element;
		}

		/// <summary>
		/// Reads an element from the current position of the binary reader.
		/// </summary>
		/// <returns>The element that was read.</returns>
		protected abstract T ReadElement();

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection
			return IndexOf(value) != null;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList
			for (int i = 0; i < this.Count; i++)
			{
				T current = this[i];
				if (Object.Equals(value, current))
					return i;
			}
			return null;
		}

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			for (int i = 0; i < this.Count; i++)
			{
				T current = this[i];
				if (Object.Equals(value, current))
				{
					isPresent = true;
					return current;
				}
			}
			isPresent = false;
			return default(T);
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return GetMember(value);
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < this.Count; i++)
			{
				yield return this[i];
			}
		}

		/// <inheritdoc />
		SC.IEnumerator SC.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.reader != null);
			Contract.Invariant(this.offset >= 0);
			Contract.Invariant(this.elementSize >= 0);
		}
		#endregion
	}
}
