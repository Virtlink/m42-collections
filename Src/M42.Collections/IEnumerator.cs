﻿using System;
using SCG = System.Collections.Generic;
using System.Text;

namespace M42.Collections
{
	/// <summary>
	/// Iterates over a sequence.
	/// </summary>
	/// <typeparam name="T">The type of elements in the iterated sequence.</typeparam>
	public interface IEnumerator<out T> : IDisposable, SCG.IEnumerator<T>
	{
		/// <summary>
		/// Gets the element at the enumerator's current position in the sequence.
		/// </summary>
		/// <value>The current element.</value>
		T Current
		{ get; }

		/// <summary>
		/// Moves the enumerator to the next element in the sequence.
		/// </summary>
		/// <returns><see langword="true"/> if there is at least one more element
		/// in the sequence; otherwise, <see langword="false"/> when the enumerator
		/// reached the end of the sequence.</returns>
		/// <exception cref="InvalidOperationException">
		/// The sequence has been modified while being enumerated.
		/// </exception>
		bool MoveNext();
	}
}
