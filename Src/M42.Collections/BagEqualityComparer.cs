﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// Compares two bags or multisets for equality.
	/// </summary>
	/// <typeparam name="T">The type of elements in the bag.</typeparam>
	public sealed class BagEqualityComparer<T> : SCG.IEqualityComparer<SCG.IEnumerable<T>>
	{
		private readonly SCG.IEqualityComparer<T> elementComparer;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="BagEqualityComparer{T}"/> class.
		/// </summary>
		public BagEqualityComparer()
			: this(null)
		{
			// Nothing to do.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="BagEqualityComparer{T}"/> class.
		/// </summary>
		/// <param name="elementComparer">The comparer to use to compare two elements;
		/// or <see langword="null"/> to use the default comparer.</param>
		public BagEqualityComparer(SCG.IEqualityComparer<T> elementComparer)
		{
			this.elementComparer = elementComparer ?? SCG.EqualityComparer<T>.Default;
		}
		#endregion

		/// <inheritdoc />
		public bool Equals(SCG.IEnumerable<T> x, SCG.IEnumerable<T> y)
		{
			// CONTRACT: Inherited from IEqualityComparer<T>
			if (Object.ReferenceEquals(x, y))
				// Either they are the same object,
				// or they are both null.
				return true;
			else if (x == null || y == null)
				// One of them is null.
				return false;

			var xCollection = x as ICollection;
			var yCollection = y as ICollection;
			if (xCollection != null && yCollection != null)
			{
				if (xCollection.Count != yCollection.Count)
					// They have a different number of elements.
					return false;
				else if (xCollection.IsEmpty)
					// Both are empty.
					return true;
			}

			var xOccurrences = CountOccurrences(x);
			var yOccurrences = CountOccurrences(y);

			foreach (var pair in xOccurrences)
			{
				// NOTE: 'yCount' defaults to 0 when the element doesn't exist.
				int xCount = pair.Value;
				int yCount = yOccurrences.GetValue(pair.Key);

				if (xCount != yCount)
					return false;
			}
			return true;
		}

		/// <inheritdoc />
		public int GetHashCode(SCG.IEnumerable<T> obj)
		{
			// CONTRACT: Inherited from IEqualityComparer<T>
			var occurrences = CountOccurrences(obj);

			// NOTE: Equal objects should return equal hash codes.
			// Since we can't guarantee the ordering of the elements
			// (e.g. when IComparable<T> is not implemented and the bag's order is not stable)
			// we use the number of occurrences of each element to
			// calculate the hash code.

			int hash = 17;
			unchecked
			{
				foreach (var count in occurrences.Values.OrderBy(x => x))
				{
					hash = hash * 29 + count.GetHashCode();
				}
			}
			return hash;
		}

		/// <summary>
		/// Counts the number of occurrences of equal elements in the sequence.
		/// </summary>
		/// <param name="sequence">The sequence of elements.</param>
		/// <returns>A keyed collection that has the number of occurrences for each element.</returns>
		private IKeyedCollection<T, int> CountOccurrences(SCG.IEnumerable<T> sequence)
		{
			var occurrences = new HashMap<T, int>(this.elementComparer);
			foreach (var element in sequence)
			{
				// NOTE: 'count' defaults to 0 when the element doesn't exist.
				int count = occurrences.GetValue(element);
				occurrences[element] = count + 1;
			}
			return occurrences;
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.elementComparer != null);
		}
		#endregion
	}
}
