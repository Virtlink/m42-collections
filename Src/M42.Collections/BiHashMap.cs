﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A bi-directional hash map.
	/// </summary>
	/// <remarks>
	/// Each key and each value in this map is unique.
	/// </remarks>
	public class BiHashMap<TKey, TValue> : IMutableKeyedCollection<TKey, TValue>
	{
		private readonly HashMap<TKey, TValue> innerMap;
		private readonly HashMap<TValue, TKey> inverseMap;

		private readonly BiHashMap<TValue, TKey> inverse;
		/// <summary>
		/// Gets the inverse hash map.
		/// </summary>
		/// <value>The <see cref="BiHashMap{TValue, TKey}"/> for the inverse relation.</value>
		/// <remarks>
		/// Mutating the inverse also mutates this instance, and vice versa.
		/// </remarks>
		public BiHashMap<TValue, TKey> Inverse
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<BiHashMap<TValue, TKey>>() != null);
				#endregion
				return this.inverse;
			}
		}

		/// <inheritdoc />
		public int Count
		{
			get { return this.innerMap.Count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return Count == 0; }
		}

		/// <inheritdoc />
		public ICollection<TKey> Keys
		{
			get { return this.innerMap.Keys; }
		}

		/// <inheritdoc />
		public ICollection<TValue> Values
		{
			get { return this.inverseMap.Keys; }
		}

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get
			{
				return this.innerMap[key];
			}
			set
			{
				if (this.inverseMap.ContainsKey(value))
					throw new DuplicateKeyException("The value already exists in the inverse map.");

				this.innerMap[key] = value;
				this.inverseMap[value] = key;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="BiHashMap{TKey, TValue}"/> class.
		/// </summary>
		public BiHashMap()
		{
			this.innerMap = new HashMap<TKey, TValue>();
			this.inverseMap = new HashMap<TValue, TKey>();
			// NOTE: This must come after setting the innerMap and inverseMap fields.
			this.inverse = new BiHashMap<TValue, TKey>(this);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="BiHashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="inverse">The inverse hash map.</param>
		private BiHashMap(BiHashMap<TValue, TKey> inverse)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(inverse != null);
			#endregion
			this.innerMap = inverse.inverseMap;
			this.inverseMap = inverse.innerMap;
			this.inverse = inverse;
		}
		#endregion

		#region Mutations
		/// <inheritdoc />
		public bool Add(TKey key, TValue value)
		{
			if (this.innerMap.ContainsKey(key) || this.inverseMap.ContainsKey(value))
				return false;

			bool success1 = this.innerMap.Add(key, value);
			bool success2 = this.inverseMap.Add(value, key);
			Contract.Assume(success1 == success2);
			return success1 && success2;
		}

		/// <inheritdoc />
		public bool RemoveKey(TKey key)
		{
			TValue value;
			if (!TryGetValue(key, out value))
				return false;
			return RemoveKeyValue(key, value);
		}

		/// <inheritdoc />
		private bool RemoveKeyValue(TKey key, TValue value)
		{
			bool success1 = this.innerMap.RemoveKey(key);
			bool success2 = this.inverseMap.RemoveKey(value);
			Contract.Assume(success1 == success2);
			return success1 && success2;
		}

		/// <inheritdoc />
		public void Clear()
		{
			this.innerMap.Clear();
			this.inverseMap.Clear();
		}

		/// <inheritdoc />
		bool IMutableCollection<IAssociation<TKey, TValue>>.Add(IAssociation<TKey, TValue> association)
		{
			return Add(association.Key, association.Value);
		}

		/// <inheritdoc />
		bool IMutableKeyedCollection<TKey, TValue>.RemoveKey(object key)
		{
			if (!(key is TKey))
				return false;
			return RemoveKey((TKey)key);
		}

		/// <inheritdoc />
		bool IMutableCollection<IAssociation<TKey, TValue>>.Remove(object valueToRemove)
		{
			IAssociation<TKey, TValue> association = valueToRemove as IAssociation<TKey, TValue>;
			if (association == null)
				return false;

			TKey key = association.Key;
			TValue value = association.Value;

			// Check that Value is actually the value assigned to Key
			TValue actualValue;
			if (!TryGetValue(key, out actualValue) || !this.inverseMap.KeyComparer.Equals(actualValue, value))
				return false;

			return RemoveKeyValue(key, value);
		}
		#endregion

		/// <summary>
		/// Returns whether the map contains one or more elements
		/// with a key that is considered to be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <returns><see langword="true"/> when the collection contains the
		/// specified key; otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool ContainsKey(TKey key)
		{
			return this.innerMap.ContainsKey(key);
		}

		/// <inheritdoc />
		public virtual bool IsValidKey(object key)
		{
			return key is TKey;
		}

		/// <inheritdoc />
		public virtual bool IsValidValue(object value)
		{
			return value is TValue;
		}

		/// <summary>
		/// Attempts to get a value from the <see cref="BiHashMap{TKey, TValue}"/>.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <param name="value">The value that corresponds to the key.</param>
		/// <returns><see langword="true"/> when the key was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetValue(TKey key, out TValue value)
		{
			return this.innerMap.TryGetValue(key, out value);
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>A <see cref="SCG.IEnumerator{T}"/> that can be used
		/// to iterate through the collection.</returns>
		public SCG.IEnumerator<Association<TKey, TValue>> GetEnumerator()
		{
			return this.innerMap.GetEnumerator();
		}

		#region Explicit Interface Implementations
		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get { return false; }
		}

		/// <inheritdoc />
		TValue IKeyedCollection<TKey, TValue>.this[object key]
		{
			get { return ((IKeyedCollection<TKey, TValue>)this.innerMap)[key]; }
		}

		/// <inheritdoc />
		bool IKeyedCollection<TKey, TValue>.ContainsKey(object key)
		{
			return this.innerMap.ContainsKey(key);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			return ((ICollection<IAssociation<TKey, TValue>>)this.innerMap).Contains(value);
		}

		/// <inheritdoc />
		bool ICollection.IsValidMember(object value)
		{
			return value is IAssociation<TKey, TValue>;
		}

		/// <inheritdoc />
		TValue IKeyedCollection<TKey, TValue>.GetValue(object key, out bool isPresent)
		{
			return ((IKeyedCollection<TKey, TValue>)this.innerMap).GetValue(key, out isPresent);
		}

		/// <inheritdoc />
		TValue IKeyedCollection<TKey, TValue>.GetValue(object key)
		{
			return ((IKeyedCollection<TKey, TValue>)this.innerMap).GetValue(key);
		}

		/// <inheritdoc />
		IAssociation<TKey, TValue> ICollection<IAssociation<TKey, TValue>>.GetMember(object value, out bool isPresent)
		{
			return ((ICollection<Association<TKey, TValue>>)this.innerMap).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		IAssociation<TKey, TValue> ICollection<IAssociation<TKey, TValue>>.GetMember(object value)
		{
			return ((ICollection<Association<TKey, TValue>>)this.innerMap).GetMember(value);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<Association<TKey, TValue>>)this.innerMap).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<Association<TKey, TValue>>)this.innerMap).GetMember(value);
		}

		/// <inheritdoc />
		SCG.IEnumerator<IAssociation<TKey, TValue>> SCG.IEnumerable<IAssociation<TKey, TValue>>.GetEnumerator()
		{
			return ((SCG.IEnumerable<IAssociation<TKey, TValue>>)this.innerMap).GetEnumerator();
		}

		/// <inheritdoc />
		SC.IEnumerator SC.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.innerMap != null);
			Contract.Invariant(this.inverseMap != null);
			Contract.Invariant(this.inverse != null);
			Contract.Invariant(this.inverseMap.Count == this.inverseMap.Count);
		}
		#endregion
	}
}
