﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections.Specialized
{
	/// <summary>
	/// An array of bits.
	/// </summary>
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public sealed class BitArray : IMutableArray<bool>, SCG.IList<bool>
	{
		/// <summary>
		/// Number of bits per element in this array.
		/// </summary>
		private const int ElementLength = 32;
		/// <summary>
		/// Log2 of <see cref="ElementLength"/>.
		/// </summary>
		private const int ElementLengthLog = 5;
		/// <summary>
		/// A mask with all ones.
		/// </summary>
		private const int ElementMask = ElementLength - 1;

		/// <summary>
		/// The bit array.
		/// </summary>
		private uint[] bits;

		private int count;
		/// <inheritdoc />
		public int Count
		{
			get { return count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return count == 0; }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return false; }
		}

		/// <inheritdoc />
		public bool this[int index]
		{
			get
			{
				int elementIndex = index >> ElementLengthLog;
				int bitIndex = index & ElementMask;
				uint element = this.bits[elementIndex];
				uint bit = element & (uint)(1 << bitIndex);
				return bit != 0;
			}
			set
			{
				int elementIndex = index >> ElementLengthLog;
				int bitIndex = index & ElementMask;
				uint element = this.bits[elementIndex];
				uint bit = (uint)(1 << bitIndex);
				if (value == true)
					element |= bit;
				else
					element &= ~bit;
				this.bits[elementIndex] = element;
			}
		}

		/// <inheritdoc />
		bool IList<bool>.this[int index]
		{
			get { return this[index]; }
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get { return this[index]; }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="BitArray"/> class.
		/// </summary>
		/// <param name="length">The length of the bit array.</param>
		/// <param name="defaultValue">The default value for each bit.</param>
		public BitArray(int length, bool defaultValue)
			: this(length)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			if (defaultValue == true)
			{
				for (int i = 0; i < this.bits.Length; i++)
				{
					this.bits[i] = unchecked((uint)-1);
				}
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="BitArray"/> class.
		/// </summary>
		/// <param name="length">The length of the bit array.</param>
		public BitArray(int length)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
			int elementCount = GetElementCount(length);
			this.bits = new uint[elementCount];
			this.count = length;
		}
		#endregion

		#region SetAll() SetRange()
		/// <summary>
		/// Sets all elements to a particular value.
		/// </summary>
		/// <param name="value">The new value for each element.</param>
		public void SetAll(bool value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(((ICollection<bool>)this).IsValidMember(value));
			#endregion
			SetRange(0, Count, value);
		}

		/// <summary>
		/// Sets all elements to a particular value.
		/// </summary>
		/// <param name="valueProvider">A function that provides a new value
		/// for each element.</param>
		public void SetAll(Func<int, bool> valueProvider)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(valueProvider != null);
			#endregion
			SetRange(0, Count, valueProvider);
		}

		/// <summary>
		/// Sets a range of elements to a particular value.
		/// </summary>
		/// <param name="start">The zero-based index of the first element
		/// to set the value of.</param>
		/// <param name="count">The number of elements to set
		/// the value of.</param>
		/// <param name="value">The new value for each element.</param>
		public void SetRange(int start, int count, bool value)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(start >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start <= Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start + count <= Count);
			Contract.Requires<ArgumentException>(((ICollection<bool>)this).IsValidMember(value));
			#endregion
			int end = start + count;
			for (int i = start; i < end; i++)
			{
				this[i] = value;
			}
		}

		/// <summary>
		/// Sets a range of elements to a particular value.
		/// </summary>
		/// <param name="start">The zero-based index of the first element
		/// to set the value of.</param>
		/// <param name="count">The number of elements to set
		/// the value of.</param>
		/// <param name="valueProvider">A function that provides a new value
		/// for each element.</param>
		public void SetRange(int start, int count, Func<int, bool> valueProvider)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(start >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start <= Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(start + count <= Count);
			Contract.Requires<ArgumentNullException>(valueProvider != null);
			#endregion
			int end = start + count;
			for (int i = start; i < end; i++)
			{
				this[i] = valueProvider(i);
			}
		}
		#endregion

		/// <summary>
		/// Resets all elements in this array to the default value.
		/// </summary>
		public void ResetAll()
		{
			System.Array.Clear(this.bits, 0, this.bits.Length);
		}

		/// <summary>
		/// Gets the number of elements required for a particular length.
		/// </summary>
		/// <param name="length">The number of bits.</param>
		/// <returns>The number of elements.</returns>
		private static int GetElementCount(int length)
		{
			if (length == 0)
				return 0;
			int elementCount = (length - 1) >> ElementLengthLog;
			if ((length & ElementMask) != 0)
				elementCount++;
			return elementCount;
		}

		/// <inheritdoc />
		int? IList.IndexOf(object value)
		{
			if (!(value is bool))
				return null;
			bool toFind = (bool)value;
			for (int i = 0; i < this.count; i++)
			{
				if (this[i] == toFind)
					return i;
			}
			return null;
#if false
			bool toFind = (bool)value;
			// true:  00000000 00000000 00000000 00000000
			// false: 11111111 11111111 11111111 11111111
			uint quickCheck = toFind ? 0 : unchecked((uint)-1);

			int index = -1;
			uint element;
			// Check all whole elements.
			for (int i = 0; i < this.bits.Length; i++)
			{
				element = this.bits[i] ^ quickCheck;
				if (element == 0)
					continue;
				// Binary search on each of the elements.
				index = (i << ElementLengthLog);
				if ((element & 0xFFFF0000) != 0)
				{
					element >>= 16;
					index += 16;
				}
				if ((element & 0xFF00) != 0)
				{
					element >>= 8;
					index += 8;
				}
				if ((element & 0xF0) != 0)
				{
					element >>= 4;
					index += 4;
				}
				if ((element & 0xC) != 0)
				{
					element >>= 2;
					index += 2;
				}
				if ((element & 0x2) != 0)
				{
					index++;
				}
			}
			if (index >= 0 && index < this.count)
				return index;
			else
				return null;
#endif
		}

		/// <inheritdoc />
		bool ICollection<bool>.GetMember(object value, out bool isPresent)
		{
			isPresent = value is bool && ((ICollection<bool>)this).Contains(value);
			return isPresent ? (bool)value : false;
		}

		/// <inheritdoc />
		bool ICollection<bool>.GetMember(object value)
		{
			bool isPresent;
			return ((ICollection<bool>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<bool>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<bool>)this).GetMember(value);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			return ((IList<bool>)this).IndexOf(value) != null;
		}

		/// <inheritdoc />
		bool ICollection.IsValidMember(object value)
		{
			return value is bool;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<bool> GetEnumerator()
		{
			uint element;
			uint bit;
			for (int i = 0; i < this.bits.Length - 1; i++)
			{
				for (int j = 0; j < ElementLength; j++)
				{
					element = this.bits[i];
					bit = element & (uint)(1 << j);
					yield return (bit != 0);
				}
			}
			for (int j = 0; j < (this.count & ElementMask); j++)
			{
				element = this.bits[this.bits.Length - 1];
				bit = element & (uint)(1 << j);
				yield return (bit != 0);
			}
		}

		/// <inheritdoc />
		SC.IEnumerator SC.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#region System.Collections.Generic.IList<T>
		/// <inheritdoc />
		int SCG.ICollection<bool>.Count
		{
			get { return this.Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<bool>.IsReadOnly
		{
			get { return this.IsFrozen; }
		}

		/// <inheritdoc />
		bool SCG.IList<bool>.this[int index]
		{
			get
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				#endregion
				return this[index];
			}
			set
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				if (!((ICollection<bool>)this).IsValidMember(value))
					throw new ArgumentException("The value is not valid.", "value");
				#endregion
				this[index] = value;
			}
		}

		/// <inheritdoc />
		void SCG.ICollection<bool>.Add(bool item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<bool>.Insert(int index, bool item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		bool SCG.ICollection<bool>.Remove(bool item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<bool>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.ICollection<bool>.Clear()
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		[ContractVerification(false)]
		bool SCG.ICollection<bool>.Contains(bool item)
		{
			return ((ICollection<bool>)this).Contains(item);
		}

		/// <inheritdoc />
		int SCG.IList<bool>.IndexOf(bool item)
		{
			int? index = ((IList<bool>)this).IndexOf(item);
			return index ?? -1;
		}

		/// <inheritdoc />
		void SCG.ICollection<bool>.CopyTo(bool[] array, int arrayIndex)
		{
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}

		/// <inheritdoc />
		SCG.IEnumerator<bool> SCG.IEnumerable<bool>.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		#endregion
	}
}
