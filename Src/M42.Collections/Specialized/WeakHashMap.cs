﻿using System;
using System.Collections;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M42.Collections.Specialized
{
	/// <summary>
	/// A hash map with weak references to its values.
	/// </summary>
	/// <typeparam name="TKey">The type of the keys.</typeparam>
	/// <typeparam name="TValue">The type of the values.</typeparam>
	public class WeakHashMap<TKey, TValue> : SCG.IEnumerable<IAssociation<TKey, TValue>>
		where TValue : class
	{
		/// <summary>
		/// The inner map.
		/// </summary>
		private readonly HashMap<TKey, WeakReference> innerMap;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="WeakHashMap{TKey, TValue}"/> class.
		/// </summary>
		public WeakHashMap()
		{
			this.innerMap = new HashMap<TKey, WeakReference>();
		}
		#endregion

		/// <summary>
		/// Attempts to get the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The associated value; or the default value when not found.</param>
		/// <returns><see langword="true"/> when the key was present and returned;
		/// otherwise, <see langword="false"/>.</returns>
		public bool TryGetValue(TKey key, out TValue value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidKey(key));
			Contract.Ensures(IsValidValue(Contract.ValueAtReturn(out value)));
			#endregion

			WeakReference reference;
			if (!this.innerMap.TryGetValue(key, out reference))
			{
				value = default(TValue);
				return false;
			}

			value = (TValue)reference.Target;
			if (value == null)
			{
				// The value is no longer present.
				// Remove the key.
				this.innerMap.RemoveKey(key);
			}
			return value != null;
		}

		/// <summary>
		/// Adds the specified key-value pair to the map.
		/// </summary>
		/// <param name="key">The key to add.</param>
		/// <param name="value">The value that is associated with
		/// <paramref name="key"/>.</param>
		/// <returns><see langword="true"/> when the key and value were added;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// A key-value pair may sometimes not be added, for example, when an
		/// equal element already exists in a set.
		/// </remarks>
		public bool Add(TKey key, TValue value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidKey(key));
			Contract.Requires<ArgumentException>(IsValidValue(value));
			#endregion

			return this.innerMap.Add(key, new WeakReference(value));
		}

		/// <summary>
		/// Puts the specified key-value pair in the map.
		/// </summary>
		/// <param name="key">The key to add.</param>
		/// <param name="value">The value that is associated with
		/// <paramref name="key"/>.</param>
		/// <returns><see langword="true"/>.</returns>
		/// <remarks>
		/// If a key-value pair with the specified key is already present,
		/// it is replaced.
		/// </remarks>
		public bool Put(TKey key, TValue value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidKey(key));
			Contract.Requires<ArgumentException>(IsValidValue(value));
			#endregion
			this.innerMap[key] = new WeakReference(value);
			return true;
		}

		/// <summary>
		/// Removes the key-value pair with the specified key, if present.
		/// </summary>
		/// <param name="key">The key to remove.</param>
		/// <returns><see langword="true"/> when a key was found and
		/// removed; otherwise, <see langword="false"/> when no such key
		/// was found.</returns>
		public bool RemoveKey(TKey key)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidKey(key));
			#endregion

			return this.innerMap.RemoveKey(key);
		}

		/// <summary>
		/// Removes all key-value pairs from the map.
		/// </summary>
		public void Clear()
		{
			this.innerMap.Clear();
		}

		/// <summary>
		/// Cleans the map.
		/// </summary>
		/// <remarks>
		/// This method might reduce the memory footprint.
		/// </remarks>
		public void Clean()
		{
			foreach (var pair in this.innerMap)
			{
				if (pair.Value != null)
					continue;

				this.innerMap.RemoveKey(pair.Key);
			}
		}

		/// <summary>
		/// Returns whether the specified key is a valid key for this map.
		/// </summary>
		/// <param name="key">The key to test.</param>
		/// <returns><see langword="true"/> when <paramref name="key"/>
		/// would be a valid key for this map;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// This method does <em>not</em> check whether the key is valid
		/// for the current state of the map. For example, the
		/// instance would not return <see langword="false"/> just because the
		/// given key is already present.
		/// </remarks>
		[Pure]
		public virtual bool IsValidKey(TKey key)
		{
			return key is TKey;
		}

		/// <summary>
		/// Returns whether the specified value is a valid value for this map.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/>
		/// would be a valid value for this map;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public virtual bool IsValidValue(TValue value)
		{
			return value == null || value is TValue;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator()
		{
			return this.innerMap
				.Select(pair => Tuple.Create(pair.Key, pair.Value.Target))
				.Where(t => t.Item2 != null)
				.Select(t => (IAssociation<TKey, TValue>) Association.Create(t.Item1, (TValue) t.Item2))
				.ToM42Array()
				.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}
}
