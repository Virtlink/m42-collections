﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;

namespace M42.Collections
{
	partial class LinkedList<T>
	{
		/// <summary>
		/// A node in the linked list.
		/// </summary>
		public sealed class Node
		{
			/// <summary>
			/// The owner of the node.
			/// </summary>
			private readonly LinkedList<T> owner;

			private T value;
			/// <summary>
			/// Gets or sets the value of the linked list node.
			/// </summary>
			/// <value>The value.</value>
			public T Value
			{
				get { return this.value; }
				set { this.value = value; }
			}

			// When the previous node is the linked list's start dummy node,
			// the corresponding property returns null.
			// Use the field for the actual previous node.
			private Node previous;
			/// <summary>
			/// Gets the previous node.
			/// </summary>
			/// <value>The previous node;
			/// or <see langword="null"/> when there is none.</value>
			public Node Previous
			{
				get
				{
					if (this.previous == this.owner.head)
						return null;
					return this.previous;
				}
			}

			// When the next node is the linked list's end dummy node,
			// the corresponding property returns null.
			// Use the field for the actual next node.
			private Node next;
			/// <summary>
			/// Gets the next node.
			/// </summary>
			/// <value>The next node;
			/// or <see langword="null"/> when there is none.</value>
			public Node Next
			{
				get
				{
					if (this.next == this.owner.tail)
						return null;
					return this.next;
				}
			}

			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Node"/> class.
			/// </summary>
			/// <param name="owner">The owner of the node.</param>
			/// <param name="value">The value of the node.</param>
			internal Node(LinkedList<T> owner, T value)
			{
				#region Contract
				Contract.Requires<ArgumentNullException>(owner != null);
				#endregion
				this.owner = owner;
				this.value = value;
			}
			#endregion

			/// <summary>
			/// Connects two nodes.
			/// </summary>
			/// <param name="left">The left-hand node.</param>
			/// <param name="right">The right-hand node.</param>
			internal static void Connect(Node left, Node right)
			{
				// Disconnect 'left' from its next node.
				if (left != null)
				{
					var oldNext = left.next;
					left.next = null;
					if (oldNext != null)
						oldNext.previous = null;
				}
				// Disconnect 'right' from its previous node.
				if (right != null)
				{
					var oldPrevious = right.previous;
					right.previous = null;
					if (oldPrevious != null)
						oldPrevious.next = null;
				}
				if (left != null && right != null)
				{
					// Connect 'left' to 'right'
					left.next = right;
					right.previous = left;
				}
			}

			#region Equality
			/// <inheritdoc />
			public override int GetHashCode()
			{
				// We have no immutable fields, so
				// return the fixed Object.GetHashCode() instead.
				return base.GetHashCode();
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				// Compare objects by reference.
				return Object.ReferenceEquals(this, obj);
			}
			#endregion

			/// <inheritdoc />
			[Pure]
			public bool IsValidValue(object value)
			{
				return this.owner.IsValidMember(value);
			}

			/// <summary>
			/// Adds the specified value after this node in the linked list.
			/// </summary>
			/// <param name="value">The value to add.</param>
			/// <returns>The added <see cref="Node"/>.</returns>
			public Node AddAfter(T value)
			{
				#region Contract
				Contract.Requires<ArgumentException>(IsValidValue(value));
				#endregion
				var node = this.owner.CreateNode(value);
				var next = this.next;
				Connect(this, node);
				Connect(node, next);
				this.owner.count++;
				return node;
			}

			/// <summary>
			/// Adds the specified value after this node in the linked list.
			/// </summary>
			/// <param name="value">The value to add.</param>
			/// <returns>The added <see cref="Node"/>.</returns>
			public Node AddBefore(T value)
			{
				#region Contract
				Contract.Requires<ArgumentException>(IsValidValue(value));
				#endregion
				var node = this.owner.CreateNode(value);
				var previous = this.previous;
				Connect(node, this);
				Connect(previous, node);
				this.owner.count++;
				return node;
			}

			/// <summary>
			/// Removes this node from the linked list.
			/// </summary>
			public void Remove()
			{
				Connect(this.previous, this.next);
				this.owner.count--;
			}

			/// <inheritdoc />
			public override string ToString()
			{
				// CONTRACT: Inherited from Object
				return value != null ? value.ToString() : "(null)";
			}

			#region Invariants
			[ContractInvariantMethod]
			[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
				Justification = "Required for code contracts.")]
			private void ObjectInvariant()
			{
				Contract.Invariant(this.owner != null);
			}
			#endregion
		}
	}
}
