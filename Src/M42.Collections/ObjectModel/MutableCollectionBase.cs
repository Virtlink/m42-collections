﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// Base class for mutable collections.
	/// </summary>
	[ContractClass(typeof(Contracts.MutableCollectionBaseContract<>))]
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public abstract class MutableCollectionBase<T> : IMutableCollection<T>
	{
		/// <inheritdoc />
		public abstract int Count
		{ get; }

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.Count == 0;
			}
		}

		/// <inheritdoc />
		public virtual bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return false;
			}
		}

		#region Comparisons
		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the equality comparer used to compare elements in this list.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		/// <summary>
		/// Compares two values.
		/// </summary>
		/// <param name="expected">The expected value.</param>
		/// <param name="value">The value to compare to.</param>
		/// <returns><see langword="true"/> when the values are equal;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		protected bool AreEqual(object expected, T value)
		{
			return (expected == null || expected is T)
				&& this.Comparer.Equals((T)expected, value);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="MutableCollectionBase{T}"/> class.
		/// </summary>
		/// <param name="comparer">The equality comparer used to compare value
		/// in this collection.</param>
		protected MutableCollectionBase(SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.comparer = comparer;
		}
		#endregion

		#region Protected Abstract Members
		/// <summary>
		/// Removes all elements from the collection.
		/// </summary>
		protected abstract void ClearElements();

		/// <summary>
		/// Adds an element with the specified value to the collection.
		/// </summary>
		/// <param name="value">The value of the element to add.</param>
		/// <returns><see langword="true"/> when the element was added;
		/// otherwise, <see langword="false"/>.</returns>
		protected abstract bool AddElement(T value);

		/// <summary>
		/// Removes the element with the specified value from the collection.
		/// </summary>
		/// <param name="value">The value of the element to remove.</param>
		/// <returns><see langword="true"/> when the element was removed;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>
		/// Implementations should not throw exceptions. When the type of
		/// <paramref name="value"/> is not compatible or when
		/// <see cref="IsValidMember"/> would return <see langword="false"/>,
		/// then this method should return <see langword="false"/> instead
		/// of throwing an exception. This indicates that the requested element
		/// could not be found.
		/// </para>
		/// <para>
		/// The <see cref="Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// </para>
		/// </remarks>
		protected abstract bool RemoveElement(object value);

		/// <summary>
		/// Returns whether the collection contains an element that is
		/// considered to be equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns><see langword="true"/> when the collection contains an
		/// element that is considered to be equal to the specified value;
		/// or <see langword="false"/> when no such element
		/// could be found.</returns>
		/// <remarks>
		/// <para>
		/// Implementations should not throw exceptions. When the type of
		/// <paramref name="value"/> is not compatible or when
		/// <see cref="IsValidMember"/> would return <see langword="false"/>,
		/// then this method should return <see langword="false"/> instead
		/// of throwing an exception. This indicates that the requested element
		/// could not be found.
		/// </para>
		/// <para>
		/// The <see cref="Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// </para>
		/// </remarks>
		[Pure]
		protected virtual bool ContainsElement(object value)
		{
			T result;
			return TryGetMemberElement(value, out result);
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		protected abstract bool TryGetMemberElement(object value, out T result);
		#endregion

		/// <inheritdoc />
		public void Clear()
		{
			ClearElements();
		}

		/// <inheritdoc />
		public bool Add(T value)
		{
			// CONTRACT: Inherited from IMutableCollection<T>
			return AddElement(value);
		}

		/// <summary>
		/// Removes an element from the collection that is equal to the
		/// specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns><see langword="true"/> when an element equal to
		/// <paramref name="value"/> was found and removed; otherwise,
		/// <see langword="false"/> when such an element was either not found
		/// or could not be removed.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		public bool Remove(T value)
		{
			#region Contract
			Contract.Requires<CollectionFrozenException>(!IsFrozen);
			#endregion
			return ((IMutableCollection<T>)this).Remove(value);
		}

		/// <inheritdoc />
		bool IMutableCollection<T>.Remove(object value)
		{
			// CONTRACT: Inherited from IMutableCollection<T>
			return RemoveElement(value);
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetMember(object value, out T result)
		{
			return TryGetMemberElement(value, out result);
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			T result;
			isPresent = TryGetMemberElement(value, out result);
			return isPresent ? result : default(T);
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool present;
			return ((ICollection<T>)this).GetMember(value, out present);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <summary>
		/// Returns whether the collection contains an element that is equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when the collection contains an
		/// element that is equal to <paramref name="value"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>
		/// The <see cref="Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// To specify a custom comparer, use the appropriate
		/// <see cref="System.Linq.Enumerable.Contains"/> overload.
		/// </para>
		/// </remarks>
		[Pure]
		public bool Contains(T value)
		{
			return ((ICollection<T>)this).Contains(value);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return ContainsElement(value);
		}

		/// <inheritdoc />
		public virtual bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public abstract SCG.IEnumerator<T> GetEnumerator();

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetEnumerator();
		}

		#region SCG.ICollection<T>
#if false
		/// <inheritdoc />
		int SCG.ICollection<T>.Count
		{
			get { return this.Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.IsReadOnly
		{
			get { return this.IsFrozen; }
		}
		
		/// <inheritdoc />
		void SCG.ICollection<T>.Add(T item)
		{
			this.Add(item);
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Remove(T item)
		{
			return this.Remove(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Clear()
		{
			this.Clear();
		}

		/// <inheritdoc />
		[ContractVerification(false)]
		bool SCG.ICollection<T>.Contains(T item)
		{
			return this.Contains(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			#region Contract
			if (arrayIndex + this.Count > array.Length)
				throw new ArgumentOutOfRangeException("arrayIndex");
			#endregion
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}

		/// <inheritdoc />
		SCG.IEnumerator<T> SCG.IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}
#endif
		#endregion
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(MutableCollectionBase<>))]
		abstract class MutableCollectionBaseContract<T> : MutableCollectionBase<T>
		{
			public MutableCollectionBaseContract() : base(null) { }

			protected override void ClearElements()
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			protected override bool AddElement(T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			protected override bool RemoveElement(object value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			protected override bool ContainsElement(object value)
			{
				return default(bool);
			}
		}
	}
	#endregion
}
