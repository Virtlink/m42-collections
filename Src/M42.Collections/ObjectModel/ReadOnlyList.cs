﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A read-only list that wraps a mutable list.
	/// </summary>
	/// <typeparam name="T">The type of elements in the list.</typeparam>
	public class ReadOnlyList<T> : ReadOnlyCollection<T>, IList<T>	//, SCG.IList<T>
	{
		/// <summary>
		/// The list that has been wrapped.
		/// </summary>
		private readonly IList<T> wrappedList;

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this.wrappedList[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this.wrappedList[index];
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ReadOnlyList{T}"/> class.
		/// </summary>
		/// <param name="wrappedList">The list wrapped by this
		/// <see cref="ReadOnlyList{T}"/>.</param>
		public ReadOnlyList(IList<T> wrappedList)
			: base(wrappedList)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrappedList != null);
			#endregion
			this.wrappedList = wrappedList;
		}
		#endregion

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			return this.wrappedList.IndexOf(value);
		}

		#region System.Collections.Generic.IList<T>
#if false
		/// <inheritdoc />
		T SCG.IList<T>.this[int index]
		{
			get
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				#endregion
				return this[index];
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <inheritdoc />
		void SCG.IList<T>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<T>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		int SCG.IList<T>.IndexOf(T item)
		{
			int? index = this.IndexOf(item);
			return index ?? -1;
		}
#endif
		#endregion

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrappedList != null);
		}
		#endregion
	}
}
