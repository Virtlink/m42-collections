﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A queue that wraps an enumerable.
	/// </summary>
	public sealed class QueueFromEnumerator<T> : IMutableSequence<T>, IDisposable
	{
		/// <summary>
		/// The enumerator.
		/// </summary>
		private readonly IEnumerator<T> enumerator;

		private bool isEmpty = false;
		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return this.isEmpty; }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="QueueFromEnumerator{T}"/> class.
		/// </summary>
		/// <param name="enumerator">The enumerator.</param>
		public QueueFromEnumerator(IEnumerator<T> enumerator)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(enumerator != null);
			#endregion
			this.enumerator = enumerator;

			// Position the enumerator at the first element.
			MoveNext();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="QueueFromEnumerator{T}"/> class.
		/// </summary>
		/// <param name="enumerable">The enumerable.</param>
		public QueueFromEnumerator(IEnumerable<T> enumerable)
			: this(enumerable.GetEnumerator())
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(enumerable != null);
			#endregion
		}
		#endregion

		#region Disposing
		/// <summary>
		/// Whether this object has been disposed.
		/// </summary>
		private bool isDisposed = false;

		/// <summary>
		/// Asserts that this object has not been disposed.
		/// </summary>
		private void AssertNotDisposed()
		{
			if (this.isDisposed)
				throw new ObjectDisposedException(this.GetType().FullName);
		}

		/// <inheritdoc />
		public void Dispose()
		{
			if (!this.isDisposed)
			{
				this.isEmpty = true;
				this.enumerator.Dispose();
			}
			this.isDisposed = true;
		}
		#endregion

		/// <inheritdoc />
		bool IMutableSequence<T>.Add(T value)
		{
			// CONTRACT: Inherited from IMutableSequence<T>.
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public T Take()
		{
			// CONTRACT: Inherited from IMutableSequence<T>.
			AssertNotDisposed();

			var value = this.enumerator.Current;
			MoveNext();
			return value;
		}

		/// <inheritdoc />
		public T Peek()
		{
			// CONTRACT: Inherited from IMutableSequence<T>.
			AssertNotDisposed();

			return this.enumerator.Current;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from IMutableSequence<T>.
			return value == null
				|| value is T;
		}

		/// <summary>
		/// Moves to the next element.
		/// </summary>
		private void MoveNext()
		{
			this.isEmpty = !this.enumerator.MoveNext();
			if (this.isEmpty)
				this.enumerator.Dispose();
		}
	}
}
