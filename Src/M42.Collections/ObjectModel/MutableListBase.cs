﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// Base class for mutable lists.
	/// </summary>
	[ContractClass(typeof(Contracts.MutableListBaseContract<>))]
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public abstract class MutableListBase<T> : MutableCollectionBase<T>, IMutableList<T>
	{
		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IMutableList<T>
				return GetElement(index);
			}
			set
			{
				// CONTRACT: Inherited from IMutableList<T>
				SetElement(index, value);
			}
		}

		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="MutableListBase{T}"/> class.
		/// </summary>
		/// <param name="comparer">The equality comparer used to compare value
		/// in this list.</param>
		protected MutableListBase(SCG.IEqualityComparer<T> comparer)
			: base(comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}
		#endregion

		#region Protected Abstract Members
		/// <inheritdoc />
		protected override sealed bool AddElement(T value)
		{
			return Insert(this.Count, value);
		}

		/// <inheritdoc />
		protected override sealed bool RemoveElement(object value)
		{
			int? index = IndexOfElement(value);
			if (index == null)
				return false;
			return RemoveAt((int)index);
		}

		/// <inheritdoc />
		protected override sealed bool ContainsElement(object value)
		{
			return IndexOfElement(value) != null;
		}

		/// <inheritdoc />
		protected override sealed bool TryGetMemberElement(object value, out T result)
		{
			int? index = IndexOfElement(value);
			if (index == null)
			{
				result = default(T);
				return false;
			}
			else
			{
				result = GetElement((int)index);
				return true;
			}
		}

		/// <summary>
		/// Removes all elements from the collection.
		/// </summary>
		protected override abstract void ClearElements();

		/// <summary>
		/// Gets the value of the element with the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element whose
		/// value to get.</param>
		/// <returns>The value of the element
		/// with the specified index.</returns>
		[Pure]
		protected abstract T GetElement(int index);

		/// <summary>
		/// Sets the value of the element with the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to
		/// set.</param>
		/// <param name="value">The new value of the element.</param>
		protected abstract void SetElement(int index, T value);

		/// <summary>
		/// Inserts an element with the specified value at the specified index
		/// in the list.
		/// </summary>
		/// <param name="index">The zero-based index at which to insert the
		/// element.</param>
		/// <param name="value">The value of the element to insert into the
		/// list.</param>
		/// <returns><see langword="true"/> when the element was inserted;
		/// otherwise, <see langword="false"/>.</returns>
		protected abstract bool InsertElement(int index, T value);

		/// <summary>
		/// Removes the element at the specified index from the list.
		/// </summary>
		/// <param name="index">The zero-based index of the element to
		/// remove.</param>
		/// <returns><see langword="true"/> when the element was removed;
		/// otherwise, <see langword="false"/>.</returns>
		protected abstract bool RemoveElement(int index);

		/// <summary>
		/// Returns the index in the collection of an element that is
		/// considered to be equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The zero-based index of the first element that is
		/// considered to be equal to the specified value;
		/// or <see langword="null"/> when no such element
		/// could be found.</returns>
		/// <remarks>
		/// <para>
		/// Multiple calls to this method on a list that has not
		/// changed in between calls will return the same index.
		/// </para>
		/// <para>
		/// Implementations should not throw exceptions. When the type of
		/// <paramref name="value"/> is not compatible or when
		/// <see cref="ICollection.IsValidMember"/> would return <see langword="false"/>,
		/// then this method should return <see langword="null"/> instead
		/// of throwing an exception. This indicates that the requested element
		/// could not be found.
		/// </para>
		/// <para>
		/// The <see cref="MutableCollectionBase{T}.Comparer"/> property returns the equality
		/// comparer that is used to compare the elements.
		/// </para>
		/// </remarks>
		[Pure]
		protected abstract int? IndexOfElement(object value);
		#endregion

		/// <summary>
		/// Adds multiple elements to the list.
		/// </summary>
		/// <param name="elements">A sequence of elements.</param>
		/// <returns>The number of elements that were successfully
		/// added to the list.</returns>
		public int AddRange(SCG.IEnumerable<T> elements)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Ensures(Contract.Result<int>() >= 0);
			#endregion
			int addedCount = 0;
			foreach (var element in elements)
			{
				bool success = Add(element);
				if (success)
					addedCount++;
			}
			return addedCount;
		}

		/// <inheritdoc />
		public bool Insert(int index, T value)
		{
			return InsertElement(index, value);
		}

		/// <summary>
		/// Inserts multiple elements into the list.
		/// </summary>
		/// <param name="index">The zero-based index at which to insert the elements.</param>
		/// <param name="elements">A sequence of elements.</param>
		/// <returns>The number of elements that were successfully
		/// added to the list.</returns>
		public int InsertRange(int index, SCG.IEnumerable<T> elements)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Ensures(Contract.Result<int>() >= 0);
			#endregion
			int insertedCount = 0;
			foreach (var element in elements)
			{
				bool success = Insert(index, element);
				if (success)
				{
					index++;
					insertedCount++;
				}
			}
			return insertedCount;
		}

		/// <inheritdoc />
		public bool RemoveAt(int index)
		{
			return RemoveElement(index);
		}

		/// <summary>
		/// Returns the index in the collection of an element that is
		/// considered to be equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The zero-based index of the first element that is
		/// considered to be equal to the specified value;
		/// or <see langword="null"/> when no such element
		/// could be found.</returns>
		/// <remarks>
		/// <para>
		/// Multiple calls to this method on a list that has not
		/// changed in between calls will return the same index.
		/// </para>
		/// <para>
		/// The implementation determines the equality
		/// comparer that is used to compare the elements.
		/// </para>
		/// </remarks>
		[Pure]
		public int? IndexOf(T value)
		{
			return IndexOfElement((object)value);
		}

		/// <inheritdoc />
		int? IList.IndexOf(object value)
		{
			return this.IndexOfElement(value);
		}

		#region SCG.IList<T>
#if false
		/// <inheritdoc />
		T SCG.IList<T>.this[int index]
		{
			get
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				#endregion
				return this[index];
			}
			set
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				if (!IsValidMember(value))
					throw new ArgumentException("The value is not valid.", "value");
				#endregion
				this[index] = value;
			}
		}
		
		/// <inheritdoc />
		void SCG.IList<T>.Insert(int index, T item)
		{
			this.Insert(index, item);
		}

		/// <inheritdoc />
		void SCG.IList<T>.RemoveAt(int index)
		{
			this.RemoveAt(index);
		}
		
		/// <inheritdoc />
		int SCG.IList<T>.IndexOf(T item)
		{
			int? index = this.IndexOf(item);
			return index ?? -1;
		}
#endif
		#endregion
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(MutableListBase<>))]
		abstract class MutableListBaseContract<T> : MutableListBase<T>
		{
			public MutableListBaseContract() : base(null) { }

			protected override T GetElement(int index)
			{
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index < Count);
				return default(T);
			}

			protected override void SetElement(int index, T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index < Count);
			}

			protected override bool InsertElement(int index, T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index <= Count);
				return default(bool);
			}

			protected override bool RemoveElement(int index)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index < Count);
				return default(bool);
			}

			//protected override IList<T> GetFrozenList()
			//{
			//	Contract.Ensures(Contract.Result<IList<T>>() != null);
			//	Contract.Ensures(Contract.Result<IList<T>>().IsFrozen);
			//	return default(IList<T>);
			//}
		}
	}
	#endregion
}
