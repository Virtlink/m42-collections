﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A collection that owns the objects put in it.
	/// </summary>
	/// <typeparam name="T">The type of objects put in the list.</typeparam>
	/// <typeparam name="TOwner">The type of the owner
	/// of the objects.</typeparam>
	public abstract class PossessionCollection<T, TOwner> : MutableCollectionBase<T>
		where TOwner : class
	{
		/// <summary>
		/// The wrapped collection.
		/// </summary>
		private readonly IMutableCollection<T> wrappedCollection;

		/// <inheritdoc />
		public override int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.wrappedCollection.Count;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="PossessionCollection{T, TOwner}"/> class.
		/// </summary>
		/// <param name="owner">The owner of the collection.</param>
		protected PossessionCollection(TOwner owner)
			: this(owner, new ArrayList<T>())
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(owner != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PossessionCollection{T, TOwner}"/> class.
		/// </summary>
		/// <param name="owner">The owner of the collection.</param>
		/// <param name="wrappedCollection">The collection to wrap.</param>
		protected PossessionCollection(TOwner owner, IMutableCollection<T> wrappedCollection)
			: base(EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(owner != null);
			Contract.Requires<ArgumentNullException>(wrappedCollection != null);
			#endregion
			this.owner = owner;
			this.wrappedCollection = wrappedCollection;
		}
		#endregion

		/// <inheritdoc />
		protected override bool AddElement(T value)
		{
			// CONTRACT: Inherited from MutableCollectionBase<T>
			if (this.wrappedCollection.Add(value))
			{
				Contract.Assume(!Object.ReferenceEquals(value, null)); 
				Own(value);
				return true;
			}
			else
				return false;
		}

		/// <inheritdoc />
		protected override bool RemoveElement(object value)
		{
			// CONTRACT: Inherited from MutableCollectionBase<T>
			bool isPresent;
			var member = this.wrappedCollection.GetMember(value, out isPresent);
			if (isPresent && this.wrappedCollection.Remove(value))
			{
				Contract.Assume(!Object.ReferenceEquals(member, null));
				Disown(member);
				return true;
			}
			else
				return false;
		}

		/// <inheritdoc />
		protected override void ClearElements()
		{
			// CONTRACT: Inherited from ArrayList<T>
			foreach (var item in this)
			{
				Contract.Assume(!Object.ReferenceEquals(item, null));
				Disown(item);
			}
			this.wrappedCollection.Clear();
		}

		/// <inheritdoc />
		protected override bool ContainsElement(object value)
		{
			// CONTRACT: Inherited from MutableCollectionBase<T>
			return this.wrappedCollection.Contains(value);
		}

		/// <inheritdoc />
		public override IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from MutableCollectionBase<T>
			return this.wrappedCollection.GetEnumerator();
		}

		/// <inheritdoc />
		protected override bool TryGetMemberElement(object value, out T result)
		{
			bool isPresent;
			result = this.wrappedCollection.GetMember(value, out isPresent);
			return isPresent;
		}

		/// <inheritdoc />
		public override bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.wrappedCollection.IsValidMember(value)
				&& value != null;
		}

		#region Owning
		private TOwner owner;
		/// <summary>
		/// Gets the owner of this collection.
		/// </summary>
		/// <value>The owner of the collection.</value>
		protected TOwner Owner
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<TOwner>() != null);
				#endregion
				return this.owner;
			}
		}

		/// <summary>
		/// Sets the owner of the specified item to be this collection's owner.
		/// </summary>
		/// <param name="item">The item to set the owner of.</param>
		private void Own(T item)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(item != null);
			#endregion
			var currentOwnerCollection = GetOwnerCollection(item);
			if (currentOwnerCollection != null && currentOwnerCollection != this)
				throw new InvalidOperationException("The object is already owned by another collection.");
				//currentOwnerCollection.Remove(item);
			SetOwner(item, this.owner);
		}

		/// <summary>
		/// Disowns the specified item.
		/// </summary>
		/// <param name="item">The item to be disowned.</param>
		private void Disown(T item)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(item != null);
			#endregion
			var currentOwnerCollection = GetOwnerCollection(item);
			if (currentOwnerCollection == this)
				SetOwner(item, null);
		}

		/// <summary>
		/// Gets the collection of the current owner of the item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>The current owner's <see cref="PossessionCollection{T, TOwner}"/>;
		/// or <see langword="null"/> when the item has no owner.</returns>
		protected abstract PossessionCollection<T, TOwner> GetOwnerCollection(T item);

		/// <summary>
		/// Sets the owner of the specified item to <see cref="Owner"/>.
		/// </summary>
		/// <param name="item">The item to set the owner of.</param>
		/// <param name="owner">The new owner; or <see langword="null"/> to clear the owner.</param>
		protected abstract void SetOwner(T item, TOwner owner);
		#endregion

		#region Invariant
		/// <summary>
		/// Asserts the invariants of this type.
		/// </summary>
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrappedCollection != null);
			Contract.Invariant(this.owner != null);
		}
		#endregion
	}
}
