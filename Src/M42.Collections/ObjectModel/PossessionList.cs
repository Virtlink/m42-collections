﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A list that owns the objects put in it.
	/// </summary>
	/// <typeparam name="T">The type of objects put in the list.</typeparam>
	/// <typeparam name="TOwner">The type of the owner
	/// of the objects.</typeparam>
	public abstract class PossessionList<T, TOwner> : MutableListBase<T>
		where TOwner : class
	{
		/// <summary>
		/// The wrapped list.
		/// </summary>
		private readonly IMutableList<T> wrappedList;

		/// <inheritdoc />
		public override int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.wrappedList.Count;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="PossessionList{T, TOwner}"/> class.
		/// </summary>
		/// <param name="owner">The owner of the collection.</param>
		protected PossessionList(TOwner owner)
			: this(owner, new ArrayList<T>())
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(owner != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PossessionList{T, TOwner}"/> class.
		/// </summary>
		/// <param name="owner">The owner of the collection.</param>
		/// <param name="wrappedList">The list to wrap.</param>
		protected PossessionList(TOwner owner, IMutableList<T> wrappedList)
			: base(EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(owner != null);
			Contract.Requires<ArgumentNullException>(wrappedList != null);
			#endregion
			this.owner = owner;
			this.wrappedList = wrappedList;
		}
		#endregion

		/// <inheritdoc />
		protected override bool InsertElement(int index, T value)
		{
			// CONTRACT: Inherited from MutableListBase<T>
			if (this.wrappedList.Insert(index, value))
			{
				Contract.Assume(!Object.ReferenceEquals(value, null));
				Own(value);
				return true;
			}
			else
				return false;
		}

		/// <inheritdoc />
		protected override bool RemoveElement(int index)
		{
			// CONTRACT: Inherited from MutableListBase<T>
			var value = this[index];
			if (this.wrappedList.RemoveAt(index))
			{
				Contract.Assume(!Object.ReferenceEquals(value, null));
				Disown(value);
				return true;
			}
			else
				return false;
		}

		/// <inheritdoc />
		protected override void SetElement(int index, T value)
		{
			// CONTRACT: Inherited from MutableListBase<T>
			var oldValue = this[index];
			Contract.Assume(!Object.ReferenceEquals(oldValue, null));
			Disown(oldValue);
			Contract.Assume(!Object.ReferenceEquals(value, null));
			Own(value);

			this.wrappedList[index] = value;
		}

		/// <inheritdoc />
		protected override void ClearElements()
		{
			// CONTRACT: Inherited from MutableListBase<T>
			foreach (var item in this)
			{
				Contract.Assume(!Object.ReferenceEquals(item, null));
				Disown(item);
			}
			this.wrappedList.Clear();
		}

		/// <inheritdoc />
		protected override T GetElement(int index)
		{
			return this.wrappedList[index];
		}

		/// <inheritdoc />
		protected override int? IndexOfElement(object value)
		{
			return this.wrappedList.IndexOf(value);
		}

		/// <inheritdoc />
		public override IEnumerator<T> GetEnumerator()
		{
			return this.wrappedList.GetEnumerator();
		}

		/// <inheritdoc />
		public override bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.wrappedList.IsValidMember(value)
				&& value != null;
		}

		#region Owning
		private TOwner owner;
		/// <summary>
		/// Gets the owner of this list.
		/// </summary>
		/// <value>The owner of the list.</value>
		protected TOwner Owner
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<TOwner>() != null);
				#endregion
				return this.owner;
			}
		}

		/// <summary>
		/// Sets the owner of the specified item to be this collection's owner.
		/// </summary>
		/// <param name="item">The item to set the owner of.</param>
		private void Own(T item)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(item != null);
			#endregion
			var currentOwnerCollection = GetOwnerCollection(item);
			if (currentOwnerCollection != null && currentOwnerCollection != this)
				throw new InvalidOperationException("The object is already owned by another list.");
				//currentOwnerCollection.Remove(item);
			SetOwner(item, this.owner);
		}

		/// <summary>
		/// Disowns the specified item.
		/// </summary>
		/// <param name="item">The item to be disowned.</param>
		private void Disown(T item)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(item != null);
			#endregion
			var currentOwnerCollection = GetOwnerCollection(item);
			if (currentOwnerCollection == this)
				SetOwner(item, null);
		}

		/// <summary>
		/// Gets the collection of the current owner of the item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>The current owner's <see cref="PossessionList{T, TOwner}"/>;
		/// or <see langword="null"/> when the item has no owner.</returns>
		protected abstract PossessionList<T, TOwner> GetOwnerCollection(T item);

		/// <summary>
		/// Sets the owner of the specified item to <see cref="Owner"/>.
		/// </summary>
		/// <param name="item">The item to set the owner of.</param>
		/// <param name="owner">The new owner; or <see langword="null"/> to clear the owner.</param>
		protected abstract void SetOwner(T item, TOwner owner);
		#endregion

		#region Invariant
		/// <summary>
		/// Asserts the invariants of this type.
		/// </summary>
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrappedList != null);
			Contract.Invariant(this.owner != null);
		}
		#endregion
	}
}
