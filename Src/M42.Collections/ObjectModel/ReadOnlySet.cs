﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A read-only set that wraps a mutable set.
	/// </summary>
	/// <typeparam name="T">The type of elements in the set.</typeparam>
	public class ReadOnlySet<T> : ReadOnlyCollection<T>, ISet<T>
	{
		/// <summary>
		/// The set that has been wrapped.
		/// </summary>
		private readonly ISet<T> wrappedSet;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="ReadOnlySet{T}"/> class.
		/// </summary>
		/// <param name="wrappedSet">The collection wrapped by this
		/// <see cref="ReadOnlySet{T}"/>.</param>
		public ReadOnlySet(ISet<T> wrappedSet)
			: base(wrappedSet)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrappedSet != null);
			#endregion
			this.wrappedSet = wrappedSet;
		}
		#endregion

		/// <inheritdoc />
		public bool Overlaps(SC.IEnumerable other)
		{
			return this.wrappedSet.Overlaps(other);
		}

		/// <inheritdoc />
		public bool SetEquals(SC.IEnumerable other)
		{
			return this.wrappedSet.SetEquals(other);
		}

		/// <inheritdoc />
		public bool IsSubsetOf(SC.IEnumerable other)
		{
			return this.wrappedSet.IsSubsetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSubsetOf(SC.IEnumerable other)
		{
			return this.wrappedSet.IsProperSubsetOf(other);
		}

		/// <inheritdoc />
		public bool IsSupersetOf(SC.IEnumerable other)
		{
			return this.wrappedSet.IsSupersetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSupersetOf(SC.IEnumerable other)
		{
			return this.wrappedSet.IsProperSupersetOf(other);
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrappedSet != null);
		}
		#endregion
	}
}
