﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A read-only collection that wraps a mutable collection.
	/// </summary>
	/// <typeparam name="T">The type of elements in the collection.</typeparam>
	public class ReadOnlyCollection<T> : ICollection<T>	//, SCG.ICollection<T>
	{
		/// <summary>
		/// The collection that has been wrapped.
		/// </summary>
		private readonly ICollection<T> wrappedCollection;

		/// <inheritdoc />
		public int Count
		{
			get { return this.wrappedCollection.Count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return this.wrappedCollection.IsEmpty; }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return this.wrappedCollection.IsFrozen; }
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="ReadOnlyCollection{T}"/> class.
		/// </summary>
		/// <param name="wrappedCollection">The collection wrapped by this
		/// <see cref="ReadOnlyCollection{T}"/>.</param>
		public ReadOnlyCollection(ICollection<T> wrappedCollection)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(wrappedCollection != null);
			#endregion
			this.wrappedCollection = wrappedCollection;
		}
		#endregion

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetMember(object value, out T result)
		{
			bool isPresent;
			result = this.wrappedCollection.GetMember(value, out isPresent);
			return isPresent;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.wrappedCollection.GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.wrappedCollection.GetMember(value);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return this.wrappedCollection.GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return this.wrappedCollection.GetMember(value);
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			return this.wrappedCollection.Contains(value);
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			return this.wrappedCollection.IsValidMember(value);
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			return this.wrappedCollection.GetEnumerator();
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return ((System.Collections.IEnumerable)this.wrappedCollection).GetEnumerator();
		}

		#region System.Collections.Generic.ICollection<T>
#if false
		/// <inheritdoc />
		int SCG.ICollection<T>.Count
		{
			get { return this.Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.IsReadOnly
		{
			get { return this.IsFrozen; }
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Clear()
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		[ContractVerification(false)]
		bool SCG.ICollection<T>.Contains(T item)
		{
			return this.Contains(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}
#endif
		#endregion

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.wrappedCollection != null);
		}
		#endregion
	}
}
