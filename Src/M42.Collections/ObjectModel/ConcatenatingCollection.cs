﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// Concatenates two or more collections into one.
	/// </summary>
	/// <typeparam name="T">The type of elements.</typeparam>
	public sealed class ConcatenatingCollection<T> : ICollection<T>
	{
		/// <summary>
		/// The concatenated collections.
		/// </summary>
		private readonly Array<ICollection<T>> collections;

		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return collections.Sum(c => c.Count);
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return Count == 0;
			}
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return collections.All(c => c.IsFrozen);
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatenatingCollection{T}"/> class.
		/// </summary>
		/// <param name="collections">The collections that are concatenated.</param>
		public ConcatenatingCollection(params ICollection<T>[] collections)
			: this((SCG.IEnumerable<ICollection<T>>)collections)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(collections != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatenatingCollection{T}"/> class.
		/// </summary>
		/// <param name="collections">The collections that are concatenated.</param>
		public ConcatenatingCollection(SCG.IEnumerable<ICollection<T>> collections)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(collections != null);
			#endregion
			this.collections = collections.ToM42Array();
		}
		#endregion

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.collections.Any(c => c.Contains(value));
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.collections.Any(c => c.IsValidMember(value));
		}

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			T result;
			foreach (var collection in this.collections)
			{
				result = collection.GetMember(value, out isPresent);
				if (isPresent)
					return result;
			}
			isPresent = false;
			return default(T);
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.collections
				.SelectMany(c => c)
				.GetEnumerator();
		}

		#region Explicit Methods
		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetMember(value);
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetEnumerator();
		}
		#endregion
	}
}
