﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;
using SL = System.Linq;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// A fixed-size list.
	/// </summary>
	/// <typeparam name="T">The type of elements in the list.</typeparam>
	/// <remarks>
	/// Use the <see cref="Array{T}"/> class for fast working with arrays.
	/// Use the <see cref="FixedSizeList{T}"/> class for a fixed-size
	/// collection whose getter and setter behavior can be overridden.
	/// </remarks>
	public class FixedSizeList<T> : IMutableArray<T>, IFreezable	//, SCG.IList<T>
	{
		/// <summary>
		/// The underlying array.
		/// </summary>
		private readonly Array<T> innerArray;

		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return innerArray.Count;
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return Count == 0;
			}
		}

		private bool isFrozen;
		/// <inheritdoc />
		/// <remarks>
		/// Empty arrays are implicitly frozen, as they can never be altered.
		/// </remarks>
		public bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from
				return this.isFrozen;
			}
		}

		#region Comparisons
		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the equality comparer used to compare elements in this list.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		/// <summary>
		/// Compares two values.
		/// </summary>
		/// <param name="expected">The expected value.</param>
		/// <param name="value">The value to compare to.</param>
		/// <returns><see langword="true"/> when the values are equal;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		protected bool AreEqual(object expected, T value)
		{
			return (expected == null || expected is T)
				&& this.Comparer.Equals((T)expected, value);
		}
		#endregion

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IMutableArray<T>
				return GetElement(index);
			}
			set
			{
				// CONTRACT: Inherited from IMutableArray<T>
				SetElement(index, value);
			}
		}

		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="values">The values in the array.</param>
		/// <param name="comparer">The comparer used to compare elements
		/// to values.</param>
		public FixedSizeList(SCG.IEnumerable<T> values, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(values != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.innerArray = values.ToM42Array();
			this.comparer = comparer;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="values">The values in the array.</param>
		public FixedSizeList(SCG.IEnumerable<T> values)
			: this(values, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(values != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="defaultValue">The value for each element.</param>
		public FixedSizeList(int length, T defaultValue)
			: this(length, SCG.EqualityComparer<T>.Default, defaultValue)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="initializer">Function that provides a value for each
		/// element of the array.</param>
		public FixedSizeList(int length, Func<int, T> initializer)
			: this(length, SCG.EqualityComparer<T>.Default, initializer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			Contract.Requires<ArgumentNullException>(initializer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="comparer">The comparer used to compare elements
		/// to values.</param>
		/// <param name="defaultValue">The value for each element.</param>
		public FixedSizeList(int length, SCG.IEqualityComparer<T> comparer, T defaultValue)
			: this(length, comparer, i => defaultValue)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="comparer">The comparer used to compare elements
		/// to values.</param>
		/// <param name="initializer">Function that provides a value for each
		/// element of the array.</param>
		public FixedSizeList(int length, SCG.IEqualityComparer<T> comparer, Func<int, T> initializer)
			: this(length, comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Requires<ArgumentNullException>(initializer != null);
			#endregion
			for (int i = 0; i < length; i++)
			{
				this.innerArray[i] = initializer(i);
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		public FixedSizeList(int length)
			: this(length, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FixedSizeList{T}"/> class.
		/// </summary>
		/// <param name="length">The length of the array.</param>
		/// <param name="comparer">The comparer used to compare elements
		/// to values.</param>
		public FixedSizeList(int length, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(length >= 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.comparer = comparer;
			this.innerArray = new Array<T>(length);
		}
		#endregion

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			for (int i = 0; i < this.innerArray.Count; i++)
			{
				if (AreEqual(value, this.innerArray[i]))
					return i;
			}
			return null;
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetMember(object value, out T result)
		{
			bool isPresent;
			result = ((ICollection<T>)this).GetMember(value, out isPresent);
			return isPresent;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			var index = ((IList<T>)this).IndexOf(value);
			isPresent = (index != null);
			T result = default(T);
			if (isPresent)
				result = this[(int)index];
			return result;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return IndexOf(value) != null;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public void Freeze()
		{
			this.isFrozen = true;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			return SL.Enumerable.Cast<T>(this.innerArray).GetEnumerator();
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#region Protected Virtual Members
		/// <summary>
		/// Gets the value of the element with the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element whose
		/// value to get.</param>
		/// <returns>The value of the element
		/// with the specified index.</returns>
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		protected virtual T GetElement(int index)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(index < Count);
			#endregion
			return this.innerArray[index];
		}

		/// <summary>
		/// Sets the value of the element with the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to
		/// set.</param>
		/// <param name="value">The new value of the element.</param>
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		protected virtual void SetElement(int index, T value)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(index < Count);
			#endregion
			this.innerArray[index] = value;
		}
		#endregion

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.innerArray != null);
		}
		#endregion

		#region SCG.IList
#if false
		/// <inheritdoc />
		int SCG.ICollection<T>.Count
		{
			get { return this.Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.IsReadOnly
		{
			get { return this.IsFrozen; }
		}

		/// <inheritdoc />
		T SCG.IList<T>.this[int index]
		{
			get
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				#endregion
				return this[index];
			}
			set
			{
				#region Contract
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException("index");
				if (!IsValidMember(value))
					throw new ArgumentException("The value is not valid.", "value");
				#endregion
				this[index] = value;
			}
		}

		/// <inheritdoc />
		int SCG.IList<T>.IndexOf(T item)
		{
			int? index = this.IndexOf(item);
			return index ?? -1;
		}

		/// <inheritdoc />
		void SCG.IList<T>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.IList<T>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Clear()
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		[ContractVerification(false)]
		bool SCG.ICollection<T>.Contains(T item)
		{
			return this.Contains(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		SCG.IEnumerator<T> SCG.IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}
#endif
		#endregion
	}
}
