﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections.ObjectModel
{
	/// <summary>
	/// Concatenates two or more lists into one.
	/// </summary>
	/// <typeparam name="T">The type of elements.</typeparam>
	public sealed class ConcatenatingList<T> : IList<T>
	{
		/// <summary>
		/// The concatenated collections.
		/// </summary>
		private readonly Array<IList<T>> lists;

		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return lists.Sum(c => c.Count);
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return Count == 0;
			}
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return lists.All(c => c.IsFrozen);
			}
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				int i = 0;
				while (index >= this.lists[i].Count && i < this.lists.Count)
				{
					i++;
					index -= this.lists[i].Count;
				}
				if (i == this.lists.Count)
					throw new ArgumentOutOfRangeException("index");
				return this.lists[i][index];
			}
		}

		#region Explicit Properties
		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this[index];
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatenatingList{T}"/> class.
		/// </summary>
		/// <param name="lists">The lists that are concatenated.</param>
		public ConcatenatingList(params IList<T>[] lists)
			: this((SCG.IEnumerable<IList<T>>)lists)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(lists != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatenatingList{T}"/> class.
		/// </summary>
		/// <param name="lists">The lists that are concatenated.</param>
		public ConcatenatingList(SCG.IEnumerable<IList<T>> lists)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(lists != null);
			#endregion
			this.lists = lists.ToM42Array();
		}
		#endregion

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			int? index;
			int baseIndex = 0;
			for (int i = 0; i < this.lists.Count; i++)
			{
				index = this.lists[i].IndexOf(value);
				if (index != null)
					return baseIndex + (int)index;
				baseIndex += this.lists[i].Count;
			}
			return null;
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.lists.Any(c => c.Contains(value));
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.lists.Any(c => c.IsValidMember(value));
		}

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			T result;
			foreach (var collection in this.lists)
			{
				result = collection.GetMember(value, out isPresent);
				if (isPresent)
					return result;
			}
			isPresent = false;
			return default(T);
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return this.lists
				.SelectMany(c => c)
				.GetEnumerator();
		}

		#region Explicit Methods
		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetMember(value);
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from ICollection<T>
			return GetEnumerator();
		}
		#endregion
	}
}
