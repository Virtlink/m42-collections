﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	partial class HashMap<TKey, TValue>
	{
		/// <summary>
		/// Comparer used to compare <see cref="Association{TKey, TValue}"/> objects.
		/// </summary>
		private sealed class AssociationComparer : SCG.IEqualityComparer<Association<TKey, TValue>>
		{
			private readonly SCG.IEqualityComparer<TKey> keyComparer;
			/// <summary>
			/// Gets the comparer used to compare keys.
			/// </summary>
			/// <value>An <see cref="SCG.IEqualityComparer{TKey}"/>.</value>
			public SCG.IEqualityComparer<TKey> KeyComparer
			{
				get
				{
					#region Contract
					Contract.Ensures(Contract.Result<SCG.IEqualityComparer<TKey>>() != null);
					#endregion
					return this.keyComparer;
				}
			}

			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="AssociationComparer"/> class.
			/// </summary>
			/// <param name="keyComparer">The key comparer.</param>
			public AssociationComparer(SCG.IEqualityComparer<TKey> keyComparer)
			{
				#region Contract
				Contract.Requires<ArgumentNullException>(keyComparer != null);
				#endregion
				this.keyComparer = keyComparer;
			}
			#endregion

			/// <inheritdoc />
			public bool Equals(Association<TKey, TValue> x, Association<TKey, TValue> y)
			{
				return keyComparer.Equals(x.Key, y.Key);
			}

			/// <inheritdoc />
			public int GetHashCode(Association<TKey, TValue> obj)
			{
				return keyComparer.GetHashCode(obj.Key);
			}

			#region Invariants
			[ContractInvariantMethod]
			[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
				Justification = "Required for code contracts.")]
			private void ObjectInvariant()
			{
				Contract.Invariant(this.keyComparer != null);
			}
			#endregion
		}
	}
}
