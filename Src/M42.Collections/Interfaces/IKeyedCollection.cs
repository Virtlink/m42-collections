﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A collection whose elements can be retrieved by key.
	/// </summary>
	/// <typeparam name="TKey">The type of the keys in the collection.</typeparam>
	/// <typeparam name="TValue">The type of the elements in the collection.</typeparam>
	[ContractClass(typeof(Contracts.IKeyedCollectionContract<,>))]
	public interface IKeyedCollection<out TKey, out TValue>
		: ICollection<IAssociation<TKey, TValue>>
	{
		/// <summary>
		/// Gets a collection of keys in this keyed collection.
		/// </summary>
		/// <value>A collection of keys.</value>
		ICollection<TKey> Keys { get; }

		/// <summary>
		/// Gets a collection of values in this keyed collection.
		/// </summary>
		/// <value>A collection of values.</value>
		ICollection<TValue> Values { get; }

		/// <summary>
		/// Returns whether the collection contains one or more elements
		/// with a key that is considered to be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <returns><see langword="true"/> when the collection contains the
		/// specified key; otherwise, <see langword="false"/>.</returns>
		[Pure]
		bool ContainsKey(object key);

		/// <summary>
		/// Gets a value from the keyed collection that has a key that is
		/// considered to be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <param name="isPresent">Returns <see langword="true"/> when
		/// a key equal to <paramref name="key"/> was present in the
		/// collection and its value was returned;
		/// otherwise, <see langword="false"/> when
		/// no such key was present and the returned value is the default
		/// of <typeparamref name="TValue"/>.</param>
		/// <returns>The value associated with <paramref name="key"/>;
		/// or the default of <typeparamref name="TValue"/> when no such value
		/// was found.</returns>
		TValue GetValue(object key, out bool isPresent);

		/// <summary>
		/// Gets a value from the keyed collection that has a key that is
		/// considered to be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <returns>The value associated with <paramref name="key"/>;
		/// or the default of <typeparamref name="TValue"/> when no such value
		/// was found.</returns>
		/// <remarks>
		/// When <typeparamref name="TValue"/> is a value type, or a reference type
		/// which may be <see langword="null"/>, then this overload does not
		/// show the difference between a value of a key that is present in the
		/// collection but equal to the default of <typeparamref name="TValue"/>,
		/// and a key that is not present. Use the overload that has a
		/// boolean <em>out</em> parameter instead.
		/// </remarks>
		TValue GetValue(object key);

		/// <summary>
		/// Gets an element that is associated with a key that is considered to
		/// be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <value>An element that is associated with a key that is
		/// considered to be equal to <paramref name="key"/>.</value>
		/// <exception cref="System.Collections.Generic.KeyNotFoundException">
		/// There is no element associated with a key that is considered to be
		/// equal to <paramref name="key"/>.
		/// </exception>
		TValue this[object key] { get; }
		
		/// <summary>
		/// Returns whether the specified key is a valid key for this
		/// keyed collection.
		/// </summary>
		/// <param name="key">The key to test.</param>
		/// <returns><see langword="true"/> when <paramref name="key"/>
		/// would be a valid key for this keyed collection;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// This method does <em>not</em> check whether the key is valid
		/// for the current state of the keyed collection. For example, the
		/// instance would not return <see langword="false"/> just because the
		/// given key is already present.
		/// </remarks>
		[Pure]
		bool IsValidKey(object key);

		/// <summary>
		/// Returns whether the specified value is a valid value for this
		/// keyed collection.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/>
		/// would be a valid value for this keyed collection;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		bool IsValidValue(object value);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IKeyedCollection<,>))]
		abstract class IKeyedCollectionContract<TKey, TValue> : IKeyedCollection<TKey, TValue>
		{
			public ICollection<TKey> Keys
			{
				get
				{
					Contract.Ensures(Contract.Result<ICollection<TKey>>() != null);
					Contract.Ensures(Contract.ForAll(Contract.Result<ICollection<TKey>>(), k => IsValidKey(k)));
					return default(ICollection<TKey>);
				}
			}

			public ICollection<TValue> Values
			{
				get
				{
					Contract.Ensures(Contract.Result<ICollection<TValue>>() != null);
					Contract.Ensures(Contract.ForAll(Contract.Result<ICollection<TValue>>(), v => IsValidValue(v)));
					return default(ICollection<TValue>);
				}
			}

			public TValue this[object key]
			{
				get
				{
					Contract.Ensures(IsValidValue(Contract.Result<TValue>()));
					return default(TValue);
				}
			}

			public TValue GetValue(object key, out bool isPresent)
			{
				isPresent = default(bool);
				return default(TValue);
			}

			public TValue GetValue(object key)
			{
				return default(TValue);
			}

			public bool ContainsKey(object key)
			{
				return default(bool);
			}

			public bool IsValidKey(object key)
			{
				return default(bool);
			}

			public bool IsValidValue(object value)
			{
				return default(bool);
			}

			// ICollection<T>
			public abstract IAssociation<TKey, TValue> GetMember(object value, out bool isPresent);
			public abstract IAssociation<TKey, TValue> GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
