﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A mutable list whose items can only be replaced.
	/// </summary>
	/// <typeparam name="T">The type of element in the array.</typeparam>
	/// <remarks>
	/// There is no <c>IArray&lt;T&gt;</c> read-only counterpart for this interface.
	/// Use <see cref="IList{T}"/> instead.
	/// </remarks>
	[ContractClass(typeof(Contracts.IMutableArrayContract<>))]
	public interface IMutableArray<T> : IList<T>
	{
		/// <summary>
		/// Gets or sets an element that has the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element.</param>
		/// <returns>The element at <paramref name="index"/>.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		new T this[int index] { get; set; }
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableArray<>))]
		abstract class IMutableArrayContract<T> : IMutableArray<T>
		{
			public T this[int index]
			{
				get
				{
					Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
					Contract.Requires<ArgumentOutOfRangeException>(index < Count);
					Contract.Ensures(IsValidMember(Contract.Result<T>()));
					return default(T);
				}
				set
				{
					Contract.Requires<CollectionFrozenException>(!IsFrozen);
					Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
					Contract.Requires<ArgumentOutOfRangeException>(index < Count);
					Contract.Requires<ArgumentException>(IsValidMember(value));
				}
			}

			// IList<T>
			T IList<T>.this[int index] { get { throw new NotImplementedException(); } }

			// IList
			object IList.this[int index] { get { throw new NotImplementedException(); } }
			public abstract int? IndexOf(object value);

			// ICollection<T>
			public abstract T GetMember(object value, out bool isPresent);
			public abstract T GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
