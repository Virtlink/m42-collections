﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// An indexed ordered finite list of elements.
	/// </summary>
	[ContractClass(typeof(Contracts.IListContract))]
	public interface IList : ICollection
	{
		/// <summary>
		/// Gets an element that has the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element.</param>
		/// <returns>The element at <paramref name="index"/>.</returns>
		object this[int index] { get; }

		/// <summary>
		/// Returns the index in the collection of an element that is
		/// considered to be equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The zero-based index of the first element that is
		/// considered to be equal to the specified value;
		/// or <see langword="null"/> when no such element
		/// could be found.</returns>
		/// <remarks>
		/// <para>
		/// Multiple calls to this method on a list that has not
		/// changed in between calls will return the same index.
		/// </para>
		/// <para>
		/// Implementations should not throw exceptions. When the type of
		/// <paramref name="value"/> is not compatible or when
		/// <see cref="ICollection.IsValidMember"/> would return <see langword="false"/>,
		/// then this method should return <see langword="null"/> instead
		/// of throwing an exception. This indicates that the requested element
		/// could not be found.
		/// </para>
		/// <para>
		/// The <paramref name="value"/> is compared to the value of each
		/// element using the list's default equality comparer.
		/// </para>
		/// </remarks>
		[Pure]
		int? IndexOf(object value);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IList))]
		abstract class IListContract : IList
		{
			public object this[int index]
			{
				get
				{
					Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
					Contract.Requires<ArgumentOutOfRangeException>(index < Count);
					Contract.Ensures(IsValidMember(Contract.Result<object>()));
					return default(object);
				}
			}

			public int? IndexOf(object value)
			{
				// TODO: Fix this contract. The static checker can't figure it out.
				//Contract.Ensures(!Contract.Result<int?>().HasValue || Contract.Result<int?>().Value >= 0);
				//Contract.Ensures(!Contract.Result<int?>().HasValue || Contract.Result<int?>().Value < Count);
				return default(int?);
			}

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
