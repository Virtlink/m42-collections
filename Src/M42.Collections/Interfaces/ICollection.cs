﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A collection.
	/// </summary>
	[ContractClass(typeof(Contracts.ICollectionContract))]
	public interface ICollection : SC.IEnumerable
	{
		/// <summary>
		/// Gets the number of elements in the collection.
		/// </summary>
		/// <value>The number of elements in the collection.</value>
		int Count
		{ get; }

		/// <summary>
		/// Gets whether the collection is empty.
		/// </summary>
		/// <value><see langword="true"/> when there are no more elements in
		/// the collection; otherwise, <see langword="false"/>.</value>
		bool IsEmpty
		{ get; }

		/// <summary>
		/// Gets whether the collection could ever change.
		/// </summary>
		/// <value><see langword="true"/> when the collection will never
		/// change; otherwise, <see langword="false"/> when the collection
		/// might change.</value>
		/// <remarks>
		/// When this property returns <see langword="false"/>, there is no
		/// guarantee that the oollection never changes, even if the oollection
		/// is read-only.
		/// </remarks>
		bool IsFrozen
		{ get; }

		/// <summary>
		/// Gets a value from the collection that is considered to be equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="isPresent">Returns <see langword="true"/> when
		/// a value equal to <paramref name="value"/> was present in the
		/// collection and returned; otherwise, <see langword="false"/> when
		/// no such value was present and the returned value is
		/// <see langword="null"/>.</param>
		/// <returns>A value that is equal to <paramref name="value"/>;
		/// or <see langword="null"/> when no such value
		/// was found.</returns>
		[Pure]
		object GetMember(object value, out bool isPresent);

		/// <summary>
		/// Gets a value from the collection that is considered to be equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>A value that is equal to <paramref name="value"/>;
		/// or <see langword="null"/> when no such value was found.</returns>
		/// <remarks>
		/// This overload does not show the difference between a value that is
		/// present in the collection but equal to <see langword="null"/>,
		/// and a value that is not present. Use the overload that has a
		/// boolean <em>out</em> parameter instead.
		/// </remarks>
		[Pure]
		object GetMember(object value);

		/// <summary>
		/// Returns whether the collection contains an element that is equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when the collection contains an
		/// element that is equal to <paramref name="value"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>
		/// Implementations should not throw exceptions. When the type of
		/// <paramref name="value"/> is not compatible or when
		/// <see cref="IsValidMember"/> would return <see langword="false"/>,
		/// then this method should return <see langword="false"/> instead
		/// of throwing an exception. This indicates that the requested element
		/// could not be found.
		/// </para>
		/// <para>
		/// The implementation determines the equality
		/// comparer that is used to compare the elements.
		/// To specify a custom comparer, use the appropriate
		/// <see cref="System.Linq.Enumerable.Contains"/> overload.
		/// </para>
		/// </remarks>
		[Pure]
		bool Contains(object value);

		/// <summary>
		/// Returns whether the specified value would be a valid member of this
		/// collection.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/>
		/// would be a valid member of this collection;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>
		/// This method does <em>not</em> check whether the value is valid
		/// for the current state of the collection. For example, a collection
		/// that does not allow duplicate values would not return
		/// <see langword="false"/> just because the given value is already
		/// present.
		/// </para>
		/// <para>
		/// Implementations should not throw errors.
		/// </para>
		/// </remarks>
		[Pure]
		bool IsValidMember(object value);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(ICollection))]
		abstract class ICollectionContract : ICollection
		{
			public int Count
			{
				get
				{
					Contract.Ensures(Contract.Result<int>() >= 0);
					return default(int);
				}
			}

			public bool IsEmpty
			{
				get
				{
					Contract.Ensures(Contract.Result<bool>() == (Count == 0));
					return default(bool);
				}
			}

			public bool Contains(object value)
			{
				return default(bool);
			}

			public bool IsFrozen
			{
				get
				{
					return default(bool);
				}
			}

			public bool IsValidMember(object value)
			{
				return default(bool);
			}

			public object GetMember(object value, out bool isPresent)
			{
				isPresent = default(bool);
				return default(object);
			}

			public object GetMember(object value)
			{
				return default(object);
			}

			// IEnumerable
			public abstract SC.IEnumerator GetEnumerator();
		}
	}
	#endregion
}
