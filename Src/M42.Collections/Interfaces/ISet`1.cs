﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A set of unique elements.
	/// </summary>
	/// <typeparam name="T">The type of elements in the set.</typeparam>
	[ContractClass(typeof(Contracts.ISetContract<>))]
	public interface ISet<out T> : ICollection<T>, ISet
	{
		
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(ISet<>))]
		abstract class ISetContract<T> : ISet<T>
		{
			// ISet
			public abstract bool Overlaps(SC.IEnumerable other);
			public abstract bool SetEquals(SC.IEnumerable other);
			public abstract bool IsSubsetOf(SC.IEnumerable other);
			public abstract bool IsProperSubsetOf(SC.IEnumerable other);
			public abstract bool IsSupersetOf(SC.IEnumerable other);
			public abstract bool IsProperSupersetOf(SC.IEnumerable other);

			// ICollection<T>
			public abstract T GetMember(object value, out bool isPresent);
			public abstract T GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
