﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A collection.
	/// </summary>
	/// <typeparam name="T">The type of elements in the collection.</typeparam>
	[ContractClass(typeof(Contracts.ICollectionContract<>))]
	public interface ICollection<out T> : SCG.IEnumerable<T>, ICollection
	{
		/// <summary>
		/// Gets a value from the collection that is considered to be equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="isPresent">Returns <see langword="true"/> when
		/// a value equal to <paramref name="value"/> was present in the
		/// collection and returned; otherwise, <see langword="false"/> when
		/// no such value was present and the returned value is the default
		/// of <typeparamref name="T"/>.</param>
		/// <returns>A value that is equal to <paramref name="value"/>;
		/// or the default of <typeparamref name="T"/> when no such value
		/// was found.</returns>
		[Pure]
		new T GetMember(object value, out bool isPresent);

		/// <summary>
		/// Gets a value from the collection that is considered to be equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>A value that is equal to <paramref name="value"/>;
		/// or the default of <typeparamref name="T"/> when no such value
		/// was found.</returns>
		/// <remarks>
		/// When <typeparamref name="T"/> is a value type, or a reference type
		/// which may be <see langword="null"/>, then this overload does not
		/// show the difference between a value that is present in the
		/// collection but equal to the default of <typeparamref name="T"/>,
		/// and a value that is not present. Use the overload that has a
		/// boolean <em>out</em> parameter instead.
		/// </remarks>
		[Pure]
		new T GetMember(object value);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(ICollection<>))]
		abstract class ICollectionContract<T> : ICollection<T>
		{
			public T GetMember(object value, out bool isPresent)
			{
				isPresent = default(bool);
				return default(T);
			}

			public T GetMember(object value)
			{
				return default(T);
			}

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();
			
			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
