﻿#region Copyright and License
// Copyright 2012 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using SC = System.Collections;
using SCG = System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace M42.Collections
{
	/// <summary>
	/// Interface for collections that can be frozen.
	/// </summary>
	/// <typeparam name="TCollection">The type of the frozen
	/// collection.</typeparam>
	/// <typeparam name="T">The type of elements in the frozen
	/// collection.</typeparam>
	/// <remarks>
	/// A frozen collection is a collection that will never change.
	/// When a collection is not freezable, the only way to ensure that the
	/// collection will never change is to make a copy, put it in a read-only
	/// container and destroy all references to the copy.
	/// </remarks>
	public interface IFreezable<out TCollection, out T>
		where TCollection : ICollection<T>
	{
		/// <summary>
		/// Gets whether the collection could ever change.
		/// </summary>
		/// <value><see langword="true"/> when the collection will never
		/// change; otherwise, <see langword="false"/> when the collection
		/// might change.</value>
		/// <remarks>
		/// When this property returns <see langword="false"/>, there is no
		/// guarantee that the oollection never changes, even if the oollection
		/// is read-only.
		/// </remarks>
		bool IsFrozen
		{ get; }

		/// <summary>
		/// Freezes this collection in its current state.
		/// </summary>
		/// <returns>The frozen collection.</returns>
		TCollection Freeze();
	}
}
