﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// An ordered list of elements whose contents can be modified.
	/// </summary>
	/// <typeparam name="T">The type of elements in the list.</typeparam>
	[ContractClass(typeof(Contracts.IMutableListContract<>))]
	public interface IMutableList<T>
		: IMutableArray<T>, IList<T>, IMutableCollection<T>
	{
		/// <summary>
		/// Inserts the specified value at the specified index in the list.
		/// </summary>
		/// <param name="index">The zero-based index at which to insert
		/// <paramref name="value"/>.</param>
		/// <param name="value">The value to insert.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/>
		/// was successfully inserted into the list; otherwise,
		/// <see langword="false"/>.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool Insert(int index, T value);

		/// <summary>
		/// Removes the element at the specified zero-based index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to
		/// remove.</param>
		/// <returns><see langword="true"/> when an element at the specified
		/// index was found and removed; otherwise, <see langword="false"/>
		/// when the element could not be removed.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool RemoveAt(int index);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableList<>))]
		abstract class IMutableListContract<T> : IMutableList<T>
		{
			public bool Insert(int index, T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index <= Count);
				return default(bool);
			}

			public bool RemoveAt(int index)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
				Contract.Requires<ArgumentOutOfRangeException>(index < Count);
				return default(bool);
			}

			// IMutableCollection<T>
			public abstract bool Add(T value);
			public abstract bool Remove(object value);
			public abstract void Clear();

			// IMutableArray<T>
			public abstract T this[int index] { get; set; }

			// IList<T>
			T IList<T>.this[int index] { get { throw new NotImplementedException(); } }

			// IList
			object IList.this[int index] { get { throw new NotImplementedException(); } }
			public abstract int? IndexOf(object value);

			// ICollection<T>
			public abstract T GetMember(object value, out bool isPresent);
			public abstract T GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
