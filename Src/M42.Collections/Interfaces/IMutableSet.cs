﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A mutable set of unique elements.
	/// </summary>
	/// <typeparam name="T">The type of elements in the set.</typeparam>
	[ContractClass(typeof(Contracts.IMutableSetContract<>))]
	public interface IMutableSet<T> : ISet<T>, IMutableCollection<T>
	{
		/// <summary>
		/// Updates the element that is equal to the specified value
		/// with the specified value.
		/// </summary>
		/// <param name="value">The new value of the element.</param>
		/// <returns><see langword="true"/> when an equal element was found
		/// and updated; otherwise, <see langword="false"/>.</returns>
		bool Update(T value);

		/// <summary>
		/// Adds the specified value to the set, overwriting an existing equal
		/// value if present.
		/// </summary>
		/// <param name="value">The new value of the element.</param>
		/// <returns><see langword="true"/> when added or updated;
		/// otherwise, <see langword="false"/>.</returns>
		bool AddOrUpdate(T value);

		/// <summary>
		/// Removes all elements in the specified enumerable collection
		/// from this set.
		/// </summary>
		/// <param name="other">The enumerable collection of elements to remove from this set.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		void ExceptWith(SCG.IEnumerable<T> other);

		/// <summary>
		/// Removes any elements common to the specified enumerable collection and this set
		/// from this set, and adds any other elements from the specified enumerable collection to this set.
		/// </summary>
		/// <param name="other">The enumerable collection of elements to add to or remove from this set.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		void SymmetricExceptWith(SCG.IEnumerable<T> other);

		/// <summary>
		/// Keeps only elements common to the specified enumerable collection and this set
		/// in this set.
		/// </summary>
		/// <param name="other">The enumerable collection of elements to intersect with.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		void IntersectWith(SCG.IEnumerable<T> other);

		/// <summary>
		/// Keeps all elements in the specified enumerable collection and this set
		/// in this set.
		/// </summary>
		/// <param name="other">The enumerable collection of elements to union with.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		void UnionWith(SCG.IEnumerable<T> other);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableSet<>))]
		abstract class IMutableSetContract<T> : IMutableSet<T>
		{
			public bool Update(T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			public bool AddOrUpdate(T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			public void ExceptWith(SCG.IEnumerable<T> other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			public void SymmetricExceptWith(SCG.IEnumerable<T> other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			public void IntersectWith(SCG.IEnumerable<T> other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			public void UnionWith(SCG.IEnumerable<T> other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			// IMutableCollection<T>
			public abstract bool Add(T value);
			public abstract bool Remove(object value);
			public abstract void Clear();

			// IMutableArray<T>
			public abstract T this[int index] { get; set; }

			// ISet<T>
			public abstract bool Overlaps(SC.IEnumerable other);
			public abstract bool SetEquals(SC.IEnumerable other);
			public abstract bool IsSubsetOf(SC.IEnumerable other);
			public abstract bool IsProperSubsetOf(SC.IEnumerable other);
			public abstract bool IsSupersetOf(SC.IEnumerable other);
			public abstract bool IsProperSupersetOf(SC.IEnumerable other);

			// ICollection<T>
			public abstract T GetMember(object value, out bool isPresent);
			public abstract T GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
