﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A set of unique elements.
	/// </summary>
	[ContractClass(typeof(Contracts.ISetContract))]
	public interface ISet : ICollection
	{
		/// <summary>
		/// Returns whether this set and the specified enumerable collection have
		/// any elements in common.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set has one or more elements in common with <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		[Pure]
		bool Overlaps(SC.IEnumerable other);

		/// <summary>
		/// Returns whether this set and the specified enumerable collection have
		/// all elements in common.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set has all elements in common with <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>Duplicate elements in <paramref name="other"/> are ignored.</para>
		/// <para>The order of elements in <paramref name="other"/> is ignored.</para>
		/// </remarks>
		[Pure]
		bool SetEquals(SC.IEnumerable other);

		/// <summary>
		/// Returns whether the entire set is a subset of the specified
		/// enumerable collection.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set is a subset of <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		[Pure]
		bool IsSubsetOf(SC.IEnumerable other);

		/// <summary>
		/// Returns whether the entire set is a proper subset of the specified
		/// enumerable collection.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set is a proper subset of <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>A proper subset of <paramref name="other"/> is a subset of <paramref name="other"/>
		/// where <paramref name="other"/> contains at least one element that this set does not contain.</para>
		/// <para>Duplicate elements in <paramref name="other"/> are ignored.</para>
		/// </remarks>
		[Pure]
		bool IsProperSubsetOf(SC.IEnumerable other);

		/// <summary>
		/// Returns whether the entire set is a superset of the specified
		/// enumerable collection.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set is a superset of <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// Duplicate elements in <paramref name="other"/> are ignored.
		/// </remarks>
		[Pure]
		bool IsSupersetOf(SC.IEnumerable other);

		/// <summary>
		/// Returns whether the entire set is a proper superset of the specified
		/// enumerable collection.
		/// </summary>
		/// <param name="other">The enumerable collection to compare against.</param>
		/// <returns><see langword="true"/> when this set is a proper superset of <paramref name="other"/>;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// <para>A proper subset of <paramref name="other"/> is a superset of <paramref name="other"/>
		/// where this set contains at least one element that <paramref name="other"/> does not contain.</para>
		/// <para>Duplicate elements in <paramref name="other"/> are ignored.</para>
		/// </remarks>
		[Pure]
		bool IsProperSupersetOf(SC.IEnumerable other);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(ISet))]
		abstract class ISetContract : ISet
		{
			public bool Overlaps(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				return default(bool);
			}

			public bool SetEquals(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				//Contract.Ensures(Contract.Result<bool>().Implies(Overlaps(other)));
				//Contract.Ensures(Contract.Result<bool>().Implies(IsSubsetOf(other)));
				//Contract.Ensures(Contract.Result<bool>().Implies(IsSupersetOf(other)));
				return default(bool);
			}

			public bool IsSubsetOf(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				//Contract.Ensures(Contract.Result<bool>().Implies(Overlaps(other)));
				return default(bool);
			}

			public bool IsProperSubsetOf(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				//Contract.Ensures(Contract.Result<bool>().Implies(IsSubsetOf(other)));
				return default(bool);
			}

			public bool IsSupersetOf(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				//Contract.Ensures(Contract.Result<bool>().Implies(Overlaps(other)));
				return default(bool);
			}

			public bool IsProperSupersetOf(SC.IEnumerable other)
			{
				Contract.Requires<ArgumentNullException>(other != null);
				//Contract.Ensures(Contract.Result<bool>().Implies(IsSupersetOf(other)));
				return default(bool);
			}

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
