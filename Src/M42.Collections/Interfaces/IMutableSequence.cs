﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;

namespace M42.Collections
{
	/// <summary>
	/// A mutable sequence, such as a queue or stack.
	/// </summary>
	/// <typeparam name="T">The type of elements in the sequence.</typeparam>
	[ContractClass(typeof(Contracts.IMutableSequenceContract<>))]
	public interface IMutableSequence<T>
	{
		/// <summary>
		/// Gets whether the sequence is empty.
		/// </summary>
		/// <value><see langword="true"/> when there are no more elements in
		/// the sequence; otherwise, <see langword="false"/>.</value>
		bool IsEmpty
		{ get; }

		/// <summary>
		/// Returns whether the specified value is a valid member of this
		/// sequence.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/>
		/// would be a valid member of this sequence;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// This method does <em>not</em> check whether the value is valid
		/// for the current state of the sequence. For example, a sequence
		/// that does not allow duplicate values would not return
		/// <see langword="false"/> just because the given value is already
		/// present.
		/// </remarks>
		[Pure]
		bool IsValidMember(object value);

		/// <summary>
		/// Adds a value to the sequence.
		/// </summary>
		/// <param name="value">The value to add to the sequence.</param>
		/// <returns><see langword="true"/> when the value was successfully
		/// added to the sequence; otherwise, <see langword="false"/>.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool Add(T value);

		/// <summary>
		/// Takes and returns the element from the sequence.
		/// </summary>
		/// <returns>The next element in the sequence.</returns>
		/// <exception cref="SequenceEmptyException">
		/// The sequence is empty.
		/// </exception>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		T Take();

		/// <summary>
		/// Returns the element that would be returned by the next call to
		/// <see cref="Take"/>, without actually removing the element.
		/// </summary>
		/// <returns>The element that would be returned by the next call to
		/// <see cref="Take"/>.</returns>
		/// <exception cref="SequenceEmptyException">
		/// The sequence is empty.
		/// </exception>
		T Peek();
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableSequence<>))]
		abstract class IMutableSequenceContract<T> : IMutableSequence<T>
		{
			public bool IsEmpty
			{
				get { return default(bool); }
			}

			public bool IsValidMember(object value)
			{
				return default(bool);
			}

			public bool Add(T value)
			{
				Contract.Requires<ArgumentException>(IsValidMember(value));
				return default(bool);
			}

			public T Take()
			{
				Contract.Requires<SequenceEmptyException>(!IsEmpty);
				return default(T);
			}

			public T Peek()
			{
				Contract.Requires<SequenceEmptyException>(!IsEmpty);
				return default(T);
			}
		}
	}
	#endregion
}
