﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace M42.Collections
{
	/// <summary>
	/// An association between a key and a value.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <remarks>
	/// Don't inherit from this interface. To create an association,
	/// use <see cref="Association.Create"/>.
	/// </remarks>
	public interface IAssociation<out TKey, out TValue>
	{
		/// <summary>
		/// Gets the key of the association.
		/// </summary>
		/// <value>A key.</value>
		TKey Key
		{ get; }

		/// <summary>
		/// Gets the value of the association.
		/// </summary>
		/// <value>A value.</value>
		TValue Value
		{ get; }
	}
}
