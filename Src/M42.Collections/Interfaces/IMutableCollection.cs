﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A mutable collection.
	/// </summary>
	/// <typeparam name="T">The type of elements in the collection.</typeparam>
	[ContractClass(typeof(Contracts.IMutableCollectionContract<>))]
	public interface IMutableCollection<T> : ICollection<T>
	{
		/// <summary>
		/// Adds a value to the collection.
		/// </summary>
		/// <param name="value">The value to add to the collection.</param>
		/// <returns><see langword="true"/> when the value was successfully
		/// added to the collection; otherwise, <see langword="false"/>.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool Add(T value);

		/// <summary>
		/// Removes an element from the collection that is equal to the
		/// specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns><see langword="true"/> when an element equal to
		/// <paramref name="value"/> was found and removed; otherwise,
		/// <see langword="false"/> when such an element was either not found
		/// or could not be removed.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool Remove(object value);

		/// <summary>
		/// Removes all elements from the collection.
		/// </summary>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		void Clear();
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableCollection<>))]
		abstract class IMutableCollectionContract<T> : IMutableCollection<T>
		{
			public bool Add(T value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentException>(IsValidMember(value));
				return default(bool);
			}

			public bool Remove(object value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			public void Clear()
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
			}

			// ICollection<T>
			public abstract T GetMember(object value, out bool isPresent);
			public abstract T GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
