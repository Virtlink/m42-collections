﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A mutable collection whose elements can be retrieved by key.
	/// </summary>
	/// <typeparam name="TKey">The type of the keys in the collection.</typeparam>
	/// <typeparam name="TValue">The type of the elements in the collection.</typeparam>
	[ContractClass(typeof(Contracts.IMutableKeyedCollectionContract<,>))]
	public interface IMutableKeyedCollection<TKey, TValue>
		: IKeyedCollection<TKey, TValue>, IMutableCollection<IAssociation<TKey, TValue>>
	{
		/// <summary>
		/// Gets or sets an element that is associated with a key that is
		/// considered to be equal to the specified key.
		/// </summary>
		/// <param name="key">The key to look for or set.</param>
		/// <value>An element that is associated with a key that is
		/// considered to be equal to <paramref name="key"/>.</value>
		/// <remarks>
		/// When setting the value associated with a particular key,
		/// all existing associated elements are removed and the set value is
		/// the only element associated with <paramref name="key"/>.
		/// </remarks>
		/// <exception cref="System.Collections.Generic.KeyNotFoundException">
		/// There is no element associated with a key that is considered to be
		/// equal to <paramref name="key"/>.
		/// </exception>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		TValue this[TKey key] { get; set; }
		
		/// <summary>
		/// Adds the specified key-value pair to the collection.
		/// </summary>
		/// <param name="key">The key to add.</param>
		/// <param name="value">The value that is associated with
		/// <paramref name="key"/>.</param>
		/// <returns><see langword="true"/> when the key and value were added;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// A key-value pair may sometimes not be added, for example, when an
		/// equal element already exists in a set.
		/// </remarks>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool Add(TKey key, TValue value);

		/// <summary>
		/// Removes a key-value pair whose key is considered to be equal to
		/// the specified key from the collection.
		/// </summary>
		/// <param name="key">The key to remove.</param>
		/// <returns><see langword="true"/> when a key was found that is
		/// considered to be equal to <paramref name="key"/>, and it was
		/// removed; otherwise, <see langword="false"/> when no such key
		/// was found.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		bool RemoveKey(object key);
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IMutableKeyedCollection<,>))]
		abstract class IMutableKeyedCollectionContract<TKey, TValue> : IMutableKeyedCollection<TKey, TValue>
		{
			public TValue this[TKey key]
			{
				get
				{
					Contract.Ensures(IsValidValue(Contract.Result<TValue>()));
					return default(TValue);
				}
				set
				{
					Contract.Requires<CollectionFrozenException>(!IsFrozen);
					Contract.Requires<ArgumentException>(IsValidKey(key));
					Contract.Requires<ArgumentException>(IsValidValue(value));
				}
			}

			public bool Add(TKey key, TValue value)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				Contract.Requires<ArgumentException>(IsValidKey(key));
				Contract.Requires<ArgumentException>(IsValidValue(value));
				return default(bool);
			}

			public bool RemoveKey(object key)
			{
				Contract.Requires<CollectionFrozenException>(!IsFrozen);
				return default(bool);
			}

			// IKeyedCollection<TKey, TValue>
			public abstract ICollection<TKey> Keys { get; }
			public abstract ICollection<TValue> Values { get; }
			public abstract TValue this[object key] { get; }
			public abstract TValue GetValue(object key, out bool isPresent);
			public abstract TValue GetValue(object key);
			public abstract bool ContainsKey(object key);
			public abstract bool IsValidKey(object value);
			public abstract bool IsValidValue(object value);

			// IMutableCollection<T>
			public abstract bool Add(IAssociation<TKey, TValue> value);
			public abstract bool Remove(object value);
			public abstract void Clear();

			// ICollection<T>
			public abstract IAssociation<TKey, TValue> GetMember(object value, out bool isPresent);
			public abstract IAssociation<TKey, TValue> GetMember(object value);

			// ICollection
			public abstract int Count { get; }
			public abstract bool IsEmpty { get; }
			public abstract bool Contains(object value);
			public abstract bool IsFrozen { get; }
			public abstract bool IsValidMember(object value);
			object ICollection.GetMember(object value, out bool isPresent) { throw new NotImplementedException(); }
			object ICollection.GetMember(object value) { throw new NotImplementedException(); }

			// IEnumerable<T>
			public abstract SCG.IEnumerator<IAssociation<TKey, TValue>> GetEnumerator();

			// IEnumerable
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
