﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;

namespace M42.Collections
{
	/// <summary>
	/// Interface for collections that can be frozen.
	/// </summary>
	/// <remarks>
	/// A frozen collection is a collection that will never change.
	/// When a collection is not freezable, the only way to ensure that the
	/// collection will never change is to make a copy, put it in a read-only
	/// container and destroy all references to the copy.
	/// </remarks>
	[ContractClass(typeof(Contracts.IFreezableContract))]
	public interface IFreezable
	{
		/// <summary>
		/// Gets whether the collection could ever change.
		/// </summary>
		/// <value><see langword="true"/> when the collection will never
		/// change; otherwise, <see langword="false"/> when the collection
		/// might change.</value>
		/// <remarks>
		/// When this property returns <see langword="false"/>, there is no
		/// guarantee that the oollection never changes, even if the oollection
		/// is read-only.
		/// </remarks>
		bool IsFrozen
		{ get; }

		/// <summary>
		/// Freezes this collection in its current state.
		/// </summary>
		/// <remarks>
		/// Freezing a collection may cause the internal structures to be
		/// cloned to guarantee that the collection will never change.
		/// This happens most often because a reference to an internal structure
		/// may have leaked from the collection.
		/// </remarks>
		void Freeze();
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IFreezable))]
		abstract class IFreezableContract : IFreezable
		{
			public void Freeze()
			{
				Contract.Requires<InvalidOperationException>(!IsFrozen);
				Contract.Ensures(IsFrozen);
			}

			public bool IsFrozen
			{
				get
				{
					return default(bool);
				}
			}
		}
	}
	#endregion
}
