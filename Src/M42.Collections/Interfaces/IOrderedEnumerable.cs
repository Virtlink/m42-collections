﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections.Interfaces
{
	/// <summary>
	/// An enumerable collection that is ordered, as opposed to having no
	/// specific order.
	/// </summary>
	/// <typeparam name="T">The type of elements.</typeparam>
	[ContractClass(typeof(Contracts.IOrderedEnumerableContract<>))]
	public interface IOrderedEnumerable<out T> : SCG.IEnumerable<T>
	{
		/// <summary>
		/// Returns an enumerator that iterates through the collection
		/// in reverse.
		/// </summary>
		/// <returns>An <see cref="SCG.IEnumerator{T}"/> that can be used to iterate
		/// through the collection.</returns>
		SCG.IEnumerator<T> GetReverseEnumerator();
	}

	#region Contract
	namespace Contracts
	{
		[ContractClassFor(typeof(IOrderedEnumerable<>))]
		abstract class IOrderedEnumerableContract<T> : IOrderedEnumerable<T>
		{
			public SCG.IEnumerator<T> GetReverseEnumerator()
			{
				Contract.Ensures(Contract.Result<SCG.IEnumerator<T>>() != null);
				return default(SCG.IEnumerator<T>);
			}

			// IEnumerable<T>
			public abstract SCG.IEnumerator<T> GetEnumerator();
			SC.IEnumerator SC.IEnumerable.GetEnumerator() { throw new NotImplementedException(); }
		}
	}
	#endregion
}
