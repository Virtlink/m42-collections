﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A set of unique objects.
	/// </summary>
	public class HashSet<T> : IMutableSet<T>, IFreezable
	{
		/// <summary>
		/// The maximum percentage of the bucket array that may be full.
		/// </summary>
		private readonly float loadFactor = 0.66f;
		/// <summary>
		/// A random factor by which the hash is multiplied.
		/// </summary>
		private readonly uint hashFactor;
		/// <summary>
		/// The initial power-of-two size of the bucket array.
		/// </summary>
		private readonly int initialPower;

		/// <summary>
		/// The array with all the buckets.
		/// </summary>
		private Bucket[] bucketArray;

		/// <summary>
		/// The maximum number of items to contain in the bucket array.
		/// </summary>
		private int resizeThreshold;		// Reset()
		/// <summary>
		/// The current power-of-two size of the bucket array.
		/// </summary>
		private int power;
		/// <summary>
		/// The bit mask of the index bits.
		/// </summary>
		private int indexMask;

		private int count = 0;
		/// <inheritdoc />
		public int Count
		{
			get { return this.count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return Count == 0; }
		}

		private bool isFrozen = false;
		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return this.isFrozen; }
		}

		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the comparer used to compare the values of elements in this
		/// collection.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		#region Constructors
		/// <summary>
		/// The minimum fill factor.
		/// </summary>
		private const float MinFillFactor = 0.10f;
		/// <summary>
		/// The maximum fill factor.
		/// </summary>
		private const float MaxFillFactor = 0.95f;
		/// <summary>
		/// The minimum power.
		/// </summary>
		private const int MinPower = 4;
		/// <summary>
		/// The default initial capacity.
		/// </summary>
		private const int DefaultInitialCapacity = 4;
		/// <summary>
		/// The default fill factor.
		/// </summary>
		private const float DefaultFillFactor = 0.70f;
		/// <summary>
		/// A big prime number.
		/// </summary>
		private const int BigPrime = 486187739;

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="HashSet{T}"/> class.
		/// </summary>
		public HashSet()
			: this(SCG.EqualityComparer<T>.Default)
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the <see cref="HashSet{T}"/> class.
		/// </summary>
		/// <param name="comparer">The comparer used to compare values.</param>
		public HashSet(SCG.IEqualityComparer<T> comparer)
			: this(comparer, DefaultInitialCapacity, DefaultFillFactor)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HashSet{T}"/> class.
		/// </summary>
		/// <param name="comparer">The comparer used to compare values.</param>
		/// <param name="initialCapacity">The initial capacity of the set.</param>
		/// <param name="loadFactor">The load factor of the hash set.</param>
		public HashSet(SCG.IEqualityComparer<T> comparer, int initialCapacity, float loadFactor)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Requires<ArgumentOutOfRangeException>(initialCapacity >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(loadFactor >= MinFillFactor);
			Contract.Requires<ArgumentOutOfRangeException>(loadFactor <= MaxFillFactor);
			#endregion
			this.comparer = comparer;
			this.loadFactor = loadFactor;
			this.hashFactor = BigPrime;

			// Find the lowest power of two that would result in a capacity
			// that is greater than or equal to the specified initial capacity.
			initialCapacity = (int)(initialCapacity / loadFactor);
			this.initialPower = MinPower;
			while ((initialCapacity - 1 >> this.initialPower) > 0)
				this.initialPower++;

			Reset();
		}

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="HashSet{T}"/> class.
		/// </summary>
		/// <param name="elements">The elements used to populate the set with.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="elements"/> are ignored.
		/// </remarks>
		public HashSet(SCG.IEnumerable<T> elements)
			: this(elements, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HashSet{T}"/> class.
		/// </summary>
		/// <param name="elements">The elements used to populate the set with.</param>
		/// <param name="comparer">The comparer used to compare values.</param>
		/// <remarks>
		/// Duplicate elements in <paramref name="elements"/> are ignored.
		/// </remarks>
		public HashSet(SCG.IEnumerable<T> elements, SCG.IEqualityComparer<T> comparer)
			: this(comparer, DefaultInitialCapacity, DefaultFillFactor)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			foreach (var element in elements)
			{
				Add(element);
			}
		}
		#endregion

		/// <inheritdoc />
		public bool Add(T value)
		{
			return AddElement(value);
		}

		/// <inheritdoc />
		public bool Remove(object value)
		{
			return RemoveElement(value);
		}

		/// <inheritdoc />
		public bool Update(T value)
		{
			return UpdateElement(value);
		}

		/// <inheritdoc />
		public bool AddOrUpdate(T value)
		{
			return AddOrUpdateElement(value);
		}

		/// <inheritdoc />
		public void Clear()
		{
			ClearElements();
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		public bool TryGetMember(object value, out T result)
		{
			if (!(value is T))
			{
				result = default(T);
				return false;
			}

			var bucket = GetBucket((T)value);
			result = bucket != null ? bucket.Value : default(T);
			return bucket != null;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			T result;
			isPresent = TryGetMember(value, out result);
			return isPresent ? result : default(T);
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool present;
			return ((ICollection<T>)this).GetMember(value, out present);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			T result;
			isPresent = TryGetMember(value, out result);
			return isPresent ? result : default(T);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			bool present;
			return ((ICollection<T>)this).GetMember(value, out present);
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			if (!(value is T))
				return false;

			return GetBucket((T)value) != null;
		}

		/// <inheritdoc />
		public virtual bool IsValidMember(object value)
		{
			return value is T;
		}

		/// <inheritdoc />
		public void ExceptWith(SCG.IEnumerable<T> other)
		{
			// CONTRACT: Inherited from IMutableSet<T>
			foreach (var element in other)
			{
				Remove(element);
			}
		}

		/// <inheritdoc />
		public void SymmetricExceptWith(SCG.IEnumerable<T> other)
		{
			// CONTRACT: Inherited from IMutableSet<T>
			var otherSet = other.ToM42Set(this.comparer);
			foreach (var element in otherSet)
			{
				if (!Add(element))
					Remove(element);
			}
		}

		/// <inheritdoc />
		public void IntersectWith(SCG.IEnumerable<T> other)
		{
			// CONTRACT: Inherited from IMutableSet<T>
			var otherSet = other.ToM42Set(this.comparer);
			ArrayList<T> toRemove = new ArrayList<T>();
			foreach (var element in this)
			{
				if (!otherSet.Contains(element))
					toRemove.Add(element);
			}
			ExceptWith(toRemove);
		}

		/// <inheritdoc />
		public void UnionWith(SCG.IEnumerable<T> other)
		{
			// CONTRACT: Inherited from IMutableSet<T>
			foreach (var element in other)
			{
				Add(element);
			}
		}

		/// <inheritdoc />
		public bool Overlaps(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			foreach (var element in other)
			{
				if (this.Contains(element))
					return true;
			}
			return false;
		}

		/// <inheritdoc />
		public bool SetEquals(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			foreach (var element in other)
			{
				if (!this.Contains(element))
					return false;
			}
			var otherSet = other.OfType<T>().ToM42Set(this.comparer);
			foreach (var element in this)
			{
				if (!otherSet.Contains(element))
					return false;
			}
			return true;
		}

		/// <inheritdoc />
		public bool IsSubsetOf(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			// All items in 'this' are in 'other'.
			var otherSet = other.OfType<T>().ToM42Set(this.comparer);
			foreach (var element in this)
			{
				if (!otherSet.Contains(element))
					return false;
			}
			return true;
		}

		/// <inheritdoc />
		public bool IsProperSubsetOf(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			// All items in this are in other.
			// No items in other are not in this.
			return IsSubsetOf(other) && !SetEquals(other);
		}

		/// <inheritdoc />
		public bool IsSupersetOf(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			// All items in 'other' are in 'this'.
			foreach (var element in other)
			{
				if (!this.Contains(element))
					return false;
			}
			return true;
		}

		/// <inheritdoc />
		public bool IsProperSupersetOf(SC.IEnumerable other)
		{
			// CONTRACT: Inherited from ISet<T>
			return IsSupersetOf(other) && !SetEquals(other);
		}

		/// <inheritdoc />
		public System.Collections.Generic.IEnumerator<T> GetEnumerator()
		{
			// NOTE: The order is not specified, but it is fixed
			// as long as the set is not modified.
			for (int i = 0; i < this.bucketArray.Length; i++)
			{
				var bucket = this.bucketArray[i];
				while (bucket != null)
				{
					yield return bucket.Value;
					bucket = bucket.Next;
				}
			}
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}


		/// <inheritdoc />
		public void Freeze()
		{
			// CONTRACT: Inherited from IEnumerable<T>
			this.isFrozen = true;
		}

		/// <summary>
		/// Asserts that the collection is not frozen,
		/// or throws an exception.
		/// </summary>
		private void AssertNotFrozen()
		{
			if (this.isFrozen)
				throw new CollectionFrozenException();
		}

		#region Protected Virtual Members
		/// <summary>
		/// Adds an element with the specified value to the set.
		/// </summary>
		/// <param name="value">The value of the element to add to the
		/// set.</param>
		/// <returns><see langword="true"/> when the element was added;
		/// otherwise, <see langword="false"/> when it already
		/// exists.</returns>
		protected virtual bool AddElement(T value)
		{
			AssertNotFrozen();
			return AddOrUpdateValue(value, true, false) != null;
		}

		/// <summary>
		/// Removes the element with the specified value from the set.
		/// </summary>
		/// <param name="value">The value to remove from the set.</param>
		/// <returns><see langword="true"/> when the element was removed;
		/// otherwise, <see langword="false"/>.</returns>
		protected virtual bool RemoveElement(object value)
		{
			AssertNotFrozen();
			if (!(value is T))
				return false;
			return RemoveValue((T)value) != null;
		}

		/// <summary>
		/// Updates the element with the specified value.
		/// </summary>
		/// <param name="value">The value to update in the set.</param>
		/// <returns><see langword="true"/> when the element was updated;
		/// otherwise, <see langword="false"/>.</returns>
		protected virtual bool UpdateElement(T value)
		{
			AssertNotFrozen();
			return AddOrUpdateValue(value, false, true) != null;
		}

		/// <summary>
		/// Adds or updates the element with the specified value.
		/// </summary>
		/// <param name="value">The value to add or update in the set.</param>
		/// <returns><see langword="true"/> when the element was aded or
		/// updated; otherwise, <see langword="false"/>.</returns>
		protected virtual bool AddOrUpdateElement(T value)
		{
			AssertNotFrozen();
			return AddOrUpdateValue(value, true, true) != null;
		}

		/// <summary>
		/// Removes all elements from the dictionary.
		/// </summary>
		protected virtual void ClearElements()
		{
			AssertNotFrozen();
			Reset();
		}
		#endregion

		#region Bucket
		/// <summary>
		/// A bucket that holds a single element.
		/// </summary>
		private sealed class Bucket
		{
			/// <summary>
			/// The value in the bucket.
			/// </summary>
			public T Value;
			/// <summary>
			/// The cached hash value.
			/// </summary>
			public int Hash;
			/// <summary>
			/// The next bucket in the linked list of buckets that have the
			/// same index.
			/// </summary>
			public Bucket Next;

			/// <summary>
			/// Initializes a new instance of the <see cref="Bucket"/> class.
			/// </summary>
			public Bucket(T value, int hash, Bucket next)
			{
				this.Value = value;
				this.Hash = hash;
				this.Next = next;
			}
		}
		#endregion

		/// <summary>
		/// Returns whether the two values are equal.
		/// </summary>
		/// <param name="lhv">The left-hand value.</param>
		/// <param name="rhv">The right-hand value.</param>
		/// <returns><see langword="true"/> when <paramref name="lhv"/>
		/// and <paramref name="rhv"/> are equal;
		/// otherwise, <see langword="false"/>.</returns>
		/// <remarks>
		/// The left-hand value and right-hand value do not need to be of the
		/// same type.
		/// </remarks>
		private bool AreEqual(T lhv, T rhv)
		{
			return this.comparer.Equals(lhv, rhv);
		}

		/// <summary>
		/// Returns the hash code of the specified value.
		/// </summary>
		/// <param name="value">The value to get the hash code for.</param>
		/// <returns>The hash code.</returns>
		private int GetHashCode(T value)
		{
			return this.comparer.GetHashCode(value);
		}

		/// <summary>
		/// Randomizes and trims the hash code to allow it to be used as an
		/// index into the internal bucket array.
		/// </summary>
		/// <param name="hash">The hash code.</param>
		/// <returns>The index in the internal bucket array.</returns>
		private int HashCodeToIndex(int hash)
		{
			return (int)(((uint)hash * this.hashFactor) & this.indexMask);
		}

		/// <summary>
		/// Resizes the inner bucket array.
		/// </summary>
		/// <param name="hashBits">The number of hash bits to use for all
		/// elements in this set.</param>
		private void Resize(int hashBits)
		{
			this.power = hashBits;
			this.indexMask = (1 << this.power) - 1;

			// Make the new array a factor two bigger.
			Bucket[] newBucketArray = new Bucket[indexMask + 1];

			for (int i = 0; i < bucketArray.Length; i++)
			{
				Bucket b = bucketArray[i];

				// For each chain of buckets...
				while (b != null)
				{
					// ... use the trimmed hash code as the index.
					int j = HashCodeToIndex(b.Hash);
					// Insert a new bucket at the current position in the table,
					// and use the existing bucket at that position as overflow.
					newBucketArray[j] = new Bucket(b.Value, b.Hash, newBucketArray[j]);
					// Go down the chain.
					b = b.Next;
				}

			}
			// Replace the table.
			bucketArray = newBucketArray;
			resizeThreshold = (int)(bucketArray.Length * loadFactor);
		}

		/// <summary>
		/// Ensures that the bucket array is big enough.
		/// </summary>
		private void EnsureBucketArraySize()
		{
			if (this.count > resizeThreshold)
				Resize(power + 1);
		}

		/// <summary>
		/// Gets the <see cref="Bucket"/> that contains a value that is equal
		/// to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The <see cref="Bucket"/> that contains an equal value;
		/// or <see langword="null"/> when no bucket contains
		/// an equal value.</returns>
		private Bucket GetBucket(T value)
		{
			int hash = GetHashCode(value);
			int index = HashCodeToIndex(hash);
			Bucket bucket = bucketArray[index];
			while (bucket != null)
			{
				if (AreEqual(bucket.Value, value))
					return bucket;
				bucket = bucket.Next;
			}
			return null;
		}

		/// <summary>
		/// Adds or updates a bucket with the specified value.
		/// </summary>
		/// <param name="value">The value to add or set.</param>
		/// <param name="add">Whether to add the value
		/// when it is not found.</param>
		/// <param name="update">Whether to update the value
		/// when it is already present.</param>
		/// <returns>The <see cref="Bucket"/> that was added
		/// or updated; or <see langword="null"/> when no bucket was found,
		/// added or updated.</returns>
		private Bucket AddOrUpdateValue(T value, bool add, bool update)
		{
			#region Contract
			Contract.Requires<ArgumentException>(add || update);
			#endregion
			int hash = GetHashCode(value);
			int index = HashCodeToIndex(hash);
			Bucket bucket = bucketArray[index];

			while (bucket != null)
			{
				if (AreEqual(bucket.Value, value))
				{
					// Found a bucket with the value!
					if (update)
					{
						// Change the bucket's value to the specified value.
						// But we know that the index is correct so no need to
						// move buckets around.
						bucket.Value = value;
						bucket.Hash = hash;
					}
					else
						// We are not updating, so return null.
						return null;
					break;
				}
				bucket = bucket.Next;
			}

			if (bucket == null && add)
			{
				// No bucket with that value. Prepend a new one.
				bucket = new Bucket(value, hash, bucketArray[index]);
				bucketArray[index] = bucket;
				this.count++;
				EnsureBucketArraySize();
			}

			return bucket;
		}

		/// <summary>
		/// Removes the bucket that contains a value equal to the specified
		/// value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The <see cref="Bucket"/> that was removed;
		/// or <see langword="null"/> when nothing was removed.</returns>
		private Bucket RemoveValue(T value)
		{
			if (this.count == 0)
				return null;

			int hash = GetHashCode(value);
			int index = HashCodeToIndex(hash);
			Bucket bucket = bucketArray[index];
			Bucket previous = null;

			if (bucket != null && AreEqual(bucket.Value, value))
			{
				bucketArray[index] = bucket.Next;
			}
			else
			{
				// Find the first non-null bucket that has a matching value.
				while (bucket != null && !AreEqual(bucket.Value, value))
				{
					previous = bucket;
					bucket = bucket.Next;
				}

				if (bucket == null)
					// No bucket found that matched the specified value.
					return null;

				// Remove the bucket.
				if (previous != null)
					previous.Next = bucket.Next;
				bucket.Next = null;
			}
			this.count--;
			return bucket;
		}

		/// <summary>
		/// Resets the set.
		/// </summary>
		/// <remarks>
		/// Do not make this method virtual. It is called from the constructor.
		/// </remarks>
		private void Reset()
		{
			this.power = this.initialPower;
			this.count = 0;
			this.bucketArray = new Bucket[indexMask + 1];
			this.resizeThreshold = (int)(this.bucketArray.Length * this.loadFactor);
		}

		#region SCG.ISet<T>
#if false
		/// <inheritdoc />
		int SCG.ICollection<T>.Count
		{
			get { return Count; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.IsReadOnly
		{
			get { return IsFrozen; }
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Remove(T item)
		{
			return Remove(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Add(T item)
		{
			Add(item);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.Add(T item)
		{
			return Add(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.Clear()
		{
			Clear();
		}

		/// <inheritdoc />
		bool SCG.ICollection<T>.Contains(T item)
		{
			return Contains(item);
		}

		/// <inheritdoc />
		void SCG.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			foreach (var element in this)
			{
				Contract.Assume(arrayIndex < array.Length);
				array[arrayIndex++] = element;
			}
		}

		/// <inheritdoc />
		void SCG.ISet<T>.ExceptWith(SCG.IEnumerable<T> other)
		{
			ExceptWith(other);
		}

		/// <inheritdoc />
		void SCG.ISet<T>.SymmetricExceptWith(SCG.IEnumerable<T> other)
		{
			SymmetricExceptWith(other);
		}

		/// <inheritdoc />
		void SCG.ISet<T>.UnionWith(SCG.IEnumerable<T> other)
		{
			UnionWith(other);
		}

		/// <inheritdoc />
		void SCG.ISet<T>.IntersectWith(SCG.IEnumerable<T> other)
		{
			IntersectWith(other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.IsProperSubsetOf(SCG.IEnumerable<T> other)
		{
			return IsProperSubsetOf((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.IsProperSupersetOf(SCG.IEnumerable<T> other)
		{
			return IsProperSupersetOf((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.IsSubsetOf(SCG.IEnumerable<T> other)
		{
			return IsSubsetOf((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.IsSupersetOf(SCG.IEnumerable<T> other)
		{
			return IsSupersetOf((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.Overlaps(SCG.IEnumerable<T> other)
		{
			return Overlaps((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		bool SCG.ISet<T>.SetEquals(SCG.IEnumerable<T> other)
		{
			return SetEquals((SC.IEnumerable)other);
		}

		/// <inheritdoc />
		SCG.IEnumerator<T> SCG.IEnumerable<T>.GetEnumerator()
		{
			return GetEnumerator();
		}
#endif
		#endregion
	}
}
