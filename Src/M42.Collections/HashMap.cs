﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A hash map.
	/// </summary>
	/// <typeparam name="TKey">The type of keys in the hash map.</typeparam>
	/// <typeparam name="TValue">The type of values in the hash map.</typeparam>
	public partial class HashMap<TKey, TValue> : IMutableKeyedCollection<TKey, TValue>
	{
		/// <summary>
		/// The underlying set.
		/// </summary>
		private readonly HashSet<Association<TKey, TValue>> innerSet;

		/// <inheritdoc />
		public int Count
		{
			get { return this.innerSet.Count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return Count == 0; }
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get { return this.innerSet.IsFrozen; }
		}

		private readonly HashMap<TKey, TValue>.KeyCollection keyCollection;
		/// <inheritdoc />
		public ICollection<TKey> Keys
		{
			get { return this.keyCollection; }
		}

		private readonly HashMap<TKey, TValue>.ValueCollection valueCollection;
		/// <inheritdoc />
		public ICollection<TValue> Values
		{
			get { return this.valueCollection; }
		}

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get
			{
				Association<TKey,TValue> association;
				if (!this.innerSet.TryGetMember(Association.Create(key, default(TValue)), out association))
					throw new SCG.KeyNotFoundException();
				return association.Value;
			}
			set
			{
				this.innerSet.AddOrUpdate(Association.Create(key, value));
			}
		}

		/// <inheritdoc />
		TValue IKeyedCollection<TKey, TValue>.this[object key]
		{
			get
			{
				if (!(key is TKey))
					throw new SCG.KeyNotFoundException();
				return this[(TKey)key];
			}
		}

		/// <summary>
		/// Gets the comparer used to compare the keys in this hash map.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{TKey}"/>.</value>
		public SCG.IEqualityComparer<TKey> KeyComparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<TKey>>() != null);
				#endregion
				return ((AssociationComparer)this.innerSet.Comparer).KeyComparer;
			}
		}

		private readonly SCG.IEqualityComparer<TValue> valueComparer;
		/// <summary>
		/// Gets the comparer used to compare the values in this hash map.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{TValue}"/>.</value>
		public SCG.IEqualityComparer<TValue> ValueComparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<TValue>>() != null);
				#endregion
				return this.valueComparer;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		public HashMap()
			: this((SCG.IEqualityComparer<TKey>)null, (SCG.IEqualityComparer<TValue>)null)
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="keyComparer">The comparer used to compare keys;
		/// or <see langword="null"/> to use the default key comparer.</param>
		public HashMap(SCG.IEqualityComparer<TKey> keyComparer)
			: this(keyComparer, null)
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="keyComparer">The comparer used to compare keys;
		/// or <see langword="null"/> to use the default key comparer.</param>
		/// <param name="valueComparer">The comparer used to compare values;
		/// or <see langword="null"/> to use the default value comparer.</param>
		public HashMap(SCG.IEqualityComparer<TKey> keyComparer, SCG.IEqualityComparer<TValue> valueComparer)
		{
			keyComparer = keyComparer ?? SCG.EqualityComparer<TKey>.Default;
			valueComparer = valueComparer ?? SCG.EqualityComparer<TValue>.Default;

			this.innerSet = new HashSet<Association<TKey, TValue>>(new AssociationComparer(keyComparer));
			this.valueComparer = valueComparer;
			this.keyCollection = new HashMap<TKey, TValue>.KeyCollection(this, keyComparer);
			this.valueCollection = new HashMap<TKey, TValue>.ValueCollection(this, valueComparer);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="elements">The key-value pairs to add to this hash map.</param>
		public HashMap(SCG.IEnumerable<IAssociation<TKey, TValue>> elements)
			: this(elements, null, null)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="elements">The key-value pairs to add to this hash map.</param>
		/// <param name="keyComparer">The comparer used to compare keys;
		/// or <see langword="null"/> to use the default key comparer.</param>
		public HashMap(SCG.IEnumerable<IAssociation<TKey, TValue>> elements, SCG.IEqualityComparer<TKey> keyComparer)
			: this(elements, keyComparer, null)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="HashMap{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="elements">The key-value pairs to add to this hash map.</param>
		/// <param name="keyComparer">The comparer used to compare keys;
		/// or <see langword="null"/> to use the default key comparer.</param>
		/// <param name="valueComparer">The comparer used to compare values;
		/// or <see langword="null"/> to use the default value comparer.</param>
		public HashMap(SCG.IEnumerable<IAssociation<TKey, TValue>> elements, SCG.IEqualityComparer<TKey> keyComparer, SCG.IEqualityComparer<TValue> valueComparer)
			: this(keyComparer, valueComparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
			foreach (var kv in elements)
			{
				Add(kv.Key, kv.Value);
			}
		}
		#endregion

		/// <inheritdoc />
		public bool Add(TKey key, TValue value)
		{
			return this.innerSet.Add(Association.Create(key, value));
		}

		/// <inheritdoc />
		public bool RemoveKey(object key)
		{
			if (!(key is TKey))
				return false;
			return this.innerSet.Remove(Association.Create((TKey)key, default(TValue)));
		}

		/// <summary>
		/// Attempts to get a value from the <see cref="HashMap{TKey, TValue}"/>.
		/// </summary>
		/// <param name="key">The key to look for.</param>
		/// <param name="value">The value that corresponds to the key.</param>
		/// <returns><see langword="true"/> when the key was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetValue(object key, out TValue value)
		{
			if (!(key is TKey))
			{
				value = default(TValue);
				return false;
			}
			Association<TKey, TValue> result;
			if (!this.innerSet.TryGetMember(Association.Create((TKey)key, default(TValue)), out result))
			{
				value = default(TValue);
				return false;
			}
			else
			{
				Contract.Assume(result != null);
				value = result.Value;
				return true;
			}
		}

		/// <inheritdoc />
		TValue IKeyedCollection<TKey, TValue>.GetValue(object key, out bool isPresent)
		{
			// CONTRACT: Inherited from IKeyedCollection<TKey, TValue>
			TValue result;
			isPresent = TryGetValue(key, out result);
			return result;
		}

		/// <inheritdoc />
		public TValue GetValue(object key)
		{
			// CONTRACT: Inherited from IKeyedCollection<TKey, TValue>
			TValue result;
			TryGetValue(key, out result);
			return result;
		}

		/// <inheritdoc />
		public bool ContainsKey(object key)
		{
			if (!(key is TKey))
				return false;
			return this.innerSet.Contains(Association.Create((TKey)key, default(TValue)));
		}

		/// <inheritdoc />
		IAssociation<TKey, TValue> ICollection<IAssociation<TKey, TValue>>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			var set = (ISet<Association<TKey, TValue>>)this.innerSet;
			var result = set.GetMember(value, out isPresent);
			return isPresent ? result : default(IAssociation<TKey, TValue>);
		}

		/// <inheritdoc />
		IAssociation<TKey, TValue> ICollection<IAssociation<TKey, TValue>>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return ((ICollection<IAssociation<TKey, TValue>>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<Association<TKey, TValue>>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<Association<TKey, TValue>>)this).GetMember(value);
		}

		/// <inheritdoc />
		bool ICollection.Contains(object value)
		{
			return this.innerSet.Contains(value);
		}

		/// <inheritdoc />
		public virtual bool IsValidKey(object key)
		{
			return key is TKey;
		}

		/// <inheritdoc />
		public virtual bool IsValidValue(object value)
		{
			return value == null
				|| value is TValue;
		}

		/// <inheritdoc />
		bool ICollection.IsValidMember(object value)
		{
			return value is Association<TKey, TValue>
				&& IsValidKey(((IAssociation<TKey, TValue>)value).Key)
				&& IsValidValue(((IAssociation<TKey, TValue>)value).Value);
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection.
		/// </summary>
		/// <returns>A <see cref="SCG.IEnumerator{T}"/> that can be used
		/// to iterate through the collection.</returns>
		public SCG.IEnumerator<Association<TKey, TValue>> GetEnumerator()
		{
			return this.innerSet.GetEnumerator();
		}

		/// <inheritdoc />
		SCG.IEnumerator<IAssociation<TKey, TValue>> SCG.IEnumerable<IAssociation<TKey, TValue>>.GetEnumerator()
		{
			return this.innerSet.Cast<IAssociation<TKey, TValue>>().GetEnumerator();
		}

		/// <inheritdoc />
		SC.IEnumerator SC.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <inheritdoc />
		bool IMutableCollection<IAssociation<TKey, TValue>>.Add(IAssociation<TKey, TValue> value)
		{
			return this.innerSet.Add(Association.Create(value.Key, value.Value));
		}

		/// <inheritdoc />
		bool IMutableCollection<IAssociation<TKey, TValue>>.Remove(object value)
		{
			IAssociation<object, object> ass = value as IAssociation<object, object>;
			if (ass == null)
				return false;
			Association<TKey, TValue> result;
			if (!this.innerSet.TryGetMember(ass, out result))
				return false;
			if (Object.ReferenceEquals(result, ass.Value))
				return false;
			return this.innerSet.Remove(ass);
		}

		/// <inheritdoc />
		public void Clear()
		{
			this.innerSet.Clear();
		}
	}
}
