﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;
using SL = System.Linq;

namespace M42.Collections
{
	/// <summary>
	/// Provides methods for working with <see cref="SCG.IEnumerable{T}"/> sequences.
	/// </summary>
	public static partial class EnumerableExt
	{
		/// <summary>
		/// Associates an enumerable collection of keys
		/// with an enumerable collection of values.
		/// </summary>
		/// <typeparam name="TKey">The type of keys.</typeparam>
		/// <typeparam name="TValue">The type of values.</typeparam>
		/// <param name="keys">An enumerable collection of keys.</param>
		/// <param name="values">An enumerable collection of values.</param>
		/// <returns>An enumerable collection of key-value associations.</returns>
		/// <remarks>
		/// This method associates keys with values until there are either no
		/// more keys or no more values.
		/// </remarks>
		public static SCG.IEnumerable<Association<TKey, TValue>> Associate<TKey, TValue>(this SCG.IEnumerable<TKey> keys, SCG.IEnumerable<TValue> values)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(keys != null);
			Contract.Requires<ArgumentNullException>(values != null);
			Contract.Ensures(Contract.Result<SCG.IEnumerable<Association<TKey, TValue>>>() != null);
			#endregion
			return SL.Enumerable.Zip(keys, values, (k, v) => Association.Create(k, v));
		}

		/// <summary>
		/// Creates an array with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <returns>An array with all elements in <paramref name="source"/>.</returns>
		/// <remarks>
		/// Any elements in <paramref name="source"/> are copied to the new array.
		/// When <paramref name="source"/> is empty, <see cref="Array{TSource}.Empty"/> is returned.
		/// </remarks>
		public static Array<TSource> ToM42Array<TSource>(this SCG.IEnumerable<TSource> source)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Ensures(Contract.Result<Array<TSource>>() != null);
			#endregion
			var innerArray = SL.Enumerable.ToArray(source);
			if (innerArray.Length > 0)
			{
				var result = new Array<TSource>(innerArray);
				// We throw away all references to the inner array,
				// so we can be sure the array is safe.
				result.MakeSafe();
				return result;
			}
			else
				return Array<TSource>.Empty;
		}

		/// <summary>
		/// Creates a dictionary with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <typeparam name="TValue">The type of values in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="valueSelector">A function that returns a value for a given element.</param>
		/// <param name="comparer">The comparer to use when comparing keys.</param>
		/// <returns>A dictionary with for all elements the keys and values provided by the selector functions.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="HashMap{TKey, TValue}"/> instance.
		/// </remarks>
		public static HashMap<TKey, TValue> ToM42Dictionary<TSource, TKey, TValue>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			Func<TSource, TValue> valueSelector,
			SCG.IEqualityComparer<TKey> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(valueSelector != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Ensures(Contract.Result<HashMap<TKey, TValue>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<HashMap<TKey, TValue>>(), source));
			#endregion
			var dictionary = new HashMap<TKey, TValue>(comparer);
			foreach (var element in source)
			{
				var key = keySelector(element);
				var value = valueSelector(element);
				dictionary.Add(key, value);
			}
			return dictionary;
		}

		#region ToM42Dictionary() Overloads
		/// <summary>
		/// Creates a dictionary with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <typeparam name="TValue">The type of values in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="valueSelector">A function that returns a value for a given element.</param>
		/// <returns>A dictionary with for all elements the keys and values provided by the selector functions.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="HashMap{TKey, TValue}"/> instance.
		/// </remarks>
		public static HashMap<TKey, TValue> ToM42Dictionary<TSource, TKey, TValue>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			Func<TSource, TValue> valueSelector)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(valueSelector != null);
			Contract.Ensures(Contract.Result<HashMap<TKey, TValue>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<HashMap<TKey, TValue>>(), source));
			#endregion
			return ToM42Dictionary(source, keySelector, valueSelector, SCG.EqualityComparer<TKey>.Default);
		}

		/// <summary>
		/// Creates a dictionary with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="comparer">The comparer to use when comparing keys.</param>
		/// <returns>A dictionary with for all elements the keys provided by the selector function
		/// and the element as the value.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="HashMap{TKey, TValue}"/> instance.
		/// </remarks>
		public static HashMap<TKey, TSource> ToM42Dictionary<TSource, TKey>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			SCG.IEqualityComparer<TKey> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Ensures(Contract.Result<HashMap<TKey, TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<HashMap<TKey, TSource>>(), source));
			#endregion
			return ToM42Dictionary(source, keySelector, element => element, comparer);
		}

		/// <summary>
		/// Creates a dictionary with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <returns>A dictionary with for all elements the keys provided by the selector function
		/// and the element as the value.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="HashMap{TKey, TValue}"/> instance.
		/// </remarks>
		public static HashMap<TKey, TSource> ToM42Dictionary<TSource, TKey>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Ensures(Contract.Result<HashMap<TKey, TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<HashMap<TKey, TSource>>(), source));
			#endregion
			return ToM42Dictionary(source, keySelector, SCG.EqualityComparer<TKey>.Default);
		}
		#endregion

		/// <summary>
		/// Creates a list with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <returns>A mutable list with all elements in <paramref name="source"/>.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="ArrayList{TSource}"/> instance.
		/// </remarks>
		public static ArrayList<TSource> ToM42List<TSource>(this SCG.IEnumerable<TSource> source)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Ensures(Contract.Result<ArrayList<TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<ArrayList<TSource>>(), source));
			#endregion
			return new ArrayList<TSource>(source);
		}

		/// <summary>
		/// Creates a set with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <returns>A mutable set with all elements in <paramref name="source"/>.</returns>
		/// <remarks>
		/// <para>This method guarantees to return a new <see cref="IMutableSet{TSource}"/> instance.</para>
		/// <para>Duplicate elements in <paramref name="source"/> are ignored.</para>
		/// </remarks>
		public static IMutableSet<TSource> ToM42Set<TSource>(this SCG.IEnumerable<TSource> source)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Ensures(Contract.Result<IMutableSet<TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<IMutableSet<TSource>>(), source));
			#endregion
			return ToM42Set(source, SCG.EqualityComparer<TSource>.Default);
		}

		/// <summary>
		/// Creates a set with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="comparer">The equality comparer to use.</param>
		/// <returns>A mutable set with all elements in <paramref name="source"/>.</returns>
		/// <remarks>
		/// <para>This method guarantees to return a new <see cref="IMutableSet{TSource}"/> instance.</para>
		/// <para>Duplicate elements in <paramref name="source"/> are ignored.</para>
		/// </remarks>
		public static IMutableSet<TSource> ToM42Set<TSource>(this SCG.IEnumerable<TSource> source, SCG.IEqualityComparer<TSource> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Ensures(Contract.Result<IMutableSet<TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<IMutableSet<TSource>>(), source));
			#endregion
			var set = new HashSet<TSource>(comparer);
			set.UnionWith(source);
			return set;
		}

#if false
		/// <summary>
		/// Creates a lookup with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <typeparam name="TValue">The type of values in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="valueSelector">A function that returns a value for a given element.</param>
		/// <param name="comparer">The comparer to use when comparing keys.</param>
		/// <returns>A lookup with for all elements the keys and values provided by the selector functions.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="Lookup{TKey, TValue}"/> instance.
		/// </remarks>
		public static SL.Lookup<TKey, TValue> ToLookup<TSource, TKey, TValue>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			Func<TSource, TValue> valueSelector,
			SCG.IEqualityComparer<TKey> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(valueSelector != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Ensures(Contract.Result<SL.Lookup<TKey, TValue>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<SL.Lookup<TKey, TValue>>(), source));
			#endregion
			throw new NotImplementedException();
		}

		#region ToLookup() Overloads
		/// <summary>
		/// Creates a lookup with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <typeparam name="TValue">The type of values in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="valueSelector">A function that returns a value for a given element.</param>
		/// <returns>A lookup with for all elements the keys and values provided by the selector functions.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="Lookup{TKey, TValue}"/> instance.
		/// </remarks>
		public static SL.Lookup<TKey, TValue> ToLookup<TSource, TKey, TValue>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			Func<TSource, TValue> valueSelector)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(valueSelector != null);
			Contract.Ensures(Contract.Result<SL.Lookup<TKey, TValue>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<SL.Lookup<TKey, TValue>>(), source));
			#endregion
			return ToLookup(source, keySelector, valueSelector, SCG.EqualityComparer<TKey>.Default);
		}

		/// <summary>
		/// Creates a lookup with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <param name="comparer">The comparer to use when comparing keys.</param>
		/// <returns>A lookup with for all elements the keys provided by the selector function
		/// and the element as the value.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="Lookup{TKey, TValue}"/> instance.
		/// </remarks>
		public static SL.Lookup<TKey, TSource> ToLookup<TSource, TKey>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector,
			SCG.IEqualityComparer<TKey> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			Contract.Ensures(Contract.Result<SL.Lookup<TKey, TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<SL.Lookup<TKey, TSource>>(), source));
			#endregion
			return ToLookup(source, keySelector, element => element, comparer);
		}

		/// <summary>
		/// Creates a lookup with the elements from the enumerable collection.
		/// </summary>
		/// <typeparam name="TSource">The type of elements in the enumerable collection.</typeparam>
		/// <typeparam name="TKey">The type of keys in the resulting dictionary.</typeparam>
		/// <param name="source">The source enumerable collection.</param>
		/// <param name="keySelector">A function that returns a key for a given element.</param>
		/// <returns>A lookup with for all elements the keys provided by the selector function
		/// and the element as the value.</returns>
		/// <remarks>
		/// This method guarantees to return a new <see cref="Lookup{TKey, TValue}"/> instance.
		/// </remarks>
		public static SL.Lookup<TKey, TSource> ToLookup<TSource, TKey>(
			this SCG.IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(keySelector != null);
			Contract.Ensures(Contract.Result<SL.Lookup<TKey, TSource>>() != null);
			Contract.Ensures(!Object.ReferenceEquals(Contract.Result<SL.Lookup<TKey, TSource>>(), source));
			#endregion
			return ToLookup(source, keySelector, SCG.EqualityComparer<TKey>.Default);
		}
		#endregion
#endif
	}
}
