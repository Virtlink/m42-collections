﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace M42.Collections
{
	/// <summary>
	/// Provides methods for working with <see cref="IList{T}"/> objects.
	/// </summary>
	public static class List
	{
		/// <summary>
		/// Returns an empty, immutable list.
		/// </summary>
		/// <typeparam name="TResult">The type of elements in the list.</typeparam>
		/// <returns>An empty, immutable list.</returns>
		public static IList<TResult> Empty<TResult>()
		{
			return Array<TResult>.Empty;
		}
	}
}
