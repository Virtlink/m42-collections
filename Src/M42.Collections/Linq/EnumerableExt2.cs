﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using SC = System.Collections;
using SCG = System.Collections.Generic;
using SL = System.Linq;

namespace M42.Collections
{
	static partial class EnumerableExt
	{
		/// <summary>
		/// Returns whether the specified enumerable is empty.
		/// </summary>
		/// <param name="enumerable">The enumerable to check.</param>
		/// <returns><see langword="true"/> when <paramref name="enumerable"/>
		/// is <see langword="null"/> or zero elements;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool IsEmpty<T>(this SCG.IEnumerable<T> enumerable)
		{
			// If it is null, it is empty.
			if (enumerable == null)
				return true;
			// If it is a ICollection<T>, it has a property for this.
			var collection = enumerable as ICollection<T>;
			if (collection != null)
				return collection.IsEmpty;
#if false
			// If it is a SCG.IReadOnlyCollection<T>, we check the Count property.
			var scgROCollection = enumerable as SCG.IReadOnlyCollection<T>;
			if (scgROCollection != null)
				return scgCollection.Count == 0;
#endif
			// If it is a SCG.ICollection<T>, we check the Count property.
			var scgCollection = enumerable as SCG.ICollection<T>;
			if (scgCollection != null)
				return scgCollection.Count == 0;
			// If it is a SC.ICollection, we check the Count property.
			var scCollection = enumerable as SC.ICollection;
			if (scCollection != null)
				return scCollection.Count == 0;
			// If it is anything else, we check whether the sequence contains any elements.
			return SL.Enumerable.Any(enumerable);
		}

		/// <summary>
		/// Returns an enumerable with a single value when that value is
		/// not <see langword="null"/>, or an empty enumerable.
		/// </summary>
		/// <typeparam name="T">The type of value.</typeparam>
		/// <param name="value">The single value, which may be <see langword="null"/>.</param>
		/// <returns>An enumerable with one or no elements.</returns>
		public static SCG.IEnumerable<T> Of<T>(T value)
		{
			if (Object.ReferenceEquals(value, null))
				yield break;
			yield return value;
		}
	}
}
