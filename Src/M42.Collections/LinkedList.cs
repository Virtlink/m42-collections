﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A linked list.
	/// </summary>
	/// <typeparam name="T">The type of elements in the linked list.</typeparam>
	public partial class LinkedList<T> : IMutableList<T>
	{
		// The start and end are dummy nodes. They are never returned
		// and do not count towards the number of elements in the list.
		private readonly Node head;
		private readonly Node tail;

		/// <summary>
		/// Gets the first node of the linked list.
		/// </summary>
		/// <value>The first <see cref="Node"/>;
		/// or <see langword="null"/> when the list is empty.</value>
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public Node First
		{
			get { return this.head.Next; }
		}

		/// <summary>
		/// Gets the last node of the linked list.
		/// </summary>
		/// <value>The last <see cref="Node"/>;
		/// or <see langword="null"/> when the list is empty.</value>
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public Node Last
		{
			get { return this.tail.Previous; }
		}

		private int count = 0;
		/// <inheritdoc />
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.count;
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return Count == 0;
			}
		}

		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return false;
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IMutableArray<T>
				return GetNodeAt(index).Value;
			}
			set
			{
				// CONTRACT: Inherited from IMutableArray<T>
				GetNodeAt(index).Value = value;
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		T IList<T>.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this[index];
			}
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the comparer used to compare the values of elements in this
		/// collection.
		/// </summary>
		/// <value>An <see cref="IEqualityComparer"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="LinkedList{T}"/> class.
		/// </summary>
		public LinkedList()
			: this(SCG.EqualityComparer<T>.Default)
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="LinkedList{T}"/> class.
		/// </summary>
		/// <param name="comparer">The comparer used to compare values in
		/// this list.</param>
		public LinkedList(SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.comparer = comparer;
			this.head = new Node(this, default(T));
			this.tail = new Node(this, default(T));
			Node.Connect(this.head, this.tail);
		}
		#endregion

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			var node = Find(value);
			isPresent = node != null;
			return isPresent ? node.Value : default(T);
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <summary>
		/// Returns the first <see cref="Node"/> that has a value that is
		/// equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The first <see cref="Node"/> with the specified value;
		/// or <see langword="null"/> when not found.</returns>
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public Node Find(object value)
		{
			if (!(value is T))
				return null;
			return Find(n => this.comparer.Equals(n.Value, (T)value));
		}

		/// <summary>
		/// Returns the first <see cref="Node"/> for which the specified
		/// condition is true.
		/// </summary>
		/// <param name="condition">The condition to test.</param>
		/// <returns>The first <see cref="Node"/> that satisfies the condition;
		/// or <see langword="null"/> when not found.</returns>
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public Node Find(Func<Node, bool> condition)
		{
			Node match = null;
			var current = this.head.Next;
			while (current != null)
			{
				if (condition(current))
				{
					match = current;
					break;
				}
				current = current.Next;
			}
			return match;
		}

		/// <summary>
		/// Returns the last <see cref="Node"/> that has a value that is
		/// equal to the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <returns>The last <see cref="Node"/> with the specified value;
		/// or <see langword="null"/> when not found.</returns>
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public Node FindLast(object value)
		{
			if (!(value is T))
				return null;
			return FindLast(n => this.comparer.Equals(n.Value, (T)value));
		}

		/// <summary>
		/// Returns the last <see cref="Node"/> for which the specified
		/// condition is true.
		/// </summary>
		/// <param name="condition">The condition to test.</param>
		/// <returns>The last <see cref="Node"/> that satisfies the condition;
		/// or <see langword="null"/> when not found.</returns>
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public Node FindLast(Func<Node, bool> condition)
		{
			Node match = null;
			var current = this.tail.Previous;
			while (current != null)
			{
				if (condition(current))
				{
					match = current;
					break;
				}
				current = current.Previous;
			}
			return match;
		}

		/// <summary>
		/// Returns the node at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index to look for.</param>
		/// <returns>The <see cref="Node"/> at the specified index.</returns>
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public Node GetNodeAt(int index)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(index >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(index < Count);
			#endregion
			Node current;
			int currentIndex;
			if (this.count - index > index)
			{
				// Search from first to last
				current = this.head.Next;
				currentIndex = 0;
				while (current != null && currentIndex < index)
				{
					current = current.Next;
					currentIndex++;
				}
			}
			else
			{
				// Search from last to first
				current = this.tail.Previous;
				currentIndex = this.count - 1;
				while (current != null && currentIndex > index)
				{
					current = current.Previous;
					currentIndex--;
				}
			}
			Contract.Assume(currentIndex == index);
			return current;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public bool Add(T value)
		{
			// CONTRACT: Inherited from IMutableCollection<T>
			this.tail.AddBefore(value);
			return true;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public bool Remove(object value)
		{
			// CONTRACT: Inherited from IMutableCollection<T>
			var node = Find(value);
			if (node == null)
				return false;
			node.Remove();
			return true;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(1) operation.
		/// </remarks>
		public void Clear()
		{
			// CONTRACT: Inherited from IMutableCollection<T>
			// Connect the dummy head directly to the dummy tail
			Node.Connect(this.head, this.tail);
			this.count = 0;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public bool Insert(int index, T value)
		{
			// CONTRACT: Inherited from IMutableList<T>
			Node node;
			if (index == 0)
				node = this.head.Next;
			else if (index == this.count)
				node = this.tail;
			else
				node = GetNodeAt(index);
			Contract.Assume(node != null);
			node.AddBefore(value);
			return true;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public bool RemoveAt(int index)
		{
			// CONTRACT: Inherited from IMutableList<T>
			Node node;
			if (index == 0)
				node = this.head.Next;
			else if (index == this.count)
				node = this.tail.Previous;
			else
				node = GetNodeAt(index);
			node.Remove();
			return true;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			if (!(value is T))
				return null;

			var current = this.head.Next;
			int currentIndex = 0;
			int? index = null;
			while (current != null)
			{
				if (this.comparer.Equals(current.Value, (T)value))
				{
					index = currentIndex;
					break;
				}
				current = current.Next;
				currentIndex++;
			}
			return index;
		}

		/// <inheritdoc />
		/// <remarks>
		/// This is an O(n) operation, where n is the number of elements in
		/// the collection.
		/// </remarks>
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return Find(value) != null;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			var current = this.head.Next;
			while (current != null)
			{
				yield return current.Value;
				current = current.Next;
			}
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <summary>
		/// Creates a new <see cref="Node"/>.
		/// </summary>
		/// <param name="value">The value of the node.</param>
		/// <returns>The created node.</returns>
		private Node CreateNode(T value)
		{
			#region Contract
			Contract.Ensures(Contract.Result<Node>() != null);
			#endregion
			return new Node(this, value);
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.comparer != null);
			Contract.Invariant(this.head != null);
			Contract.Invariant(this.tail != null);
			Contract.Invariant(this.count >= 0);
		}
		#endregion
	}
}
