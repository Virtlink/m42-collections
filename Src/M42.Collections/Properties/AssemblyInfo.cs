﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("M42.Collections")]
[assembly: AssemblyDescription("Specialized collections with more functionality, features and ease-of-use than the built-in .NET collection classes.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("16tonweight")]
[assembly: AssemblyProduct("M42.Collections")]
[assembly: AssemblyCopyright("Copyright © - 2012-2013 Daniel Pelsmaeker")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]

[assembly: AssemblyVersion("0.6.*")]

[assembly: InternalsVisibleTo("M42.Collections.Tests")]