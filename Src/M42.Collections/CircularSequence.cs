﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using SC = System.Collections;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// A circular sequence of elements.
	/// </summary>
	/// <typeparam name="T">The type of elements in the seuence.</typeparam>
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public class CircularSequence<T> : IMutableSequence<T>, IList<T>
	{
		/// <summary>
		/// The minimum capacity.
		/// </summary>
		/// <remarks>
		/// This must be a power of two.
		/// </remarks>
		protected const int MinimumCapacity = 8;

		/// <summary>
		/// The circular queue's data buffer.
		/// </summary>
		private T[] innerArray;
		/// <summary>
		/// The mask used to wrap the circular queue.
		/// </summary>
		private int mask;

		/// <summary>
		/// The zero-based index of the first element in the circular queue.
		/// </summary>
		private int head;

		/// <summary>
		/// Gets the capacity of this circular queue.
		/// </summary>
		/// <value>The capacity of the queue.</value>
		/// <remarks>
		/// The capacity is always a power of two.
		/// </remarks>
		public int Capacity
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<int>() >= MinimumCapacity);
				#endregion
				return this.innerArray.Length;
			}
		}

		private int count;
		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.count;
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from IMutableSequence<T>
				return this.count == 0;
			}
		}

		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return false;
			}
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this.innerArray[(this.head + index) & mask];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList
				return this[index];
			}
		}

		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the equality comparer used to compare elements in this list.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CircularSequence{T}"/> class.
		/// </summary>
		public CircularSequence()
			: this(MinimumCapacity)
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularSequence{T}"/> class.
		/// </summary>
		/// <param name="comparer">The equality comparer to use.</param>
		public CircularSequence(SCG.IEqualityComparer<T> comparer)
			: this(MinimumCapacity, comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularSequence{T}"/> class.
		/// </summary>
		/// <param name="minimumCapacity">The queue's minimum capacity.</param>
		public CircularSequence(int minimumCapacity)
			: this(minimumCapacity, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minimumCapacity >= 0);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularSequence{T}"/> class.
		/// </summary>
		/// <param name="minimumCapacity">The queue's minimum capacity.</param>
		/// <param name="comparer">The equality comparer to use.</param>
		public CircularSequence(int minimumCapacity, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(minimumCapacity >= 0);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.comparer = comparer;
			this.innerArray = new T[0];
			EnsureCapacity(minimumCapacity);
		}
		#endregion

		/// <summary>
		/// Ensures that the circular array has at least the specified capacity.
		/// </summary>
		/// <param name="minimumCapacity">The minimum capacity.</param>
		protected void EnsureCapacity(int minimumCapacity)
		{
			minimumCapacity = Math.Max(MinimumCapacity, minimumCapacity);
			int capacity = this.innerArray.Length;
			if (capacity >= minimumCapacity)
				return;

			// Double the capacity until the minimum capacity is reached.
			capacity = Math.Max(MinimumCapacity, capacity);
			while (capacity < minimumCapacity)
				capacity *= 2;

			T[] newInnerArray = new T[capacity];
			if (head + this.count >= this.innerArray.Length)
			{
				int split = this.innerArray.Length - head;
				// Copy everything from head to the end of the array.
				System.Array.Copy(this.innerArray, head, newInnerArray, 0, split);
				// Copy everything from the start of the array to the tail.
				System.Array.Copy(this.innerArray, 0, newInnerArray, split, this.count - split);
			}
			else
				// Copy everything starting from head.
				System.Array.Copy(this.innerArray, head, newInnerArray, 0, this.count);

			this.innerArray = newInnerArray;
			this.mask = capacity - 1;
			this.head = 0;
		}

		/// <inheritdoc />
		public virtual bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			return value is T;
		}

		/// <summary>
		/// Adds a value to the end of the sequence.
		/// </summary>
		/// <param name="value">The value to add to the sequence.</param>
		public bool Add(T value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			return AddLast(value);
		}

		/// <summary>
		/// Removes and returns the first element in the sequence.
		/// </summary>
		/// <returns>The first element in the sequence.</returns>
		public T Dequeue()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			return TakeFirst();
		}

		/// <summary>
		/// Removes and returns the last element in the sequence.
		/// </summary>
		/// <returns>The last element in the sequence.</returns>
		public T Pop()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			return TakeLast();
		}

		/// <summary>
		/// Removes all elements from the circular sequence.
		/// </summary>
		/// <exception cref="CollectionFrozenException">
		/// The circular sequence is frozen.
		/// </exception>
		public void Clear()
		{
			#region Contract
			Contract.Requires<CollectionFrozenException>(!((ICollection)this).IsFrozen);
			#endregion
			for (int i = 0; i < this.innerArray.Length; i++)
				this.innerArray[i] = default(T);

			this.count = 0;
			this.head = 0;
		}

		/// <summary>
		/// Adds a value to the start of the sequence.
		/// </summary>
		/// <param name="value">The value to add to the start of the sequence.</param>
		protected bool AddFirst(T value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			if ((this.count + 1) > this.innerArray.Length)
				EnsureCapacity(this.count + 1);
			this.head = (this.head - 1) & mask;
			int index = this.head;
			this.innerArray[index] = value;
			this.count++;
			return true;
		}

		/// <summary>
		/// Adds a value to the end of the sequence.
		/// </summary>
		/// <param name="value">The value to add to the end of the sequence.</param>
		protected bool AddLast(T value)
		{
			#region Contract
			Contract.Requires<ArgumentException>(IsValidMember(value));
			#endregion
			if ((this.count + 1) > this.innerArray.Length)
				EnsureCapacity(this.count + 1);
			int index = (this.head + this.count) & mask;
			this.innerArray[index] = value;
			this.count++;
			return true;
		}

		/// <summary>
		/// Removes and returns the first element from the sequence.
		/// </summary>
		/// <returns>The first element in the sequence.</returns>
		protected T TakeFirst()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			int index = this.head;
			var value = this.innerArray[index];
			this.innerArray[index] = default(T);
			this.head = (this.head + 1) & mask;
			this.count--;
			return value;
		}

		/// <summary>
		/// Removes and returns the last element from the sequence.
		/// </summary>
		/// <returns>The last element in the sequence.</returns>
		protected T TakeLast()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			int index = (this.head + (this.count - 1)) & mask;
			var value = this.innerArray[index];
			this.innerArray[index] = default(T);
			this.count--;
			return value;
		}

		/// <summary>
		/// Returns the first element from the sequence.
		/// </summary>
		/// <returns>The first element in the sequence.</returns>
		protected T PeekFirst()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			int index = this.head;
			return this.innerArray[index];
		}

		/// <summary>
		/// Returns the last element from the sequence.
		/// </summary>
		/// <returns>The last element in the sequence.</returns>
		protected T PeekLast()
		{
			#region Contract
			Contract.Requires<SequenceEmptyException>(!IsEmpty);
			#endregion
			int index = (this.head + (this.count - 1)) & mask;
			return this.innerArray[index];
		}

		/// <summary>
		/// Adds a value to the sequence.
		/// </summary>
		/// <param name="element">The value to add to the sequence.</param>
		/// <returns><see langword="true"/> when the value was successfully
		/// added to the sequence; otherwise, <see langword="false"/>.</returns>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		protected virtual bool AddElement(T element)
		{
			return AddFirst(element);
		}

		/// <summary>
		/// Takes and returns the element from the sequence.
		/// </summary>
		/// <returns>The next element in the sequence.</returns>
		/// <exception cref="SequenceEmptyException">
		/// The sequence is empty.
		/// </exception>
		/// <exception cref="CollectionFrozenException">
		/// The collection is frozen.
		/// </exception>
		protected virtual T TakeElement()
		{
			return TakeFirst();
		}

		/// <summary>
		/// Returns the element that would be returned by the next call to
		/// <see cref="TakeElement"/>, without actually removing the element.
		/// </summary>
		/// <returns>The element that would be returned by the next call to
		/// <see cref="TakeElement"/>.</returns>
		/// <exception cref="SequenceEmptyException">
		/// The sequence is empty.
		/// </exception>
		protected virtual T PeekElement()
		{
			return PeekFirst();
		}

		/// <inheritdoc />
		bool IMutableSequence<T>.Add(T value)
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			return AddElement(value);
		}

		/// <inheritdoc />
		T IMutableSequence<T>.Take()
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			return TakeElement();
		}

		/// <inheritdoc />
		public T Peek()
		{
			// CONTRACT: Inherited from IMutableSequence<T>
			return PeekElement();
		}

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			int? index = IndexOf(value);
			isPresent = (index != null);
			if (index == null)
				return default(T);
			
			return this[(int)index];
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			bool isPresent;
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return IndexOf(value).HasValue;
		}

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			if (!(value is T))
				return null;

			int tail = head + this.count;
			for (int i = head; i < tail; i++)
			{
				var element = this.innerArray[i & mask];
				if (this.comparer.Equals((T)value, element))
					return i - head;
			}
			return null;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from SCG.IEnumerator<T>
			int tail = head + this.count;
			for (int i = head; i < tail; i++)
			{
				yield return this.innerArray[i & mask];
			}
		}

		/// <inheritdoc />
		SC.IEnumerator SC.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from SC.IEnumerator
			return GetEnumerator();
		}

		/// <summary>
		/// Determines whether a value is a power of two, or zero.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <returns><see langword="true"/> when <paramref name="value"/> is a power of two,
		/// or zero; otherwise, <see langword="false"/>.</returns>
		[Pure]
		private static bool IsPowerOfTwo(int value)
		{
			return (value & (value - 1)) == 0;
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(IsPowerOfTwo(this.innerArray.Length));
		}
		#endregion
	}
}
