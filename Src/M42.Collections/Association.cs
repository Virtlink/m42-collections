﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;

namespace M42.Collections
{
	/// <summary>
	/// An association between a key and a value.
	/// </summary>
	/// <typeparam name="TKey">The key.</typeparam>
	/// <typeparam name="TValue">The value.</typeparam>
	public struct Association<TKey, TValue> : IAssociation<TKey, TValue>, IEquatable<Association<TKey, TValue>>
	{
		private readonly TKey key;
		/// <inheritdoc />
		public TKey Key
		{
			get
			{
				// CONTRACT: Inherited from IAssociation<TKey, TValue>
				return this.key;
			}
		}

		private readonly TValue value;
		/// <inheritdoc />
		public TValue Value
		{
			get
			{
				// CONTRACT: Inherited from IAssociation<TKey, TValue>
				return this.value;
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="Association{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public Association(TKey key, TValue value)
		{
			this.key = key;
			this.value = value;
		}
		#endregion

		#region Equality
		/// <inheritdoc />
		public bool Equals(Association<TKey, TValue> other)
		{
			return Object.Equals(this.key, other.key)
				&& Object.Equals(this.value, other.value);
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			return this.key.GetHashCode();
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (!(obj is Association<TKey, TValue>))
				return false;
			return Equals((Association<TKey, TValue>)obj);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified
		/// <see cref="Association{TKey, TValue}"/> objects are equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator ==(Association<TKey, TValue> left, Association<TKey, TValue> right)
		{
			return Object.Equals(left, right);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified
		/// <see cref="Association{TKey, TValue}"/> objects are not equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are not equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator !=(Association<TKey, TValue> left, Association<TKey, TValue> right)
		{
			return !(left == right);
		}
		#endregion

		/// <inheritdoc />
		public override string ToString()
		{
			return String.Format("[{0}, {1}]", this.key, this.value);
		}
	}
}
