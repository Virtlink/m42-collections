﻿using System;
using System.Collections.Generic;
using System.Text;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// An enumerable sequence of elements.
	/// </summary>
	/// <typeparam name="T">The type of elements in the sequence.</typeparam>
	public interface IEnumerable<out T> : SCG.IEnumerable<T>
	{
		/// <summary>
		/// Returns an enumerator for iterating through the sequence.
		/// </summary>
		/// <returns>An <see cref="IEnumerator{T}"/> that can iterate through
		/// the sequence.</returns>
		IEnumerator<T> GetEnumerator();
	}
}
