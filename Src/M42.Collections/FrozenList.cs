﻿#region Copyright and License
// Copyright 2012 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using M42.Collections.Debugging;
using System;
using SC = System.Collections;
using SCG = System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading.Tasks;

namespace M42.Collections
{
	/// <summary>
	/// A frozen list.
	/// </summary>
	/// <remarks>
	/// Frozen collections are guaranteed to never change. Therefore, many of
	/// the operations, such as enumerating the collection, can be done with a
	/// slight increase in performance.
	/// </remarks>
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(EnumerableDebugVizualizer<>))]
	public sealed class FrozenList<T> : IList<T>
	{
		/// <summary>
		/// The inner array.
		/// </summary>
		private readonly Array<T> innerArray;

		/// <summary>
		/// The number of valid elements in the array.
		/// </summary>
		private readonly int count;

		/// <inheritdoc />
		public int Count
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return this.count;
			}
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return Count == 0;
			}
		}

		/// <inheritdoc />
		public bool IsFrozen
		{
			get
			{
				// CONTRACT: Inherited from ICollection<T>
				return true;
			}
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				// CONTRACT: Inherited from IList<T>
				return this.innerArray[index];
			}
		}

		#region Comparisons
		private readonly SCG.IEqualityComparer<T> comparer;
		/// <summary>
		/// Gets the equality comparer used to compare elements in this list.
		/// </summary>
		/// <value>An <see cref="SCG.IEqualityComparer{T}"/>.</value>
		public SCG.IEqualityComparer<T> Comparer
		{
			get
			{
				#region Contract
				Contract.Ensures(Contract.Result<SCG.IEqualityComparer<T>>() != null);
				#endregion
				return this.comparer;
			}
		}

		/// <summary>
		/// Compares two values.
		/// </summary>
		/// <param name="expected">The expected value.</param>
		/// <param name="value">The value to compare to.</param>
		/// <returns><see langword="true"/> when the values are equal;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		private bool AreEqual(object expected, T value)
		{
			return (expected == null || expected is T)
				&& this.Comparer.Equals((T)expected, value);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="FrozenList{T}"/> class.
		/// </summary>
		/// <param name="elements">The enumerable that returns the elements
		/// that are included in this frozen list.</param>
		/// <remarks>
		/// <para>This constructor copies the data in the list, and is
		/// therefore an O(n) operation, where n is the number of elements
		/// returned by the enumerable. The original collection
		/// is not frozen.</para>
		/// <para>To get a frozen list without the data copying, call the
		/// <see cref="IFreezable{TCollection, T}.Freeze"/> method on the list
		/// and use the returned <see cref="FrozenList{T}"/> object. However,
		/// this also freezes the original list.</para>
		/// </remarks>
		public FrozenList(SCG.IEnumerable<T> elements)
			: this(elements, SCG.EqualityComparer<T>.Default)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FrozenList{T}"/> class.
		/// </summary>
		/// <param name="elements">The enumerable that returns the elements
		/// that are included in this frozen list.</param>
		/// <param name="comparer">The equality comparer to use.</param>
		/// <remarks>
		/// <para>This constructor copies the data in the list, and is
		/// therefore an O(n) operation, where n is the number of elements
		/// returned by the enumerable. The original collection
		/// is not frozen.</para>
		/// <para>To get a frozen list without the data copying, call the
		/// <see cref="IFreezable{TCollection, T}.Freeze"/> method on the list
		/// and use the returned <see cref="FrozenList{T}"/> object. However,
		/// this also freezes the original list.</para>
		/// </remarks>
		public FrozenList(SCG.IEnumerable<T> elements, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.innerArray = elements.ToArray();
			this.count = this.innerArray.Count;
			this.comparer = comparer;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FrozenList{T}"/> class.
		/// </summary>
		/// <param name="count">The number of elements in the list.</param>
		/// <param name="innerArray">The inner array.</param>
		/// <param name="comparer">The equality comparer to use.</param>
		/// <remarks>
		/// <para>This constructor is only called by the
		/// <see cref="IFreezable{TCollection, T}.Freeze"/> methods of
		/// collections.</para>
		/// <para>The caller must ensure that <paramref name="innerArray"/>
		/// can never be mutated by also freezing the original
		/// collection.</para>
		/// <para>The difference between the frozen original collection and
		/// this collection is that this collection uses the guarantees
		/// provided by a frozen collection to slightly increase the
		/// performance of some operations.</para>
		/// </remarks>
		internal FrozenList(int count, Array<T> innerArray, SCG.IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentNullException>(innerArray != null);
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.count = count;
			this.innerArray = innerArray;
			this.comparer = comparer;
		}
		#endregion

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			// CONTRACT: Inherited from IList<T>
			for (int i = 0; i < this.count; i++)
			{
				if (AreEqual(value, this.innerArray[i]))
					return i;
			}
			return null;
		}

		/// <summary>
		/// Attempts to get an element from the collection that is equal to
		/// the specified value.
		/// </summary>
		/// <param name="value">The value to look for.</param>
		/// <param name="result">The value that was found; or the default
		/// of <typeparamref name="T"/> when the value was not found.</param>
		/// <returns><see langword="true"/> when the value was found;
		/// otherwise, <see langword="false"/>.</returns>
		[Pure]
		public bool TryGetMember(object value, out T result)
		{
			bool isPresent;
			result = ((ICollection<T>)this).GetMember(value, out isPresent);
			return isPresent;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value, out bool isPresent)
		{
			// CONTRACT: Inherited from ICollection<T>
			var index = ((IList<T>)this).IndexOf(value);
			isPresent = (index != null);
			T result = default(T);
			if (isPresent)
				result = this[(int)index];
			return result;
		}

		/// <inheritdoc />
		T ICollection<T>.GetMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return ((ICollection<T>)this).GetMember(value);
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return IndexOf(value) != null;
		}

		/// <inheritdoc />
		public bool IsValidMember(object value)
		{
			// CONTRACT: Inherited from ICollection<T>
			return value == null
				|| value is T;
		}

		/// <inheritdoc />
		public SCG.IEnumerator<T> GetEnumerator()
		{
			// CONTRACT: Inherited from IEnumerable<T>
			return ((SCG.IEnumerable<T>)this.innerArray).GetEnumerator();
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			// CONTRACT: Inherited from IEnumerable
			return this.innerArray.GetEnumerator();
		}
	}
}
