﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// Methods for working with associations.
	/// </summary>
	public static class Association
	{
		/// <summary>
		/// Creates an association between a key and a value.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <returns>The association.</returns>
		public static Association<TKey, TValue> Create<TKey, TValue>(TKey key, TValue value)
		{
			return new Association<TKey, TValue>(key, value);
		}

		/// <summary>
		/// Creates a new association from the specified key-value pair.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="keyValuePair">The key-value pair.</param>
		/// <returns>The association.</returns>
		public static Association<TKey, TValue> FromKeyValuePair<TKey, TValue>(SCG.KeyValuePair<TKey, TValue> keyValuePair)
		{
			return Create(keyValuePair.Key, keyValuePair.Value);
		}

		/// <summary>
		/// Creates a new key-value pair from the specified association.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="association">The association.</param>
		/// <returns>The key-value pair.</returns>
		public static SCG.KeyValuePair<TKey, TValue> ToKeyValuePair<TKey, TValue>(IAssociation<TKey, TValue> association)
		{
			return new SCG.KeyValuePair<TKey,TValue>(association.Key, association.Value);
		}

		/// <summary>
		/// Creates a new key-value pair from the specified association.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="association">The association.</param>
		/// <returns>The key-value pair.</returns>
		public static SCG.KeyValuePair<TKey, TValue> ToKeyValuePair<TKey, TValue>(Association<TKey, TValue> association)
		{
			return new SCG.KeyValuePair<TKey, TValue>(association.Key, association.Value);
		}
	}
}
