﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	/// <summary>
	/// Debugger vizualizer for enumerables.
	/// </summary>
	/// <typeparam name="T">The type of elements in the vizualizer.</typeparam>
	public sealed class EnumerableDebugVizualizer<T>
	{
		/// <summary>
		/// The wrapped enumerable.
		/// </summary>
		private readonly SCG.IEnumerable<T> enumerable;

		/// <summary>
		/// Gets an array of items in the enumerable.
		/// </summary>
		/// <value>An array of items.</value>
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				return (T[])this.enumerable.ToM42Array();
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance
		/// of the <see cref="EnumerableDebugVizualizer{T}"/> class.
		/// </summary>
		/// <param name="enumerable">The enumerable being visualized.</param>
		public EnumerableDebugVizualizer(SCG.IEnumerable<T> enumerable)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(enumerable != null);
			#endregion
			this.enumerable = enumerable;
		}
		#endregion

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.enumerable != null);
		}
		#endregion
	}
}
