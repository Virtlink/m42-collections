﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Diagnostics.Contracts;

namespace M42.Collections
{
	/// <summary>
	/// An exception that is thrown when
	/// </summary>
	public class SequenceEmptyException : Exception
	{
		#region Constructors
		/// <summary>
		/// The default error message.
		/// </summary>
		private const string DefaultMessage = "The sequence is empty.";

		/// <summary>
		/// Initializes a new instance of the <see cref="SequenceEmptyException"/> class.
		/// </summary>
		public SequenceEmptyException()
			: this(DefaultMessage, null)
		{
			#region Contract
			Contract.Ensures(InnerException == null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SequenceEmptyException"/> class.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		public SequenceEmptyException(string message)
			: this(message, null)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(message != null);
			Contract.Ensures(Message == message);
			Contract.Ensures(InnerException == null);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SequenceEmptyException"/> class.
		/// </summary>
		/// <param name="inner">The exception that is the cause of the current exception;
		/// or <see langword="null"/> if no inner exception is specified.</param>
		public SequenceEmptyException(Exception inner)
			: this(DefaultMessage, inner)
		{
			#region Contract
			Contract.Ensures(InnerException == inner);
			#endregion
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SequenceEmptyException"/> class.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception;
		/// or <see langword="null"/> if no inner exception is specified.</param>
		public SequenceEmptyException(string message, Exception inner)
			: base(message, inner)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(message != null);
			Contract.Ensures(Message == message);
			Contract.Ensures(InnerException == inner);
			#endregion
		}
		#endregion
	}
}
