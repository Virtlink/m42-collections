﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Diagnostics.Contracts;
using System.Linq;
using SCG = System.Collections.Generic;

namespace M42.Collections
{
	partial class HashMap<TKey, TValue>
	{
		/// <summary>
		/// A collection of values in a keyed collection.
		/// </summary>
		internal sealed class ValueCollection : ICollection<TValue>
		{
			private readonly IKeyedCollection<TKey, TValue> innerKeyedCollection;
			private readonly SCG.IEqualityComparer<TValue> valueComparer;

			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="ValueCollection"/> class.
			/// </summary>
			/// <param name="keyedCollection">The keyed collection.</param>
			/// <param name="valueComparer">The key comparer.</param>
			public ValueCollection(IKeyedCollection<TKey, TValue> keyedCollection, SCG.IEqualityComparer<TValue> valueComparer)
			{
				#region Contract
				Contract.Requires<ArgumentNullException>(keyedCollection != null);
				Contract.Requires<ArgumentNullException>(valueComparer != null);
				#endregion
				this.innerKeyedCollection = keyedCollection;
				this.valueComparer = valueComparer;
			}
			#endregion

			/// <inheritdoc />
			public int Count
			{
				get { return this.innerKeyedCollection.Count; }
			}

			/// <inheritdoc />
			public bool IsEmpty
			{
				get { return Count == 0; }
			}

			/// <inheritdoc />
			public bool IsFrozen
			{
				get { return this.innerKeyedCollection.IsFrozen; }
			}

			/// <inheritdoc />
			TValue ICollection<TValue>.GetMember(object value, out bool isPresent)
			{
				// CONTRACT: Inherited from ICollection<T>
				if (!(value is TValue))
				{
					isPresent = false;
					return default(TValue);
				}

				var all = GetAll();
				var result = default(TValue);
				isPresent = false;
				foreach (var member in all)
				{
					if (this.valueComparer.Equals(member, (TValue)value))
					{
						isPresent = true;
						result = member;
						break;
					}
				}
				return result;
			}

			/// <inheritdoc />
			TValue ICollection<TValue>.GetMember(object value)
			{
				// CONTRACT: Inherited from ICollection<T>
				bool isPresent;
				return ((ICollection<TValue>)this).GetMember(value, out isPresent);
			}

			/// <inheritdoc />
			object ICollection.GetMember(object value, out bool isPresent)
			{
				// CONTRACT: Inherited from ICollection
				return ((ICollection<TValue>)this).GetMember(value, out isPresent);
			}

			/// <inheritdoc />
			object ICollection.GetMember(object value)
			{
				// CONTRACT: Inherited from ICollection
				return ((ICollection<TValue>)this).GetMember(value);
			}

			/// <inheritdoc />
			public bool Contains(object value)
			{
				bool isPresent;
				((ICollection<TValue>)this).GetMember(value, out isPresent);
				return isPresent;
			}

			/// <inheritdoc />
			public bool IsValidMember(object value)
			{
				return this.innerKeyedCollection.IsValidMember(value);
			}

			/// <summary>
			/// Returns an enumerable with all keys.
			/// </summary>
			/// <returns>An enumerable with all the keys.</returns>
			private SCG.IEnumerable<TValue> GetAll()
			{
				return from kv in this.innerKeyedCollection
					   select kv.Value;
			}

			/// <inheritdoc />
			public SCG.IEnumerator<TValue> GetEnumerator()
			{
				return GetAll().GetEnumerator();
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator()
			{
				return GetEnumerator();
			}

			#region Invariants
			[ContractInvariantMethod]
			[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
				Justification = "Required for code contracts.")]
			private void ObjectInvariant()
			{
				Contract.Invariant(this.innerKeyedCollection != null);
				Contract.Invariant(this.valueComparer != null);
			}
			#endregion
		}
	}
}
