﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace M42.Collections
{
	/// <summary>
	/// A set that maintains insertion order.
	/// </summary>
	/// <typeparam name="T">The type of elements in the set.</typeparam>
	public class OrderedHashSet<T> : IMutableSet<T>, IMutableList<T>
	{
		private readonly HashSet<T> innerSet;
		private ArrayList<T> innerList;

		/// <inheritdoc />
		public int Count
		{
			get { return this.innerSet.Count; }
		}

		/// <inheritdoc />
		public bool IsEmpty
		{
			get { return Count == 0; }
		}

		/// <inheritdoc />
		bool ICollection.IsFrozen
		{
			get { return false; }
		}

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				return this.innerList[index];
			}
			set
			{
				int? existingIndex = IndexOf(value);
				if (existingIndex != null && existingIndex != index)
					throw new InvalidOperationException("An equal element is already present in the set.");
				bool success = this.innerSet.Remove(this.innerList[index]);
				Contract.Assume(success);
				success = this.innerSet.Add(value);
				Contract.Assume(success);
				this.innerList[index] = value;
			}
		}

		/// <inheritdoc />
		T IList<T>.this[int index]
		{
			get
			{
				return this[index];
			}
		}

		/// <inheritdoc />
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
		}

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="OrderedHashSet{T}"/> class.
		/// </summary>
		public OrderedHashSet()
			: this(EqualityComparer<T>.Default)
		{
			// Nothing to do.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OrderedHashSet{T}"/> class.
		/// </summary>
		/// <param name="comparer">The equality comparer to use.</param>
		public OrderedHashSet(IEqualityComparer<T> comparer)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(comparer != null);
			#endregion
			this.innerSet = new HashSet<T>(comparer);
			this.innerList = new ArrayList<T>(comparer);
		}
		#endregion

		/// <inheritdoc />
		public bool Add(T value)
		{
			bool success = this.innerSet.Add(value);
			if (success)
			{
				success = this.innerList.Add(value);
				Contract.Assume(success);
			}
			return success;
		}

		/// <inheritdoc />
		public bool AddOrUpdate(T value)
		{
			bool success = Add(value);
			if (!success)
				success = Update(value);
			return success;
		}

		/// <inheritdoc />
		public bool Update(T value)
		{
			if (!this.innerSet.Contains(value))
				return false;
			
			int index = (int)this.innerList.IndexOf(value);
			bool success = this.innerSet.Update(value);
			if (success)
				this.innerList[index] = value;
			return success;
		}

		/// <inheritdoc />
		public bool Insert(int index, T value)
		{
			bool success = this.innerSet.Add(value);
			if (success)
			{
				success = this.innerList.Insert(index, value);
				Contract.Assume(success);
			}
			return success;
		}

		/// <inheritdoc />
		public bool RemoveAt(int index)
		{
			T value = this.innerList[index];
			bool success = this.innerSet.Remove(value);
			if (success)
			{
				success = this.innerList.RemoveAt(index);
				Contract.Assume(success);
			}
			return success;
		}

		/// <inheritdoc />
		public bool Remove(object value)
		{
			bool success = this.innerSet.Remove(value);
			if (success)
			{
				success = ((IMutableList<T>)this.innerList).Remove(value);
				Contract.Assume(success);
			}
			return success;
		}

		/// <inheritdoc />
		public void Clear()
		{
			this.innerSet.Clear();
			this.innerList.Clear();
		}

		/// <inheritdoc />
		public bool Contains(object value)
		{
			return this.innerSet.Contains(value);
		}

		/// <inheritdoc />
		public int? IndexOf(object value)
		{
			if (!Contains(value))
				return null;
			return (int)((IMutableList<T>)this.innerList).IndexOf(value);
		}

		/// <inheritdoc />
		public T GetMember(object value, out bool isPresent)
		{
			return ((IMutableSet<T>)this.innerSet).GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		public T GetMember(object value)
		{
			return this.innerSet.GetMember(value);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value, out bool isPresent)
		{
			return GetMember(value, out isPresent);
		}

		/// <inheritdoc />
		object ICollection.GetMember(object value)
		{
			return GetMember(value);
		}

		/// <inheritdoc />
		public virtual bool IsValidMember(object value)
		{
			return value is T;
		}

		/// <inheritdoc />
		public void ExceptWith(IEnumerable<T> other)
		{
			this.innerSet.ExceptWith(other);
			UpdateList();
		}

		/// <inheritdoc />
		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			this.innerSet.SymmetricExceptWith(other);
			UpdateList();
		}

		/// <inheritdoc />
		public void IntersectWith(IEnumerable<T> other)
		{
			this.innerSet.IntersectWith(other);
			UpdateList();
		}

		/// <inheritdoc />
		public void UnionWith(IEnumerable<T> other)
		{
			this.innerSet.UnionWith(other);
			UpdateList();
		}

		/// <summary>
		/// Removes all elements from the list that are not in the set.
		/// </summary>
		private void UpdateList()
		{
			var newList = new ArrayList<T>(this.innerList.Comparer);
			for (int i = 0; i < this.innerList.Count; i++)
			{
				var item = this.innerList[i];
				if (this.innerSet.Contains(item))
					newList.Add(item);
			}
			this.innerList = newList;
		}

		/// <inheritdoc />
		public bool Overlaps(System.Collections.IEnumerable other)
		{
			return this.innerSet.Overlaps(other);
		}

		/// <inheritdoc />
		public bool SetEquals(System.Collections.IEnumerable other)
		{
			return this.innerSet.SetEquals(other);
		}

		/// <inheritdoc />
		public bool IsSubsetOf(System.Collections.IEnumerable other)
		{
			return this.innerSet.IsSubsetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSubsetOf(System.Collections.IEnumerable other)
		{
			return this.innerSet.IsProperSubsetOf(other);
		}

		/// <inheritdoc />
		public bool IsSupersetOf(System.Collections.IEnumerable other)
		{
			return this.innerSet.IsSupersetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSupersetOf(System.Collections.IEnumerable other)
		{
			return this.innerSet.IsProperSupersetOf(other);
		}

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator()
		{
			return this.innerList.GetEnumerator();
		}

		/// <inheritdoc />
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#region Invariants
		[ContractInvariantMethod]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic",
			Justification = "Required for code contracts.")]
		private void ObjectInvariant()
		{
			Contract.Invariant(this.innerSet != null);
			Contract.Invariant(this.innerList != null);
		}
		#endregion
	}
}
