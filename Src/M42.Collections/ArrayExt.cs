﻿#region Copyright and License
// Copyright 2012-2013 Daniel Pelsmaeker
// 
// This file is part of M42.
// 
// M42 is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//    
// M42 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with M42.  If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using SCG = System.Collections.Generic;
using System.Diagnostics.Contracts;
using SysArray = System.Array;

namespace M42.Collections
{
	/// <summary>
	/// Static methods for working with arrays.
	/// </summary>
	public static class ArrayExt
	{
		/// <summary>
		/// Creates a new array with the specified elements.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="elements">The elements.</param>
		/// <returns>The created array.</returns>
		public static Array<T> Create<T>(params T[] elements)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Ensures(Contract.Result<Array<T>>() != null);
			#endregion
			return new Array<T>(elements);
		}

		/// <summary>
		/// Creates a new array with the specified elements.
		/// </summary>
		/// <typeparam name="T">The type of elements.</typeparam>
		/// <param name="elements">The elements.</param>
		/// <returns>The created array.</returns>
		public static Array<T> Create<T>(SCG.IEnumerable<T> elements)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(elements != null);
			Contract.Ensures(Contract.Result<Array<T>>() != null);
			#endregion
			return elements.ToM42Array();
		}

		/// <summary>
		/// Copies part of the source array to the specified destination array.
		/// </summary>
		/// <param name="source">The source array.</param>
		/// <param name="sourceIndex">The zero-based index in
		/// <paramref name="source"/> at which to start copying.</param>
		/// <param name="destination">The destination array.</param>
		/// <param name="destinationIndex">The zero-based index in
		/// <paramref name="destination"/> at which to put
		/// the first copied value.</param>
		/// <param name="count">The number of elements to copy.</param>
		public static void Copy<T>(Array<T> source, int sourceIndex, Array<T> destination, int destinationIndex, int count)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentOutOfRangeException>(sourceIndex >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(sourceIndex <= source.Count);
			Contract.Requires<ArgumentNullException>(destination != null);
			Contract.Requires<ArgumentOutOfRangeException>(destinationIndex >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(destinationIndex <= destination.Count);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(sourceIndex + count <= source.Count);
			Contract.Requires<ArgumentOutOfRangeException>(destinationIndex + count <= destination.Count);
			#endregion
			SysArray.Copy(source.innerArray, sourceIndex, destination.innerArray, destinationIndex, count);
		}

		/// <summary>
		/// Copies part of the source array to the specified destination array.
		/// </summary>
		/// <param name="source">The source array.</param>
		/// <param name="destination">The destination array.</param>
		/// <param name="count">The number of elements to copy.</param>
		public static void Copy<T>(Array<T> source, Array<T> destination, int count)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(destination != null);
			Contract.Requires<ArgumentOutOfRangeException>(count >= 0);
			Contract.Requires<ArgumentOutOfRangeException>(count <= source.Count);
			Contract.Requires<ArgumentOutOfRangeException>(count <= destination.Count);
			#endregion
			Copy(source, 0, destination, 0, count);
		}

		/// <summary>
		/// Copies the entire source array to the specified destination array.
		/// </summary>
		/// <param name="source">The source array.</param>
		/// <param name="destination">The destination array.</param>
		public static void Copy<T>(Array<T> source, Array<T> destination)
		{
			#region Contract
			Contract.Requires<ArgumentNullException>(source != null);
			Contract.Requires<ArgumentNullException>(destination != null);
			Contract.Requires<ArgumentException>(source.Count <= destination.Count);
			#endregion
			Copy(source, 0, destination, 0, source.Count);
		}
	}
}
