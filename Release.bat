cd Src/M42.Collections
nuget pack -Prop Configuration=Release -OutputDirectory "..\..\Releases" -Verbosity detailed -IncludeReferencedProjects
cd ../M42.Collections.Interop
nuget pack -Prop Configuration=Release -OutputDirectory "..\..\Releases" -Verbosity detailed -IncludeReferencedProjects
pause